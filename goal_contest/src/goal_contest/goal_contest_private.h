/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 12/5/19.
//

#ifndef GOAL_CONTEST_GOAL_CONTEST_PRIVATE_H
#define GOAL_CONTEST_GOAL_CONTEST_PRIVATE_H

namespace goal_contest {

struct AxisTest {
  bool enabled;
  std::string axis_label;
  ros::Time end;
  double amplitude;
  double period;
  double offset;

  // phase of the test sinusoid
  double current_phase;
  ros::Time current_time;

  // current_phase += 2*pi/period*delta_T
  // output amplitude * sin(current_phase)

  std::pair<bool, double> calc_joystick(const ros::Time& now) {
    if (!enabled) {
      return std::make_pair(false, 0);
    }

    // check for end of test
    if (now > end) {
      ROS_INFO_STREAM(axis_label <<" CONTEST ended!");
      enabled = false;
      return std::make_pair(false, 0);
    }

    ros::Duration dt = now - current_time;
    current_time = now;

    // don't divide by zero
    double ret = amplitude;
    if (period > 0.01) {
      current_phase += 2.0 * M_PI / period * dt.toSec();
      ret = amplitude * sin(current_phase) + offset;
    }
    if (ret > 1.0) {
      ret = 1.0;
    }
    if (ret < -1.0) {
      ret = -1.0;
    }

    return std::make_pair(true, ret);
  }

  void start_test(double _amplitude, double _period, double _duration, double _offset) {
    end = ros::Time::now() + ros::Duration(_duration);
    amplitude = _amplitude;
    period = _period;
    offset = _offset;
    current_phase = 0.0;
    enabled = true;
  }
};

struct GoalConTestPrivate {
  GoalConTestPrivate() {
    tests[0].axis_label = "NONE";
    tests[1].axis_label = "SURGE";
    tests[2].axis_label = "SWAY";
    tests[3].axis_label = "HEAVE";
    tests[4].axis_label = "HEADING";
    tests[5].axis_label = "PITCH";
    tests[6].axis_label = "ROLL";
  }

  // one for each axis, plus 0 as a placeholder for none.
  AxisTest tests[7];

  ros::ServiceServer test_cmd_service;
};

} //namespace goal_contest

#endif //GOAL_CONTEST_GOAL_CONTEST_PRIVATE_H
