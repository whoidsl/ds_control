/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 12/5/19.
//

#include "goal_contest/goal_contest.h"
#include "goal_contest_private.h"

namespace goal_contest {

GoalConTest::GoalConTest() : goal_joy::GoalJoy(), d_ptr_(std::unique_ptr<GoalConTestPrivate>(new GoalConTestPrivate))
{
}

GoalConTest::GoalConTest(int argc, char** argv, const std::string& name)
  : goal_joy::GoalJoy(argc, argv, name), d_ptr_(std::unique_ptr<GoalConTestPrivate>(new GoalConTestPrivate))
{
}

GoalConTest::~GoalConTest() = default;

void GoalConTest::setupServices() {
  DS_D(GoalConTest);

  auto nh = nodeHandle();

  d->test_cmd_service = nh.advertiseService(ros::this_node::getName() + "/test_cmd",
      &GoalConTest::_start_con_test_callback_, this);
}

double clamp(double value) {
  if (value > 1.0) {
    return 1.0;
  }
  if (value < -1.0) {
    return -1.0;
  }
  return value;
}

ds_nav_msgs::AggregatedState GoalConTest::translateJoystick(const sensor_msgs::Joy& joy) {
  // start by copying the joystick message
  sensor_msgs::Joy new_joy(joy);

  // now go through the list of axes and overwrite any active tests
  DS_D(GoalConTest);
  GoalJoy::JoystickInput joy_axis;
  auto inp = goal_joy::GoalJoy::joystickInputForGoal(GoalDOF::VELOCITY_U);
  std::pair<bool, double> test;
  auto now = ros::Time::now();

  test = d->tests[goal_contest::ConTestCmdRequest::AXIS_SURGE].calc_joystick(now);
  if (test.first) {
    new_joy.axes.at(joystickInputForGoal(GoalDOF::VELOCITY_U) & 0xFFF) = test.second;
  }
  test = d->tests[goal_contest::ConTestCmdRequest::AXIS_SWAY].calc_joystick(now);
  if (test.first) {
    new_joy.axes.at(joystickInputForGoal(GoalDOF::VELOCITY_V) & 0xFFF) = test.second;
  }
  test = d->tests[goal_contest::ConTestCmdRequest::AXIS_HEAVE].calc_joystick(now);
  if (test.first) {
    new_joy.axes.at(joystickInputForGoal(GoalDOF::VELOCITY_W) & 0xFFF) = test.second;
  }

  test = d->tests[goal_contest::ConTestCmdRequest::AXIS_HEADING].calc_joystick(now);
  if (test.first) {
    new_joy.axes.at(joystickInputForGoal(GoalDOF::ROTATION_RATE_W) & 0xFFF) = test.second;
  }
  test = d->tests[goal_contest::ConTestCmdRequest::AXIS_PITCH].calc_joystick(now);
  if (test.first) {
    new_joy.axes.at(joystickInputForGoal(GoalDOF::ROTATION_RATE_V) & 0xFFF) = test.second;
  }
  test = d->tests[goal_contest::ConTestCmdRequest::AXIS_ROLL].calc_joystick(now);
  if (test.first) {
    new_joy.axes.at(joystickInputForGoal(GoalDOF::ROTATION_RATE_U) & 0xFFF) = test.second;
  }

  // Now process joystick message as normal
  return goal_joy::GoalJoy::translateJoystick(new_joy);
}

bool GoalConTest::_start_con_test_callback_(goal_contest::ConTestCmdRequest& req,
                                            goal_contest::ConTestCmdResponse& resp) {

  if (req.axis <= 0 || req.axis > 6) {
    ROS_ERROR_STREAM("Invalid CONTEST axis " <<req.axis);
    return false;
  }

  DS_D(GoalConTest);
  ROS_INFO_STREAM("Starting test on axis " <<d->tests[req.axis].axis_label
                                           <<" for " <<req.seconds <<" seconds.  Amplitude="
                                           <<req.amplitude <<", Period=" <<req.period <<", Offset=" <<req.offset);
  d->tests[req.axis].start_test(req.amplitude, req.period, req.seconds, req.offset);
  return true;
}

}; // namespace goal_contest