/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 12/5/19.
//

#ifndef GOAL_CONTEST_GOAL_CONTEST_H
#define GOAL_CONTEST_GOAL_CONTEST_H

#include <goal_joy/goal_joy.h>
#include <goal_contest/ConTestCmd.h>
#include "../../src/goal_contest/goal_contest_private.h"

namespace goal_contest {

struct GoalConTestPrivate;

class GoalConTest : public goal_joy::GoalJoy {
  DS_DECLARE_PRIVATE(GoalConTest);
 public:

  GoalConTest();

  GoalConTest(int argc, char** argv, const std::string& name);

  ~GoalConTest() override;
  DS_DISABLE_COPY(GoalConTest)

  uint64_t type() const noexcept override
  {
    return ds_control_msgs::JoystickEnum::CONTEST;
  }

  void setupServices() override;

  // this node works mostly the same as a standard joystick node, but can
  // overwrite certain axes with injected test values as appropriate
  ds_nav_msgs::AggregatedState translateJoystick(const sensor_msgs::Joy& joy) override;

 private:
  std::unique_ptr<GoalConTestPrivate> d_ptr_;

  bool _start_con_test_callback_(goal_contest::ConTestCmdRequest& req,
      goal_contest::ConTestCmdResponse& resp);
};


}; // namespace goal_contest

#endif //GOAL_CONTEST_GOAL_CONTEST_H
