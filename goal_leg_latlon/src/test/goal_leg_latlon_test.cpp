/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 9/27/19.
//

// Just as GoalLegLatLon is essentially a port of GoalLeg to use lat/lon instead of
// XY position, this test is a port of the GoalLeg tests to use LatLon.  In fact,
// rather than write all-new test cases we define a projection and transform all the
// original values to lat/lon based on that projection.  The trackline library SHOULD
// transform back and reach all the same original answers.

#include <gtest/gtest.h>
#include <ds_libtrackline/Trackline.h>
#include "goal_leg_latlon/goal_leg_latlon.h"

const std::string PREFIX = "/goal_leg_latlon/";

using namespace goal_leg_latlon;

// little class to expose some protected functionality for testing
class GoalLegLatLonShim : public GoalLegLatLon
{
 public:

  GoalLegLatLonShim() : GoalLegLatLon() {
    GoalLegLatLon::setupParameters();
  }

  void setupParams()
  {
    GoalLegLatLon::setupParameters();
  }
};

class GoalLegLatLonTest : public ::testing::Test
{
 protected:
  void SetUp() override
  {
    test_proj_ = std::unique_ptr<ds_trackline::Projection>(new ds_trackline::Projection(-128, 40));

    underTest.reset(new GoalLegLatLon());
    ds_control_msgs::GoalLegLatLon goal = basicParamSetup();
    underTest->goalCallback(goal);
    underTest->setKappa(1.0);
  }

  ds_control_msgs::GoalLegLatLon basicParamSetup()
  {
    ds_trackline::Projection::VectorEN start_en;
    start_en << 2.0, 1.0;  // EN=EAST, then NORTH
    ds_trackline::Projection::VectorEN end_en;
    end_en << 4.0, 2.0;

    auto start_ll = test_proj_->projected2lonlat(start_en);
    auto end_ll = test_proj_->projected2lonlat(end_en);

    ds_control_msgs::GoalLegLatLon goal;

    goal.line_start.longitude = start_ll(0);
    goal.line_start.latitude = start_ll(1);

    goal.line_end.longitude = end_ll(0);
    goal.line_end.latitude = end_ll(1);

    expected_hdg = atan2(2.0, 1.0);  // DEFINITELY atan2(easting, northing)

    return goal;
  }

  ds_nav_msgs::NavState makeNavMsg(double east, double north) const {
    ds_nav_msgs::NavState ret;

    ds_trackline::Projection::VectorEN vec_en;
    vec_en <<east, north;

    auto ll = test_proj_->projected2lonlat(vec_en);

    ret.lon = ll(0);
    ret.lat = ll(1);

    return ret;
  }

  double expected_hdg;
  std::unique_ptr<ds_trackline::Projection> test_proj_;
  std::unique_ptr<GoalLegLatLon> underTest;
};

TEST_F(GoalLegLatLonTest, calculateHeadingRef0)
{
  auto vehicleState = makeNavMsg(2.0, 1.0);
  ds_nav_msgs::FlaggedDouble goal = underTest->computeHeadingGoal(vehicleState);

  EXPECT_TRUE(goal.valid);
  EXPECT_NEAR(expected_hdg, goal.value, 1.0e-5);
}

TEST_F(GoalLegLatLonTest, calculateHeadingRefMinus90)
{
  // this test puts us *just* too far off to one side, so it clamps heading distance to PI/3.0
  double dist = 1.1 * M_PI / 3.0;

  auto vehicleState = makeNavMsg((3.0 / sqrt(5.0)) * dist, (-1.0 / sqrt(5.0)) * dist);
  ds_nav_msgs::FlaggedDouble goal = underTest->computeHeadingGoal(vehicleState);

  EXPECT_TRUE(goal.valid);
  EXPECT_NEAR(expected_hdg - M_PI / 3.0, goal.value, 1.0e-5);
}

TEST_F(GoalLegLatLonTest, calculateHeadingRefPlus90)
{
  double dist = 1.1 * M_PI / 3.0;

  auto vehicleState = makeNavMsg((1.0 / sqrt(5.0)) * dist, (3.0 / sqrt(5.0)) * dist);
  ds_nav_msgs::FlaggedDouble goal = underTest->computeHeadingGoal(vehicleState);

  EXPECT_TRUE(goal.valid);
  EXPECT_NEAR(expected_hdg + M_PI / 3.0, goal.value, 1.0e-5);
}

TEST_F(GoalLegLatLonTest, calculateHeadingInvalid)
{
  double dist = 1.1 * M_PI / 3.0;

  ds_nav_msgs::NavState vehicleState;
  vehicleState.lon = std::numeric_limits<double>::quiet_NaN();
  vehicleState.lat = std::numeric_limits<double>::quiet_NaN();
  ds_nav_msgs::FlaggedDouble goal = underTest->computeHeadingGoal(vehicleState);

  EXPECT_TRUE(!goal.valid);
  EXPECT_NEAR(expected_hdg, goal.value, 1.0e-5);
}

TEST_F(GoalLegLatLonTest, rovExample)
{
  // setup our test trackline
  ds_trackline::Projection::VectorEN start_en;
  start_en << 7811.479, 2599.903;
  ds_trackline::Projection::VectorEN end_en;
  end_en <<7693.079, 2458.803;

  auto start_ll = test_proj_->projected2lonlat(start_en);
  auto end_ll = test_proj_->projected2lonlat(end_en);

  ds_control_msgs::GoalLegLatLon leg_goal;

  leg_goal.line_start.longitude = start_ll(0);
  leg_goal.line_start.latitude = start_ll(1);

  leg_goal.line_end.longitude = end_ll(0);
  leg_goal.line_end.latitude = end_ll(1);

  underTest->goalCallback(leg_goal);
  underTest->setKappa(0.03);

  // ask for a new heading goal based on a position
  auto vehicleState = makeNavMsg(7774.284, 2556.239);
  ds_nav_msgs::FlaggedDouble goal = underTest->computeHeadingGoal(vehicleState);
  ds_control_msgs::GoalLegState state = underTest->getState();

  EXPECT_TRUE(goal.valid);
  EXPECT_DOUBLE_EQ(state.new_goal, goal.value);
  EXPECT_NEAR(-2.443 + 2*M_PI, state.angle_line_segment, 0.001);
  EXPECT_NEAR(-0.426, state.off_line_vect, 0.001);
  EXPECT_NEAR(0.03, state.kappa, 0.001);
  EXPECT_NEAR(-1.000, state.sign_of_vect, 0.1);
  EXPECT_NEAR(-2.456 + 2*M_PI, state.new_goal, 0.001);
}
TEST_F(GoalLegLatLonTest, undefinedTrackline)
{
  // set a test trackline with zero length
  ds_control_msgs::GoalLegLatLon leg_goal;

  leg_goal.line_start.longitude = 10;
  leg_goal.line_start.latitude = 20;

  leg_goal.line_end.longitude = 10;
  leg_goal.line_end.latitude = 20;

  underTest->goalCallback(leg_goal);
  underTest->setKappa(0.03);

  // ask for a new heading goal based on a position
  ds_nav_msgs::NavState vehicleState;
  // position doesn't matter
  vehicleState.lon = 10.0001;
  vehicleState.lat = 20.0001;
  ds_nav_msgs::FlaggedDouble goal = underTest->computeHeadingGoal(vehicleState);
  ds_control_msgs::GoalLegState state = underTest->getState();

  EXPECT_FALSE(goal.valid);
  EXPECT_DOUBLE_EQ(0, goal.value);
}
// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "goal_leg_latlon_test");
  ros::NodeHandle nh;

  ros::AsyncSpinner spinner(2); // use two threads
  spinner.start(); // returns immediately

  return RUN_ALL_TESTS();
}
