/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 9/27/19.
//

#include "goal_leg_latlonPrivate.h"

#include <ds_util/ds_util.h>


namespace goal_leg_latlon {


GoalLegLatLon::GoalLegLatLon() : ds_control::GoalBase(),
              d_ptr_(std::unique_ptr<GoalLegLatLonPrivate>( new GoalLegLatLonPrivate))
{
}

GoalLegLatLon::GoalLegLatLon(int argc, char** argv, const std::string& name) : ds_control::GoalBase(argc, argv, name),
                                                                               d_ptr_(std::unique_ptr<GoalLegLatLonPrivate>( new GoalLegLatLonPrivate))
{
}

GoalLegLatLon::~GoalLegLatLon() = default;

ds_nav_msgs::AggregatedState GoalLegLatLon::computeGoal(const ds_nav_msgs::NavState& vehicleState) {
  ds_nav_msgs::AggregatedState ret;

  // zero out almost all of our state
  setInvalid(ret.northing);
  setInvalid(ret.easting);
  setInvalid(ret.down);
  setInvalid(ret.roll);
  setInvalid(ret.pitch);
  setInvalid(ret.surge_u);
  setInvalid(ret.sway_v);
  setInvalid(ret.heave_w);
  setInvalid(ret.p);
  setInvalid(ret.q);
  setInvalid(ret.r);
  ret.heading = computeHeadingGoal(vehicleState);
  ret.header.stamp = ros::Time::now();

  return ret;
}

ds_nav_msgs::FlaggedDouble GoalLegLatLon::computeHeadingGoal(const ds_nav_msgs::NavState& vehicleState) {
  DS_D(GoalLegLatLon);

  // prepare our return value
  ds_nav_msgs::FlaggedDouble ret;
  ret.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;

  // Update the timestamp for the state
  d->state.header.stamp = vehicleState.header.stamp;
  d->state.ds_header.io_time = ros::Time::now();
  // We have to show our work, so start by updating the goal state appropriately

  d->state.kappa = d->kappa;
  d->state.leg_number = 0;

  if (d->trackline.getLength() < 0.001) {
    ROS_ERROR_STREAM("Trackline too short! Heading is ill-defined, setting to zero");
    d->state.angle_line_segment = 0;
    ret.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
    ret.value = 0;
    return ret;
  }

  // store our old value
  d->state.old_goal = d->state.new_goal;

  // update our nominal course, and use it if the position data is invalid
  d->state.angle_line_segment = d->trackline.getCourseRad();
  if (std::isnan(vehicleState.lon) || std::isnan(vehicleState.lat)) {
    d->state.off_line_vect = std::numeric_limits<double>::quiet_NaN();
    ret.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
    ret.value = d->state.angle_line_segment;
    return ret;
  }

  // compute our alongtrack error and use it to adjust the heading goal from nominal
  auto alongacross = d->trackline.lonlat_to_trackframe(vehicleState.lon, vehicleState.lat);
  d->state.off_line_vect = -alongacross(1); // Dana's old sign convention is backwards from the new trackline library.
  d->state.sign_of_vect = ds_util::sgn(d->state.off_line_vect);

  // Set the new goal to the sum of the off track distance and the new goal
  d->state.new_goal =
      d->state.angle_line_segment +
          d->state.sign_of_vect * std::min<double>(M_PI / 3.0, fabs(d->state.off_line_vect) * d->state.kappa);

  // Take the new wrapped heading that goes from -PI to PI and add it to ref.pos
  ret.value = d->state.new_goal;

  return ret;
}

void GoalLegLatLon::vehicleStateCallback(const ds_nav_msgs::NavState& vehicleState) {
  DS_D(GoalLegLatLon);

  ds_nav_msgs::AggregatedState ref = computeGoal(vehicleState);
  publishGoal(ref);

  d->leg_state_pub.publish(d->state);
}

void GoalLegLatLon::goalCallback(const ds_control_msgs::GoalLegLatLon& goal) {
  DS_D(GoalLegLatLon);

  d->goal = goal;

  d->state.line_start.x = d->goal.line_start.longitude;
  d->state.line_start.y = d->goal.line_start.latitude;
  d->state.line_start.z = 0;

  d->state.line_end.x = d->goal.line_end.longitude;
  d->state.line_end.y = d->goal.line_end.latitude;
  d->state.line_end.z = 0;

  // our trackline has changed. Re-define it
  d->trackline = ds_trackline::Trackline(d->state.line_start.x, d->state.line_start.y,
  d->state.line_end.x, d->state.line_end.y);
}

const ds_control_msgs::GoalLegState& GoalLegLatLon::getState() const {
  DS_D(const GoalLegLatLon);
  return d->state;
}

void GoalLegLatLon::setKappa(double k) {
  DS_D(GoalLegLatLon);
  d->kappa = k;
}


void GoalLegLatLon::setupParameters() {
  DsProcess::setupParameters();
  DS_D(GoalLegLatLon);

  ros::NodeHandle handle = nodeHandle();

  d->kappa = 0.03;
  handle.param(ros::this_node::getName() + "/kappa", d->kappa);
}

void GoalLegLatLon::setupSubscriptions() {
  DsProcess::setupSubscriptions();
  DS_D(GoalLegLatLon);

  ros::NodeHandle nh = nodeHandle();

  d->goal_update_sub = nh.subscribe("goal", 10, &GoalLegLatLon::goalCallback, this);
  d->navstate_update_sub = nh.subscribe("navstate", 10, &GoalLegLatLon::vehicleStateCallback, this);
  d->leg_state_pub = nh.advertise<ds_control_msgs::GoalLegState>("internal_state", 10, false);

}

void GoalLegLatLon::setInvalid(ds_nav_msgs::FlaggedDouble& var) {
  var.value = 0;
  var.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
}

} // namespace goal_leg_latlon
