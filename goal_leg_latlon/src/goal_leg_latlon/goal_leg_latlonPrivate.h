/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 9/29/19.
//

#ifndef GOAL_LEG_LATLON_GOAL_LEG_LATLONPRIVATE_H
#define GOAL_LEG_LATLON_GOAL_LEG_LATLONPRIVATE_H

#include "goal_leg_latlon/goal_leg_latlon.h"
#include <ds_control_msgs/GoalLegState.h>
#include <ds_control_msgs/GoalLegLatLon.h>
#include <ds_libtrackline/Trackline.h>

namespace goal_leg_latlon {

struct GoalLegLatLonPrivate {

  // set to a dummy non-zero value to squelch error messages
  GoalLegLatLonPrivate() : trackline(0, 0, 0, 1) {
    kappa = 0.03;
  }


  // goal position
  ds_control_msgs::GoalLegLatLon goal;

  // internal state
  ds_control_msgs::GoalLegState state;

  // Gains
  double kappa;

  // ROS subscriptions
  ros::Subscriber goal_update_sub;
  ros::Subscriber navstate_update_sub;
  ros::Publisher leg_state_pub;

  // Geographic trackline representation
  ds_trackline::Trackline trackline;
};

}

#endif // GOAL_LEG_LATLON_GOAL_LEG_LATLONPRIVATE_H
