/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 9/27/19.
//

#ifndef GOAL_LEG_LATLON_GOAL_LEG_LATLON_H
#define GOAL_LEG_LATLON_GOAL_LEG_LATLON_H

#include "ds_control/goal_base.h"
#include <ds_control_msgs/GoalLegState.h>
#include <ds_control_msgs/GoalLegLatLon.h>
#include <boost/optional.hpp>

#include <ds_nav_msgs/AggregatedState.h>
#include <ds_nav_msgs/NavState.h>

namespace goal_leg_latlon {

struct GoalLegLatLonPrivate;

class GoalLegLatLon : public ds_control::GoalBase
{
  DS_DECLARE_PRIVATE(GoalLegLatLon);

 public:
/// @brief Construct a new Goal Leg Lat Lon
///
/// When using this constructor you must call ros::init elsewhere in your code
  GoalLegLatLon();

/// @brief Construct a new DsGoalLeg
///
/// This constructor calls ros::init(argc, argv, name) for you
///
/// @param[in] argc
/// @param[in] argv
/// @param[in] name The name of the process type
  GoalLegLatLon(int argc, char** argv, const std::string& name);

/// @brief Destroys a DsGoalJoystick
  ~GoalLegLatLon() override;
  DS_DISABLE_COPY(GoalLegLatLon);
  uint64_t type() const noexcept override
  {
    return 0;
  }

/// \brief Recompute the goal values based on nav data from the nav aggregator
  ds_nav_msgs::AggregatedState computeGoal(const ds_nav_msgs::NavState& vehicleState);
  ds_nav_msgs::FlaggedDouble computeHeadingGoal(const ds_nav_msgs::NavState& vehicleState);

  /// \brief Callback for when vehicle state messages are received
  void vehicleStateCallback(const ds_nav_msgs::NavState& vehicleState);

  /// \brief Callback for a when a new goal message is received
  void goalCallback(const ds_control_msgs::GoalLegLatLon& goal);

  const ds_control_msgs::GoalLegState& getState() const;

  // needed for testing
  void setKappa(double k);

 protected:
  void setupParameters() override;
  void setupSubscriptions() override;

 private:
  std::unique_ptr<GoalLegLatLonPrivate> d_ptr_;
  static void setInvalid(ds_nav_msgs::FlaggedDouble& var);
};

} // namespace goal_leg_latlon

#endif //GOAL_LEG_LATLON_GOAL_LEG_LATLON_H
