/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "goal_bottom_follow/goal_bottom_follow.h"
#include "tf2_ros/transform_broadcaster.h"
#include <gtest/gtest.h>

void run_one(ds_control::GoalBottomFollow& in)
{
  for (int i = 0; i < 10; ++i)
  {
    usleep(100);
    in.asio()->io_service.reset();
    boost::system::error_code ec;
    int n = in.asio()->io_service.run_one(ec);
    ROS_INFO_STREAM("Handlers executed: " << n << " Error code: " << ec);
    if (n > 0)
      return;
  }
}

TEST(GoalBottomFollow, canTransform)
{
  geometry_msgs::TransformStamped msg;
  ds_control::GoalBottomFollow goal;
  boost::optional<geometry_msgs::PointStamped> ret;

  goal.setup();

  // set and send the transform
  msg.header.stamp = ros::Time(0);
  msg.header.frame_id = "base_link";
  msg.child_frame_id = "rdi300_link";
  msg.transform.rotation.x = 1.0;
  goal.setTransform(msg);

  geometry_msgs::PointStamped in;
  in.header.frame_id = "rdi300_link";
  in.point.x = 1.0;
  in.point.y = 1.0;
  in.point.z = 1.0;

  // Verify that this transform can be executed
  ret = goal.lookupAndApplyTransform(in, "rdi300_link", ros::Time(0));
  ASSERT_TRUE(ret != boost::none);
  // Verify that the pure rotation did not change point distance
  ASSERT_EQ(sqrt(ret->point.x * ret->point.x + ret->point.y * ret->point.y + ret->point.z * ret->point.z), sqrt(3));

  // Verify that a transform from a frame for which we have no tf data cannot be executed
  ret = goal.lookupAndApplyTransform(in, "this_frame_does_not_exist", ros::Time(0));
  ASSERT_TRUE(ret == boost::none);

  // Now add also a translation to the transform and set it
  msg.transform.translation.x = 1000.0;
  goal.setTransform(msg);

  // Verify that this transform can be executed
  ret = goal.lookupAndApplyTransform(in, "rdi300_link", ros::Time(0));
  ASSERT_TRUE(ret != boost::none);
  // Verify that the translation modified point vector length
  ASSERT_GT(sqrt(ret->point.x * ret->point.x + ret->point.y * ret->point.y + ret->point.z * ret->point.z), sqrt(3));
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  ros::init(argc, argv, "goal_bottom_follow");
  testing::InitGoogleTest(&argc, argv);
  auto ret = RUN_ALL_TESTS();
  ros::shutdown();
  return ret;
};
