/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "goal_bottom_follow/goal_bottom_follow.h"
#include "goal_bottom_follow_private.h"

namespace ds_control
{
GoalBottomFollow::GoalBottomFollow()
  : ds_control::GoalBase(), d_ptr_(std::unique_ptr<GoalBottomFollowPrivate>(new GoalBottomFollowPrivate))
{
}
GoalBottomFollow::GoalBottomFollow(int argc, char** argv, const std::string& name)
  : ds_control::GoalBase(argc, argv, name)
  , d_ptr_(std::unique_ptr<GoalBottomFollowPrivate>(new GoalBottomFollowPrivate))
{
}

GoalBottomFollow::~GoalBottomFollow() = default;

void GoalBottomFollow::setupSubscriptions()
{
  GoalBase::setupSubscriptions();

  std::string topic;
  topic = ros::param::param<std::string>("~ranges_input_topic", "/ranges");

  ROS_INFO_STREAM("Subscribing for ranges at topic: " << topic);
  range_sub_ = nodeHandle().subscribe<ds_sensor_msgs::Ranges3D>(
      topic, 1, boost::bind(&GoalBottomFollow::onRanges3DMsg, this, _1));
}

void GoalBottomFollow::setupParameters()
{
  GoalBase::setupParameters();

  target_frame_ = ros::param::param<std::string>("~target_frame", "base_link");

  spin_thread_ = ros::param::param<bool>("~spin_thread", true);

  startTf(spin_thread_);
}

void GoalBottomFollow::setTransform(geometry_msgs::TransformStamped in)
{
  tfBuffer_.setTransform(in, "manual");
}

void GoalBottomFollow::startTf(bool spin_thread)
{
  // Set up tf and start listening
  auto nh = nodeHandle();
  tfListener_.reset(new tf2_ros::TransformListener(tfBuffer_, nh, spin_thread));
}

void GoalBottomFollow::stopTf()
{
  // Destroys the tf listener object and stops receiving incoming tf messages
  tfListener_.reset(nullptr);
}

void GoalBottomFollow::setRangeCallback(boost::function<void(ds_sensor_msgs::Ranges3D)> in)
{
  rangeCallback_ = in;
}

void GoalBottomFollow::clearRangeCallback(void)
{
  rangeCallback_ = NULL;
}

geometry_msgs::PointStamped GoalBottomFollow::applyTransform(geometry_msgs::PointStamped in,
                                                             geometry_msgs::TransformStamped tf_in)
{
  geometry_msgs::PointStamped out;
  tf2::doTransform(in, out, tf_in);

  return out;
}

boost::optional<geometry_msgs::PointStamped> GoalBottomFollow::lookupAndApplyTransform(geometry_msgs::PointStamped in,
                                                                                       std::string frame_id,
                                                                                       ros::Time tf_time)
{
  geometry_msgs::TransformStamped range_tf;
  std::string errstr;
  if (tfBuffer_.canTransform("base_link", frame_id, tf_time, ros::Duration(0.001), &errstr))
  {
    range_tf = tfBuffer_.lookupTransform(target_frame_, frame_id, tf_time, ros::Duration(0.001));

    geometry_msgs::PointStamped tf_range;
    tf_range = applyTransform(in, range_tf);

    return tf_range;
  }
  else
  {
    ROS_WARN_STREAM("Can's transform ranges from frame " << frame_id << " to base_link at time " << tf_time
                                                         << ". Error string: " << errstr);
    return boost::none;
  }
}

void GoalBottomFollow::onRanges3DMsg(const ds_sensor_msgs::Ranges3D::ConstPtr& msg)
{
  ROS_DEBUG_STREAM("Received ranges");
  ds_sensor_msgs::Ranges3D tf_ranges = *msg;

  for (int i = 0; i < msg->ranges.size(); ++i)
  {
    boost::optional<geometry_msgs::PointStamped> ret;
    if (ret = lookupAndApplyTransform(msg->ranges[i].range, msg->ranges[i].range.header.frame_id, ros::Time(0)))
    {
      tf_ranges.ranges[i].range = *ret;
    }
    else
    {
      return;
    }
  }
  // Call the callback if the trasnform could be done, if the callback has been set
  if (rangeCallback_)
  {
    rangeCallback_(tf_ranges);
    ROS_DEBUG_STREAM("Called range callback");
  }
}
}
