/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef GOAL_BOTTOM_FOLLOW_H
#define GOAL_BOTTOM_FOLLOW_H

#include "ds_control/goal_base.h"
#include "ds_sensor_msgs/Ranges3D.h"
#include "ds_sensor_msgs/Range3D.h"
#include "ds_param/ds_param.h"
#include "ds_param/ds_param_conn.h"
#include "ds_param/ds_param_guard.h"
#include "tf2_ros/buffer.h"
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"
#include "tf2_ros/transform_listener.h"
#include "geometry_msgs/TransformStamped.h"
#include "geometry_msgs/Vector3Stamped.h"
#include <boost/function.hpp>
#include <boost/optional.hpp>

namespace ds_control
{
struct GoalBottomFollowPrivate;

class GoalBottomFollow : public ds_control::GoalBase
{
  DS_DECLARE_PRIVATE(GoalBottomFollow);

public:
  /// @brief Construct a new GoalBottomFollow
  ///
  /// When using this constructor you must call ros::init elsewhere in your code
  GoalBottomFollow();

  /// @brief Construct a new GoalBottomFollow
  ///
  /// This constructor calls ros::init(argc, argv, name) for you
  ///
  /// @param[in] argc
  /// @param[in] argv
  /// @param[in] name The name of the process type
  GoalBottomFollow(int argc, char** argv, const std::string& name);

  /// @brief Destroys a GoalBottomFollow
  ~GoalBottomFollow() override;
  DS_DISABLE_COPY(GoalBottomFollow)

  void startTf(bool spin_thread);

  void stopTf();

  void setRangeCallback(boost::function<void(ds_sensor_msgs::Ranges3D)> in);

  void clearRangeCallback(void);

  geometry_msgs::PointStamped applyTransform(geometry_msgs::PointStamped in, geometry_msgs::TransformStamped tf_in);

  boost::optional<geometry_msgs::PointStamped> lookupAndApplyTransform(geometry_msgs::PointStamped in,
                                                                       std::string frame_id, ros::Time tf_time);

  void onRanges3DMsg(const ds_sensor_msgs::Ranges3D::ConstPtr& msg);

  /// @brief Store a transform in the tf2 buffer from a "manual" authority
  void setTransform(geometry_msgs::TransformStamped in);

  uint64_t type() const noexcept override
  {
    return 0;
  }

protected:
  void setupParameters() override;
  void setupSubscriptions() override;

private:
  std::unique_ptr<GoalBottomFollowPrivate> d_ptr_;

  tf2_ros::Buffer tfBuffer_;
  std::unique_ptr<tf2_ros::TransformListener> tfListener_;

  bool spin_thread_;

  std::string target_frame_;

  ros::Subscriber range_sub_;
  boost::function<void(ds_sensor_msgs::Ranges3D)> rangeCallback_;
};
}
#endif  // GOAL_BOTTOM_FOLLOW_H
