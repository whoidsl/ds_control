This document describes the rov bottom following algorithm.

Revision History
2008-07-03    mvj    Created.
2008-07-06    mvj    Altered modulation of ref_speed in speed 
	             padding to decrease to some non-necessarily
		     zero value speed_min, and to be fixed at 
		     a speed outside the envelope, speed_outside.
		     Also updated out-of-range/too-close logic.
2019-05-01    jisv   Copied from original Sentry repository to ds_control


Introduction:
-------------

The rov bottom follower is based on the ABE bottom follower developed by 
D. Yoerger and R. Bachmeyer.  For a given desired altitude above bottom, 
an envelope is created within which the desired depth is increased or 
decreased in discrete increments (steps) as necessary to maintain the 
vehicle within the envelope.  If the vehicle strays near the edges of the 
envelope commanded forward speed is decreased, allowing the vehicle to 
attain a higher climb slope.

There are a few differences between the ABE bottom follower and the rov 
bottom follower:

1) The step size is defined such that the vehicle will not stray outside 
   the envelope assuming the vehicle executes the (trapezoidally smoothed) 
   depth trajectory and speed trajectory.  The depth goal will be stepped 
   within a smaller sub-envelope, with padding on either side that makes 
   up for smoothed trajectories.  In order to compute the step size 	 
   properly, the smoothing parameters used by the depth trajectory 
   generator (depth rate and acceleration) must be known to the bottom 
   follower.

2) The bottom follower generates goal depths only and goal speeds only.  
   The trajectory generator creates smooth reference trajectories from the 
   goal.  To satisfy point (1) above, these should be derived from 
   trapezoidal velocity profiles.  (See goal_trajectory_trapezoidal in 
   reference.cpp.)

3) The bottom follower includes an alarm that is set to 1 whenever the 
   vehicle is outside the envelope.  The alarm will only stand down (set 
   to 0) after the vehicle has returned to within the confines of the 
   envelope for a specified timeout.  This is useful for "flying" vehicles 
   that must transition to alternative actuator allocations in order to 
   traverse steep slopes.

Parameters:
-----------

The bottom follower has 14 parameters.  These all have defaults 
(bottom_follow.h), and most can be specified in the .ini file and at runtime 
via the WBF JT command.  The most important ones appear first in the argument
list of the WBF command.  The WBF command takes a variable number of arguments
and can be used to change just the first n parameters.

alt_d: 
  Desired altitude (m).
  - Default:            50.0  
  - .ini file entry:    BOTTOM_FOLLOW_ALTITUDE
  - WBF command field:  0

env: 
  Total envelope width (m), e.g. a 10 m envelope means 5 m above and below the 
  desired altitude.
  - Default:           10.0
  - .ini file entry:   BOTTOM_FOLLOW_ENVELOPE
  - WBF command field:  1

speed: 
  Nominal forward speed goal.  The actual speed goal is reduced as the vehicle 
  strays near the edges of the envelope.
  - Default:           0.0
  - .ini file entry:   BOTTOM_FOLLOW_SPEED
  - WBF command field: 2

speed_gain:
  Non-dimensional.  Sets how aggressively speed is backed off when the bottom 
  rises or falls away more steeply than the vehicle can handle.  The portion 
  of the total envelope width taken up by speed padding is env/speed_gain.
  Values <= 1.0 are invalid.
  - Default:           2.0
  - .ini file entry:   BOTTOM_FOLLOW_SPEED_GAIN
  - WBF command field: 3

depth_rate_d:
  Maximum depth rate as used by the trajectory smoother.  For maximum performance
  this should be roughly the vehicle's maximum climb rate at the nominal forward
  speed.
  - Default:           0.0 (no acceleration padding)
  - .ini file entry:   BOTTOM_FOLLOW_DEPTH_RATE
  - WBF command field: 4

depth_accel_d:
  Depth acceleration rate (assumed constant).  This is used along with 
  depth_rate_d to determine how much padding is required to make up for
  the time required by the vehicle to accelerate.
  - Default:           0.0 (no acceleration padding)
  - .ini file entry:   BOTTOM_FOLLOW_ACCEL
  - WBF command field: 5

speed_min:
  Sets ref_speed at outer edge of speed padding.
  - Default:           0.0
  - .ini file entry:   BOTTOM_FOLLOW_MAX_BACKUP_SPEED
  - WBF command field: 6

alarm_timeout:
  Time (s) required for the vehicle to remain within the envelope before
  the alarm stands down.
  - Default:           60.0
  - .ini file entry:   BOTTOM_FOLLOW_ALARM_TIMEOUT
  - WBF command field: 7

depth_floor:
  Maximum depth (m) to which the bottom follower will command the vehicle.
  - Default:           20000 (essentially no depth floor)
  - .ini file entry:   BOTTOM_FOLLOW_DEPTH_FLOOR
  - WBF command field: 8

alt_max_range:
  Maximum range reliably reported by the altimeter.  
  - Default:           250.0
  - .ini file entry:   BOTTOM_FOLLOW_MAX_ALTIMETER_RANGE 
  - WBF command field: N/A
  
alt_bad_max_range:
  - Default:           0.0
  - .ini file entry:   BOTTOM_FOLLOW_ALTITUDE_BAD_MAX_RANGE
  - WBF command field: N/A

alt_min_range:
  Maximum range reliably reported by the altimeter.  
  - Default:           2.0
  - .ini file entry:   BOTTOM_FOLLOW_MIN_ALTIMETER_RANGE 
  - WBF command field: N/A
  
alt_bad_min_range:
  - Default:           0.0
  - .ini file entry:   BOTTOM_FOLLOW_ALTITUDE_BAD_MIN_RANGE
  - WBF command field: N/A

speed_outside:
  Sets ref_speed outside of envelope, or for un-padded envelopes
  where ref_speed would otherwise be < speed_min.
  - Default:           0.0
  - .ini file entry:   BOTTOM_FOLLOW_SPEED_OUTSIDE
  - WBF command field: N/A

Not all combinations of parameters produce a valid envelope (see below).  If 
the step size would be negative, the parameters will be rejected by the 
bottom follower.  If this happens at startup, the rov code aborts with a 
message.  If it happens in response to a WBF command, the old parameters 
are retained.  


Envelope Definition:
--------------------

Graphically the envelope is defined by:

    ====== upper = boundary =======  -----
             speed padding               ^  
    - - - - - - - - - - - - - - - -      |  
          acceleration padding           |
    - - - - - - - - - - - - - - - -      |
                                        env
     inner envelope (2*step_size)        |
                                         |
    - - - - - - - - - - - - - - - -      |
          acceleration padding           |
    - - - - - - - - - - - - - - - -      |
            speed padding                v
    ====== lower = boundary =======  -----

The depth goal is stepped within the inner envelope.  The step size is equal
to one half the width of the inner envelope.  The user specifies the total 
envelope width (env), while the bottom-follower computes the required 
paddings and the step size according to:
 
  (a)  acceleration_padding = depth_rate_d^2 / (2*depth_accel_d)
  (b)  speed_padding        = env / (2*speed_gain)
  (c)  step_size            = env / 2 - speed_padding - acceleration_padding

The final equation for step_size is
  
       step_size = env/2 * (1 - 1/speed_gain) 
                   - depth_rate_d^2 / (2*depth_accel_d)

Valid parameters require that step_size > 0.  The bottom follower will
reject parameter values that do not satisfy this condition and retain its
old ones.

The acceleration padding is the extra width required to account for the 
overshoot that a trapezoidal velocity reference trajectory will generate
while accelerating to a full rate climb:

                +------------ goal
                |     
                |           o
		|          o  reference trajectory
		|         o 
		|       o   
         -------+ o o   
                   overshoot

As long as the vehicle stays within the sub-envelope defined by the outer edges
of the acceleration envelopes, the commanded reference speed will be held 
constant at the nominal forward speed.

If the vehicle strays outside of this constant speed envelope, the commanded
reference speed is reduced toward speed_min at the outer edges of the speed 
envelope (equivalently the total envelope).  It is reduced linearly 
according to 
   
   ref_speed = speed_d - 2 *speed_gain / env * (speed_d - speed_min) * dist

where dist is the vehicle's distance from the inner edge of the speed 
padding envelope.  When dist = env/(2*speed_gain), i.e. the speed 
padding thickness (b), ref_speed = speed_min.  speed_gain is dimensionless 
and normalized by  total envelope width env.  The fraction of the total 
envelope width taken up by speed padding is 1/speed_gain.  Thus for a 
speed_gain of  2.0 and a 10 m total envelope, 2.5 m inside either edge 
of the total envelope will be used for speed padding.

It may be easier to think in terms of the dimensional ((m/s)/m) gain 
given by speed_gain/(env*2)*(speed_d - speed_min).  The advantage of the 
non-dimensional form is that higher gains are computed automatically
for more aggressive (tighter) total envelope widths without requiring
speed_gain to be changed.

Outside the envelope (defined as where ref_speed computed as above would
otherwise be < speed_min) the ref_speed is fixed to speed_outside.

In summary, the ref speed profiles across the envelope (with ref_speed 
the ordinate) look like:

     speed_min > speed_outside:

         inside+accel
       ----------------\   
                        \   
                         \speed padding 
                          \   
                           \
			    +    
			    |     outside
                            +---------------------

    speed_min < speed_outside:


         inside+accel
       ----------------\
                        \speed padding        
                         \        
                          \ +--------------------
                           \|    outside
                            +



Using the Alarm:
----------------
A fully-actuated vehicle can attain a higher climb angle (up to vertical)
by slowing down.  "Flying" vehicles like Nereus and Sentry that use wing 
lift to generate w-axis force suffer a kind of "losing battle" as forward
speed is decreased.  As speed decreases, they produce less vertical force 
and ultimately attain diminishing climb angles.  The alarm can be used
to switch into the rov-like thruster-based actuator allocation mode of 
either vehicle to create a fully actuated vehicle.

The alarm is raised when the vehicle strays outside the outer envelope.  
It will remain in effect until the vehicle has returned to within the
envelope for at least alarm_timeout.

Ultimately both Nereus and Sentry will use more sophisticated controllers
that allocate both thrusters and wings toward generating vertical thrust.
Such controllers should dramatically improve bottom following performance
by avoiding the "losing battle" that wing-lift only controllers suffer as
forward speed is reduced.  

Until these controllers are developed, it is _imperative_ that the alarm be
used to switch into an rov-like mode for flying vehicles.  If it is not,
flying vehicles can end up in "irons" - outside the envelope with their 
reference forward speed reduced to zero and no vertical lift to drive them 
back into the envelope.  Using generous envelopes with depth_rate_d and 
depth_accel_d well within the vehicle's capability will reduce the 
incidence of alarms.

Handling Bad Altitude:
----------------------
At least three types of altimeter failures can occur:
 1) seafloor out of range
 2) seafloor too close
 3) altimeter not reporting (stale) or reporting an erroneous altitude.

Altitude is read by the bottom follower from vehicle_state_t *veh->altitude.
Sentinel values for cases (1) and (2) cause a the BF to use a "pseudo-bottom"
defined according to the sentinel value.

If the altitude is equal to alt_bad_min_range and this sentinel value
is distinct from alt_bad_max_range, the bottom follower will create a 
pseudo bottom according to

  depth_bot = vehicle_depth

Otherwise, if the altitude is equal to alt_bad_max_range, the bottom 
follower will create a pseudo bottom according to

  depth_bot = vehicle_depth + altimeter_max_range

This is the correct thing to do when the altimeter is reporting maximum
range (1).  It produces a depth goal below the vehicle that drives it toward
the seafloor.  When the seafloor comes within range the goal depth will
be correct because the pseudo-bottom and actual bottom are coincident.

If the sentinel values are identical, then the BF tries to differentiate 
between bad values indicating a seafloor that is too close vs. one that 
is too far by timing how long valid altitudes below the minimum are 
reported.  If that time exceeds the alt_bad_timeout, further sentinel 
values are regarded as evidence of the vehicle sitting on the bottom.  
If however a sentinel value arrives before the timeout (or altitude
returns above the minimum), the timer is reset.  This is necessary to 
prevent fliers from causing the vehicle to stop driving toward the 
seafloor, but it is unavoidably brittle.
   
It is vitally important in the case of identical sentinel values to set 
the timeout and minimum altitude threshold such that valid altitudes below 
the minimum will be reported for at least the timeout during a maximum 
rate descent toward the seafloor.  Unfortunately crashes may still break 
this  mechanism and cause the BF to use an altitude-out-of-range 
pseudo-bottom (thereby driving the vehicle down).  

A short timeout reduces the likelihood of a crash precipitating this 
behavior, but is less robust to fliers since the BF will regard sentinel 
values received after the timeout as indicative of sitting on the seafloor.

The only real solution is an altimeter, or composite altimeter, that 
differentiates between altitudes beyond maximum range and beneath minimum.

Failure type (3) is beyond the scope of the bottom follower.  A faulty
altimeter should be grounds for aborting the mission.

Pseudo-Bottoms:
---------------
The BF will never set the depth goal to beyond depth floor, and
will instead use a suitably defined psuedo-bottom.  Bad altitudes
as decribed above also cause the BF to use a pseudo-bottom.  The
use of a pseudo-bottom rather than manipulating the goal depth 
directly keeps the BF state consistent.

Three types of pseudo-bottoms are computed, with only one in effect
at a time according to precedence defined by:
1) Depth floor 
2) Altitude below minimum (drives vehicle up)
3) Altitude above maximum (drives vehicle down)
4) Use the real bottom, veh_depth + altitude.

The pseudo-bottoms being computed are logged as bits in an error code
(see bottom_follow.h).

Logging and Tuning:
-------------------------------
Bottom follower state is logged in .BFL files.  These can be read by 
the function read_bfl.m in the matlab/structures subdirectory of the
rov repository.  There is also a function for viewing the results,
plt_bfl.m in matlab/plotting_funcs/ which can be called with an 
optional argument to plot versus distance instead of versus time.  
Plotting versus distance is necessary to visualize the true slope of 
the seafloor.

Good performance requires tuning the parameters of the bottom follower.
Depth controllers (i.e. PID gains) should be tuned first with an aim toward
attaining high climb rates and high acceleration.  Tuning the bottom
follower requires either a good simulation or field data.  A mission 
script, sentry_tune_bf.pl, suitable for collecting the necessary field 
data on Sentry can be found in the src subdirectory of the mission 
controller.  The mission consists of reciprocal tracklines at 1.0 m/s
nominal with a series of changes in desired altitude.  Two envelope 
widths are tested, 5 m and 20 m, each with a range of progressively 
more aggressive bottom follower parameters.  A plot of the simulated
results, sentry_tune_bf.png, is in this directory.  Good 
parameter choices result in few alarms.  Overly aggressive parameter
choices result in many alarms and slower progress over the seafloor.


Simulation:
-----------
bottom_follow.cpp includes a function bottom_sim() for simulating the 
seafloor near the vehicle.  It produces a seafloor as a Markov Random 
Field that is persistent and therefore suitable for testing obstacle 
avoidance algorithms and the like.  It advances a small grid centered 
roughly beneath the vehicle and permits the simulation of forward-looking and
multi-beam fathometers:

  bottom_sim(v_n,v_e) - return the depth directly beneath the vehicle
                        position (north, east).

  bottom_sim(v_n,v_e,b_n,b_e) - return the depth at a point (b_n, b_e)
                                near the vehicle's location (v_n, v_e).
                                Returns NAN if the location (b_n, b_e)
 				is too far from the vehicle's location.
