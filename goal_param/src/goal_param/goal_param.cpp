/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "goal_param/goal_param.h"
#include "goal_param_private.h"

#include <boost/algorithm/string.hpp>

namespace goal_param
{
GoalParam::GoalParam() : ds_control::GoalBase(), d_ptr_(std::unique_ptr<GoalParamPrivate>(new GoalParamPrivate))
{
}
GoalParam::GoalParam(int argc, char** argv, const std::string& name)
  : ds_control::GoalBase(argc, argv, name), d_ptr_(std::unique_ptr<GoalParamPrivate>(new GoalParamPrivate))
{
}

GoalParam::~GoalParam() = default;

void GoalParam::setupParameters()
{
  DsProcess::setupParameters();
  DS_D(GoalParam);

  ros::NodeHandle handle = nodeHandle();
  d->params = ds_param::ParamConnection::create(handle);

  std::string prefix = ros::this_node::getName() + "/";

  d->desired_northing.connect(d->params, prefix, "northing");
  d->desired_easting.connect(d->params, prefix, "easting");
  d->desired_down.connect(d->params, prefix, "down");

  d->desired_roll.connect(d->params, prefix, "roll");
  d->desired_pitch.connect(d->params, prefix, "pitch");
  d->desired_heading.connect(d->params, prefix, "heading");

  d->desired_surge_u.connect(d->params, prefix, "surge_u");
  d->desired_sway_v.connect(d->params, prefix, "sway_v");
  d->desired_heave_w.connect(d->params, prefix, "heave_w");

  d->desired_r.connect(d->params, prefix, "r");
  d->desired_p.connect(d->params, prefix, "p");
  d->desired_q.connect(d->params, prefix, "q");

  // setup a callback to recompute if any of these parameters ever change
  d->params->setCallback(boost::bind(&GoalParam::param_change_callback, this, _1));
}

ds_nav_msgs::AggregatedState GoalParam::computeGoal()
{
  DS_D(GoalParam);

  ds_nav_msgs::AggregatedState ret;

  ret.northing = d->desired_northing.toNavMsg();
  ret.easting = d->desired_easting.toNavMsg();
  ret.down = d->desired_down.toNavMsg();

  ret.roll = d->desired_roll.toNavMsg();
  ret.pitch = d->desired_pitch.toNavMsg();
  ret.heading = d->desired_heading.toNavMsg();

  ret.surge_u = d->desired_surge_u.toNavMsg();
  ret.sway_v = d->desired_sway_v.toNavMsg();
  ret.heave_w = d->desired_heave_w.toNavMsg();

  ret.r = d->desired_r.toNavMsg();
  ret.p = d->desired_p.toNavMsg();
  ret.q = d->desired_q.toNavMsg();

  ret.header.stamp = ros::Time::now();
  ret.ds_header.io_time = ret.header.stamp;

  return ret;
}

void GoalParam::param_change_callback(const ds_param::ParamConnection::ParamCollection& params)
{
  ds_nav_msgs::AggregatedState ref = computeGoal();
  publishGoal(ref);
}

void GoalParam::setupTimers()
{
  DsProcess::setupTimers();
  DS_D(GoalParam);

  auto timer_callback = [](GoalParam* base, const ros::TimerEvent& event) {
    const auto goal = base->computeGoal();
    base->publishGoal(goal);
  };

  d->timer_ = nodeHandle().createTimer(ros::Duration(1), boost::bind<void>(timer_callback, this, _1));
}
}
