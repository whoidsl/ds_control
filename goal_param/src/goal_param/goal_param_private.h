/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef GOAL_PARAM_GOAL_PARAM_PRIVATE_H
#define GOAL_PARAM_GOAL_PARAM_PRIVATE_H

#include "goal_param/goal_param.h"
#include <ds_param/ds_param.h>
#include <utility>

namespace goal_param
{
struct FlaggedDoubleParam
{
  ds_param::BoolParam::Ptr valid;
  ds_param::DoubleParam::Ptr value;

  void connect(const ds_param::ParamConnection::Ptr& conn, const std::string& prefix, const std::string& name)
  {
    valid = conn->connect<ds_param::BoolParam>(prefix + name + "_valid");
    value = conn->connect<ds_param::DoubleParam>(prefix + name + "_value");
  }

  ds_nav_msgs::FlaggedDouble toNavMsg() const
  {
    ds_nav_msgs::FlaggedDouble ret;
    ret.valid = valid->Get();
    ret.value = value->Get();
    return ret;
  }
};

struct GoalParamPrivate
{
  GoalParamPrivate() = default;
  ~GoalParamPrivate() = default;

  ds_param::ParamConnection::Ptr params;

  FlaggedDoubleParam desired_northing;
  FlaggedDoubleParam desired_easting;
  FlaggedDoubleParam desired_down;

  FlaggedDoubleParam desired_roll;
  FlaggedDoubleParam desired_pitch;
  FlaggedDoubleParam desired_heading;

  FlaggedDoubleParam desired_surge_u;
  FlaggedDoubleParam desired_sway_v;
  FlaggedDoubleParam desired_heave_w;

  FlaggedDoubleParam desired_r;
  FlaggedDoubleParam desired_p;
  FlaggedDoubleParam desired_q;

  ros::Timer timer_;
};
}
#endif  // GOAL_PARAM_GOAL_PARAM_PRIVATE_H
