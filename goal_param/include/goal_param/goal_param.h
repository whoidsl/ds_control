/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef GOAL_PARAM_GOAL_PARAM_H
#define GOAL_PARAM_GOAL_PARAM_H

#include "ds_control/goal_base.h"
#include <boost/optional.hpp>

namespace goal_param
{
struct GoalParamPrivate;

/// @brief A goal that computes its value based on distance to a specified leg between two X/Y points
///
/// # Parameters
///
/// # TODO-- fill this in!
class GoalParam : public ds_control::GoalBase
{
  DS_DECLARE_PRIVATE(GoalParam);

public:
  /// @brief Construct a new Goal Leg
  ///
  /// When using this constructor you must call ros::init elsewhere in your code
  GoalParam();

  /// @brief Construct a new DsGoalParam
  ///
  /// This constructor calls ros::init(argc, argv, name) for you
  ///
  /// @param[in] argc
  /// @param[in] argv
  /// @param[in] name The name of the process type
  GoalParam(int argc, char** argv, const std::string& name);

  /// @brief Destroys a DsGoalJoystick
  ~GoalParam() override;
  DS_DISABLE_COPY(GoalParam);
  uint64_t type() const noexcept override
  {
    return 0;
  }

  void param_change_callback(const ds_param::ParamConnection::ParamCollection& params);
  ds_nav_msgs::AggregatedState computeGoal();

protected:
  void setupParameters() override;
  void setupTimers() override;

private:
  std::unique_ptr<GoalParamPrivate> d_ptr_;
};
}
#endif  // GOAL_JOYSTICK_GOAL_JOYSTICK_H
