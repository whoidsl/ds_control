/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef GOAL_JOY_GOAL_JOY_H
#define GOAL_JOY_GOAL_JOY_H

#include "ds_control/joystick_base.h"
#include "sensor_msgs/Joy.h"
#include "ds_control_msgs/JoystickEnum.h"

namespace goal_joy
{
struct GoalJoyPrivate;

/// @brief A basic sensor_msg::Joy to ds_nav_msgs::AggregatedState translator
///
///
/// # Parameters
///
///  - `~input`    Joystick Input -> Goald DOF mapping
///  - `~gains`    Joystick Input gains.
///
/// ## Input Mapping
///    ~input
///      position_u: "NONE"
///      position_v: "NONE"
///      position_w: "NONE"
///
///      velocity_u: "NONE"
///      velocity_v: "NONE"
///      velocity_w: "NONE"
///
///      rotation_u: "NONE"
///      rotation_v: "NONE"
///      rotation_w: "NONE"
///
///      rotation_rate_u: "NONE"
///      rotation_rate_v: "NONE"
///      rotation_rate_w: "NONE"
///
///  Here, all DOFs are set to "NONE", which isn't all that helpful.  Valid joystick input strings are:
///
///  - "NONE"
///  Buttons:
///  - "A"
///  - "B"
///  - "X"
///  - "Y"
///  - "LB"
///  - "RB"
///  - "BACK"
///  - "START"
///  - "POWER"
///  - "BUTTON_STICK_LEFT"
///  - "BOTTON_STICK_RIGHT"
///
///  Analog Axes:
///  - "LEFT_STICK_HORZ"
///  - "LEFT_STICK_VERT"
///  - "TRIGGER_LEFT"
///  - "RIGHT_STICK_HORZ"
///  - "RIGHT_STICK_VERT"
///  - "TRIGGER_RIGHT"
///  - "DPAD_HORZ"
///  - "DPAD_VERT"
///
class GoalJoy : public ds_control::JoystickBase
{
  DS_DECLARE_PRIVATE(GoalJoy);

public:
  enum JoystickInputType
  {
    BUTTON = 0x1000,
    AXIS = 0x2000,
  };

  enum JoystickInput
  {
    NONE = 0,
    // Buttons
    A = 0 | BUTTON,
    B = 1 | BUTTON,
    X = 2 | BUTTON,
    Y = 3 | BUTTON,
    LB = 4 | BUTTON,
    RB = 5 | BUTTON,
    BACK = 6 | BUTTON,
    START = 7 | BUTTON,
    POWER = 8 | BUTTON,
    BUTTON_STICK_LEFT = 9 | BUTTON,
    BOTTON_STICK_RIGHT = 10 | BUTTON,

    // Axis
    LEFT_STICK_HORZ = 0 | AXIS,
    LEFT_STICK_VERT = 1 | AXIS,
    TRIGGER_LEFT = 2 | AXIS,
    RIGHT_STICK_HORZ = 3 | AXIS,
    RIGHT_STICK_VERT = 4 | AXIS,
    TRIGGER_RIGHT = 5 | AXIS,
    DPAD_HORZ = 6 | AXIS,
    DPAD_VERT = 7 | AXIS
  };

  using GoalSettings = std::array<GoalDOFSettings, GoalDOF::MAX_DOF>;
  using JoystickInputMap = std::array<JoystickInput, GoalDOF::MAX_DOF>;

  GoalJoy();

  GoalJoy(int argc, char** argv, const std::string& name);

  ~GoalJoy() override;
  DS_DISABLE_COPY(GoalJoy)

  uint64_t type() const noexcept override
  {
    return ds_control_msgs::JoystickEnum::JOY;
  }

  void setJoystickInputForGoal(JoystickInput input, GoalDOF dof);
  JoystickInput joystickInputForGoal(GoalDOF dof) const noexcept;

  void setJoystickInputForGoals(JoystickInputMap input);
  const JoystickInputMap& joystickInputForGoals() const noexcept;

  virtual ds_nav_msgs::AggregatedState translateJoystick(const sensor_msgs::Joy& joy);

  // uint64_t type() const noexcept override;

protected:
  void setupParameters() override;
  void setupSubscriptions() override;
  virtual void joystickCallback(const sensor_msgs::Joy& joy);
  ds_nav_msgs::FlaggedDouble calculateGoalForInput(GoalDOF index, const sensor_msgs::Joy& joy) const;

private:
  std::unique_ptr<GoalJoyPrivate> d_ptr_;
};

GoalJoy::JoystickInput from_string(std::string name);
std::string to_string(GoalJoy::JoystickInput input);

std::ostream& operator<<(std::ostream& os, const GoalJoy::JoystickInput& input);
}
#endif  // GOAL_JOY_GOAL_JOY_H
