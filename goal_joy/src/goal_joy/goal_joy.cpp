/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "goal_joy/goal_joy.h"
#include "goal_joy_private.h"

#include <boost/algorithm/string.hpp>

namespace goal_joy
{
GoalJoy::GoalJoy() : ds_control::JoystickBase(), d_ptr_(std::unique_ptr<GoalJoyPrivate>(new GoalJoyPrivate))
{
}
GoalJoy::GoalJoy(int argc, char** argv, const std::string& name)
  : ds_control::JoystickBase(argc, argv, name), d_ptr_(std::unique_ptr<GoalJoyPrivate>(new GoalJoyPrivate))
{
}

GoalJoy::~GoalJoy() = default;

void GoalJoy::setupSubscriptions()
{
  JoystickBase::setupSubscriptions();
  DS_D(GoalJoy);
  const auto joy_channel =
      ros::param::param<std::string>("~joystick_topic", ros::this_node::getName() + "/joystick_in");
  d->joy_sub_ = nodeHandle().subscribe(joy_channel, 10, &GoalJoy::joystickCallback, this);
}

ds_nav_msgs::FlaggedDouble GoalJoy::calculateGoalForInput(JoystickBase::GoalDOF index,
                                                          const sensor_msgs::Joy& joy) const
{
  const DS_D(GoalJoy);

  auto result = ds_nav_msgs::FlaggedDouble{};
  result.valid = false;

  const auto input = d->joystick_input_map_.at(index);

  if (input & GoalJoy::JoystickInputType::BUTTON)
  {
    try
    {
      result.value = joy.buttons.at(input & 0x0FFF);
    }
    catch (std::out_of_range&)
    {
      ROS_ERROR("Button index: %d is outside of button array (size %ld)", input & 0x0FFF, joy.buttons.size());
      return result;
    }
  }
  else if (input & GoalJoy::JoystickInputType::AXIS)
  {
    try
    {
      result.value = joy.axes.at(input & 0x0FFF);
    }
    catch (std::out_of_range&)
    {
      ROS_ERROR("Axes index: %d is outside of axes array (size %ld)", input & 0x0FFF, joy.axes.size());
      return result;
    }
  }
  else
  {
    // No joystick input assigned for this DOF
    return result;
  }

  result.value = result.value;
  result.valid = true;
  return result;
}

ds_nav_msgs::AggregatedState GoalJoy::translateJoystick(const sensor_msgs::Joy& joy)
{
  DS_D(GoalJoy);

  auto ref = ds_nav_msgs::AggregatedState{};
  ref.header.stamp = ros::Time::now();
  ref.ds_header.io_time = ref.header.stamp;
  auto uuid_ = uuid();
  std::copy(std::begin(uuid_), std::end(uuid_), std::begin(ref.ds_header.source_uuid));

  ref.northing = calculateGoalForInput(GoalDOF::POSITION_U, joy);
  ref.easting = calculateGoalForInput(GoalDOF::POSITION_V, joy);
  ref.down = calculateGoalForInput(GoalDOF::POSITION_W, joy);

  ref.roll = calculateGoalForInput(GoalDOF::ROTATION_U, joy);
  ref.pitch = calculateGoalForInput(GoalDOF::ROTATION_V, joy);
  ref.heading = calculateGoalForInput(GoalDOF::ROTATION_W, joy);

  ref.surge_u = calculateGoalForInput(GoalDOF::VELOCITY_U, joy);
  ref.sway_v = calculateGoalForInput(GoalDOF::VELOCITY_V, joy);
  ref.heave_w = calculateGoalForInput(GoalDOF::VELOCITY_W, joy);

  ref.p = calculateGoalForInput(GoalDOF::ROTATION_RATE_U, joy);
  ref.q = calculateGoalForInput(GoalDOF::ROTATION_RATE_V, joy);
  ref.r = calculateGoalForInput(GoalDOF::ROTATION_RATE_W, joy);

  return ref;
}

void GoalJoy::joystickCallback(const sensor_msgs::Joy& joy)
{
  const auto ref = translateJoystick(joy);
  applyGainsAndPublish(ref);
}

void GoalJoy::setJoystickInputForGoal(GoalJoy::JoystickInput input, GoalJoy::GoalDOF dof)
{
  DS_D(GoalJoy);
  auto& val = d->joystick_input_map_.at(dof);
  val = input;
}
GoalJoy::JoystickInput GoalJoy::joystickInputForGoal(GoalJoy::GoalDOF dof) const noexcept
{
  const DS_D(GoalJoy);
  return d->joystick_input_map_.at(dof);
}

void GoalJoy::setJoystickInputForGoals(GoalJoy::JoystickInputMap input)
{
  DS_D(GoalJoy);
  d->joystick_input_map_ = input;
}

const GoalJoy::JoystickInputMap& GoalJoy::joystickInputForGoals() const noexcept
{
  const DS_D(GoalJoy);
  return d->joystick_input_map_;
}

void GoalJoy::setupParameters()
{
  JoystickBase::setupParameters();

  auto input_map = JoystickInputMap{};
  input_map[GoalDOF::POSITION_U] = from_string(ros::param::param<std::string>("~input/position_u", "NONE"));
  input_map[GoalDOF::POSITION_V] = from_string(ros::param::param<std::string>("~input/position_v", "NONE"));
  input_map[GoalDOF::POSITION_W] = from_string(ros::param::param<std::string>("~input/position_w", "NONE"));

  input_map[GoalDOF::VELOCITY_U] = from_string(ros::param::param<std::string>("~input/velocity_u", "NONE"));
  input_map[GoalDOF::VELOCITY_V] = from_string(ros::param::param<std::string>("~input/velocity_v", "NONE"));
  input_map[GoalDOF::VELOCITY_W] = from_string(ros::param::param<std::string>("~input/velocity_w", "NONE"));

  input_map[GoalDOF::ROTATION_U] = from_string(ros::param::param<std::string>("~input/rotation_u", "NONE"));
  input_map[GoalDOF::ROTATION_V] = from_string(ros::param::param<std::string>("~input/rotation_v", "NONE"));
  input_map[GoalDOF::ROTATION_W] = from_string(ros::param::param<std::string>("~input/rotation_w", "NONE"));

  input_map[GoalDOF::ROTATION_RATE_U] = from_string(ros::param::param<std::string>("~input/rotation_rate_u", "NONE"));
  input_map[GoalDOF::ROTATION_RATE_V] = from_string(ros::param::param<std::string>("~input/rotation_rate_v", "NONE"));
  input_map[GoalDOF::ROTATION_RATE_W] = from_string(ros::param::param<std::string>("~input/rotation_rate_w", "NONE"));

  setJoystickInputForGoals(input_map);
}

GoalJoy::JoystickInput from_string(std::string name)
{
  boost::trim(name);
  boost::to_upper(name);

  if (name == "A")
  {
    return GoalJoy::JoystickInput::A;
  }

  if (name == "B")
  {
    return GoalJoy::JoystickInput::B;
  }

  if (name == "X")
  {
    return GoalJoy::JoystickInput::X;
  }

  if (name == "Y")
  {
    return GoalJoy::JoystickInput::Y;
  }
  if (name == "LB")
  {
    return GoalJoy::JoystickInput::LB;
  }
  if (name == "RB")
  {
    return GoalJoy::JoystickInput::RB;
  }
  if (name == "BACK")
  {
    return GoalJoy::JoystickInput::BACK;
  }
  if (name == "START")
  {
    return GoalJoy::JoystickInput::START;
  }
  if (name == "POWER")
  {
    return GoalJoy::JoystickInput::POWER;
  }
  if (name == "BUTTON_STICK_LEFT")
  {
    return GoalJoy::JoystickInput::BUTTON_STICK_LEFT;
  }
  if (name == "BOTTON_STICK_RIGHT")
  {
    return GoalJoy::JoystickInput::BOTTON_STICK_RIGHT;
  }
  if (name == "LEFT_STICK_HORZ")
  {
    return GoalJoy::JoystickInput::LEFT_STICK_HORZ;
  }
  if (name == "LEFT_STICK_VERT")
  {
    return GoalJoy::JoystickInput::LEFT_STICK_VERT;
  }
  if (name == "TRIGGER_LEFT")
  {
    return GoalJoy::JoystickInput::TRIGGER_LEFT;
  }
  if (name == "RIGHT_STICK_HORZ")
  {
    return GoalJoy::JoystickInput::RIGHT_STICK_HORZ;
  }
  if (name == "RIGHT_STICK_VERT")
  {
    return GoalJoy::JoystickInput::RIGHT_STICK_VERT;
  }
  if (name == "TRIGGER_RIGHT")
  {
    return GoalJoy::JoystickInput::TRIGGER_RIGHT;
  }
  if (name == "DPAD_HORZ")
  {
    return GoalJoy::JoystickInput::DPAD_HORZ;
  }
  if (name == "DPAD_VERT")
  {
    return GoalJoy::JoystickInput::DPAD_VERT;
  }

  return GoalJoy::JoystickInput::NONE;
}
std::string to_string(GoalJoy::JoystickInput input)
{
  switch (input)
  {
    case (GoalJoy::JoystickInput::A):
      return "A";
    case (GoalJoy::JoystickInput::B):
      return "B";
    case (GoalJoy::JoystickInput::X):
      return "X";
    case (GoalJoy::JoystickInput::Y):
      return "Y";
    case (GoalJoy::JoystickInput::LB):
      return "LB";
    case (GoalJoy::JoystickInput::RB):
      return "RB";
    case (GoalJoy::JoystickInput::BACK):
      return "BACK";
    case (GoalJoy::JoystickInput::START):
      return "START";
    case (GoalJoy::JoystickInput::POWER):
      return "POWER";
    case (GoalJoy::JoystickInput::BUTTON_STICK_LEFT):
      return "BUTTON_STICK_LEFT";
    case (GoalJoy::JoystickInput::BOTTON_STICK_RIGHT):
      return "BOTTON_STICK_RIGHT";
    case (GoalJoy::JoystickInput::LEFT_STICK_HORZ):
      return "LEFT_STICK_HORZ";
    case (GoalJoy::JoystickInput::LEFT_STICK_VERT):
      return "LEFT_STICK_VERT";
    case (GoalJoy::JoystickInput::TRIGGER_LEFT):
      return "TRIGGER_LEFT";
    case (GoalJoy::JoystickInput::RIGHT_STICK_HORZ):
      return "RIGHT_STICK_HORZ";
    case (GoalJoy::JoystickInput::RIGHT_STICK_VERT):
      return "RIGHT_STICK_VERT";
    case (GoalJoy::JoystickInput::TRIGGER_RIGHT):
      return "TRIGGER_RIGHT";
    case (GoalJoy::JoystickInput::DPAD_HORZ):
      return "DPAD_HORZ";
    case (GoalJoy::JoystickInput::DPAD_VERT):
      return "DPAD_VERT";
    case (GoalJoy::JoystickInput::NONE):
      return "NONE";
    default:
      return "UNKNOWN";
  }
}

std::ostream& operator<<(std::ostream& os, const GoalJoy::JoystickInput& input)
{
  os << to_string(input);
  return os;
}
}
