/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef DS_CONTROL_PID_H
#define DS_CONTROL_PID_H

#include "ds_base/ds_global.h"
#include "ros/node_handle.h"
#include "ros/time.h"
#include <memory>
#include <control_msgs/PidState.h>

namespace ds_control
{
struct PidPrivate;

/// @brief A 1 DOF PID controller with optional thresholding.
///
/// This class provides a 1 DOF PID controller with optional thresholding.
/// There are two thesholds available to set:
///
/// ## Thresholding accumulated integration error.
///
/// Maximum and minimum error values can be provided to prevent integration windup.
/// These are applied after the accumulated error is calculated, but before the value
/// is used it the output calculation.  The threshold value is retained as the integrated
/// error for the next call.
///
/// ## Thresholding PID controller output.
///
/// Maximum and minimum output values can be provided to enable output "saturation".
///
/// ## Handling state discontinuities
///
/// This PID can accept an function that can be used to wrap the calculated error value
/// into a useful range.  The most common use for this feature is when using PID control
/// with heading, where the state can jump due to angular branch cuts.
class Pid
{
  DS_DECLARE_PRIVATE(Pid)
public:
  using PidGains = struct
  {
    double kp;
    double ki;
    double kd;
  };

  Pid();
  Pid(double kp, double ki, double kd);
  Pid(PidGains gains);
  Pid(double kp, double ki, double kd, double max, double min);
  Pid(PidGains gains, double max, double min);
  virtual ~Pid();

  Pid(const Pid& other);
  Pid(Pid&& other) noexcept;

  /// @brief Set the PID controller's proportional gain term
  ///
  /// \param kp
  void setKp(double kp);

  /// @brief Get the PID controller's proportional gain term
  ///
  /// \return
  double kp() const noexcept;

  /// @brief Set the PID controller's integral gain term
  ///
  /// \param ki
  void setKi(double ki);

  /// @brief Get the PID controller's integral gain term
  ///
  /// \return
  double ki() const noexcept;

  /// @brief Set the PID controller's derivative gain term
  ///
  /// \param kd
  void setKd(double kd);

  /// @brief Get the PID controller's derivative gain term
  ///
  /// \return
  double kd() const noexcept;

  /// @brief Set all three PID gains
  ///
  /// \param gains
  void setPidGains(PidGains gains);

  /// @brief Get all three PID gains
  ///
  /// \return
  PidGains pidGains() const noexcept;

  /// @brief Return the current internal pid state
  ///
  /// \return
  control_msgs::PidState pidState() const noexcept;

  /// @brief Set the maximum output produced buy the controller.
  ///
  /// The output will "saturate" at this value.
  ///
  /// \param max
  void setMaxSaturation(double max);

  /// @brief Get the maximum output produced by the controller
  ///
  /// If no maximum value is set the returned optional will evaluate
  /// false.
  ///
  /// \return
  double maxSaturation() const noexcept;

  /// @brief Clear the maximum output saturation limit.
  void clearMaxSaturation();

  /// @brief Set the minimum output produced buy the controller.
  ///
  /// The output will "saturate" at this value.
  ///
  /// \param max
  void setMinSaturation(double max);

  /// @brief Clear the minimum output saturation limit.
  ///
  void clearMinSaturation();

  /// @brief Get the minimum output produced by the controller
  ///
  /// \return
  double minSaturation() const noexcept;

  /// @brief Set the output deadband
  ///
  /// \param deadband
  void setOutputDeadband(double deadband);

  /// @brief Get the output deadband
  ///
  /// \return
  double outputDeadband() const noexcept;

  /// @brief Set the maximum allowed accumulated integration error
  ///
  /// Used to prevent integration windup.  Similar to the min/max saturation
  /// behavior, except applied to the accumulated error term in the PID loop.
  ///
  /// \param value
  void setMaxIntegratedError(double value);

  /// @brief Clear the maximum accumulated integration error limit
  ///
  /// Sets the max integrated error to inf
  void clearMaxIntegratedError();

  /// @brief Get the maximum allowed accumulated integration error.
  ///
  /// If no maximum value is set the returned optional will evaluate
  /// false.
  ///
  /// \return
  double maxIntegratedError() const noexcept;

  /// @brief Set the minimum allowed accumulated integration error
  ///
  /// Used to prevent integration windup.  Similar to the min/max saturation
  /// behavior, except applied to the accumulated error term in the PID loop.
  ///
  /// \param min
  void setMinIntegratedError(double value);

  /// @brief Clear the minimum accumulated integration error limit
  void clearMinIntegratedError();

  /// @brief Get the minimum allowed accumulated integration error.
  ///
  /// \return
  double minIntegratedError() const noexcept;

  /// @brief Apply a constant falloff factor to the integrated error.
  ///
  /// This value is applied to the error integrator for each step:
  ///
  /// integrated_error += error * value * dt
  ///
  /// The default value is 1 (no falloff)
  /// \param value
  void setIntegratedErrorFalloff(double value);

  /// @brief Get the constant falloff factor for the integrated error
  ///
  /// \return
  double integratedErrorFalloff() const noexcept;

  /// @brief Set a wrapping function for the error value in PID calculations.
  ///
  /// This function is applied to the error term and can be used to "wrap" values
  /// with branch cuts, such as radians for heading, ensuring smooth behavior.
  ///
  /// \param wrap_func
  void setErrorWrapper(std::function<double(double actual, double reference)> wrap_func);

  /// @brief Calculate PID output
  ///
  /// Calculates the PID controller output with estimated state first-diriviatves
  ///
  /// This method estimates the first dirivative as:
  ///
  ///   dt = timestamp - last_timestamp
  ///   x_dot = (x - x_ref) / dt;
  ///   x_dot_ref = previous_error / dt;
  ///
  /// \param timestamp
  /// \param x
  /// \param x_ref
  /// \return
  double calculate(double x, double x_ref, ros::Time timestamp);

  /// @brief Calculate PID output
  ///
  /// Same as Pid::calculate(ros::Time timestamp, double x, double x_ref), but uses
  /// the current time value.
  ///
  /// \param x
  /// \param x_ref
  /// \return
  double calculate(double x, double x_ref);

  /// @brief Calculate PID output with measured velocity error
  ///
  /// This method uses measured derivative states for the derivative term instead
  /// of the error derivative.
  ///
  /// So, instead of it being:
  ///     kd * (error - prev_error) / dt
  /// It is:
  ///     kd * (x_ref_dot - x_dot)
  ///
  /// \param x
  /// \param x_ref
  /// \param x_dot
  /// \param x_ref_dot
  /// \param dt
  /// \return
  double calculate_with_measured_rate(double x, double x_ref, double x_dot, double x_ref_dot, double dt);

  /// @brief Calculate PID output
  ///
  /// Calculates the PID controller output when the state first-derivatives are known values.
  /// Both the output and integrated error terms may be thresholded to min/max values
  /// if such values have been defined.
  ///
  /// Pseudo code:
  ///
  ///    x_error = error_wrapper(x - x_ref)
  ///    x_dot_error = (x_error - prev_x_error) / dt
  ///    integrated_error += x_error * dt
  ///    integrated_error = threshold(integrated_error, max_err_allowed, min_err_allowed)
  ///    output = Kp * x_error + Kd * x_dot_error + Ki * integrated_error
  ///    returned threshold(output, max_out_allowed, min_out_allowed)
  ///
  /// \param x            The current state value
  /// \param x_dot        The current state first-derivative value
  /// \param x_ref        The desired state value
  /// \param dt           The timestep size
  /// \return
  double calculate(double x, double x_ref, double dt);

  /// @brief Reset the PID controller.
  ///
  /// This method resets the PID controller.  Gains are retained, but
  /// the controller is returned to it's initial conditions:
  ///   - the integrated error is 0
  ///   - the previous error value is 0
  ///   - invalidates the last timestamp.

  void reset();

  /// @brief Set PID controller parameters from server
  ///
  /// Looks in the provided namespace to setup the PID controller.
  /// Equivalent to the following yaml:
  ///
  /// namespace:
  ///  gains:
  ///    kp:
  ///    ki:
  ///    kd:
  ///  integrated_error_limits:
  ///    min:
  ///    max:
  ///  integrated_error_falloff:
  ///  output_saturation_limits:
  ///    min:
  ///    max:
  ///
  /// \param ns
  void setupFromParameterServer(const std::string& ns);

  void enableStatePublishing(ros::NodeHandle& nh, const std::string& topic = std::string{ "pid_state" });

private:
  std::unique_ptr<PidPrivate> d_ptr_;
};
}
#endif  // DS_CONTROL_PID_H
