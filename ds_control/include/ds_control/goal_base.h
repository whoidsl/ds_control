/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_BASE_GOAL_BASE_H
#define DS_BASE_GOAL_BASE_H

#include "ds_base/ds_process.h"
#include "ds_nav_msgs/AggregatedState.h"
#include "ds_param/ds_param_conn.h"

namespace ds_control
{
struct GoalBasePrivate;

/// @brief Base class for goal generators in DS_ROS
///
/// Closed loop control systems typically operate on the error
/// signal defined as the difference between the current state
/// of the system and the desired state.
///
/// Goal generators produce the "desired state" portion of
/// the equation
///
/// # Parameters
///
/// In addition to the parameters used in DsProcess, GoalBase looks
/// for the following:
///
/// - `~goal_output_topic`   Default: "goal_state".
///   Goal messages are published on this topic.
///
/// # Published topics
///
/// - "~goal_state" (ds_nav_msgs::AggregatedState)
///   Can be changed using the `~goal_output_topic` parameter
///   This is a *private* topic.
class GoalBase : public ds_base::DsProcess
{
  DS_DECLARE_PRIVATE(GoalBase);

public:
  /// @brief Construct a new DsProcess
  ///
  /// When using this constructor you must call ros::init elsewhere in your code
  GoalBase();

  /// @brief Construct a new DsGoalBase
  ///
  /// This constructor calls ros::init(argc, argv, name) for you
  ///
  /// @param[in] argc
  /// @param[in] argv
  /// @param[in] name The name of the process type
  GoalBase(int argc, char** argv, const std::string& name);

  /// @brief Destroys a DsGoalBase
  ~GoalBase() override;
  DS_DISABLE_COPY(GoalBase)

  /// @brief Unique type ID for this controller
  ///
  /// The type ID is used for two reasons:
  ///
  ///   - Eases downcasting a base class to the derived class
  ///   - Enables context switching using a ds_param subscription
  ///
  /// \return
  virtual uint64_t type() const noexcept = 0;

  /// @brief Publish a new goal state
  ///
  /// \param goal
  void publishGoal(const ds_nav_msgs::AggregatedState& goal);

protected:
  void setupPublishers() override;

private:
  std::unique_ptr<GoalBasePrivate> d_ptr_;
};
}
#endif  // DS_BASE_GOAL_BASE_H
