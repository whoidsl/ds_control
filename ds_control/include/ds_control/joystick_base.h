/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_CONTROL_JOYSTICK_BASE_H
#define DS_CONTROL_JOYSTICK_BASE_H

#include "ds_control/goal_base.h"
#include "ds_param/ds_param_conn.h"
#include "ds_nav_msgs/FlaggedDouble.h"
#include "ds_nav_msgs/AggregatedState.h"

namespace ds_control
{
struct JoystickBasePrivate;

class JoystickBase : public ds_control::GoalBase
{
  DS_DECLARE_PRIVATE(JoystickBase);

public:
  struct GoalDOFSettings
  {
    GoalDOFSettings() : gain(1.0), bias(0.0)
    {
    }
    double gain;
    double bias;
  };

  enum GoalDOF
  {
    POSITION_U = 0,
    POSITION_V,
    POSITION_W,
    VELOCITY_U,
    VELOCITY_V,
    VELOCITY_W,

    ROTATION_U,
    ROTATION_V,
    ROTATION_W,
    ROTATION_RATE_U,
    ROTATION_RATE_V,
    ROTATION_RATE_W,
    MAX_DOF,
  };

  using GoalSettings = std::array<GoalDOFSettings, GoalDOF::MAX_DOF>;

  JoystickBase();

  JoystickBase(int argc, char** argv, const std::string& name);

  ~JoystickBase() override;
  DS_DISABLE_COPY(JoystickBase)

  virtual uint64_t type() const noexcept = 0;

  /// @brief Enable/Disable this allocation
  ///
  /// Goal outputs should only be sent if the joystick is enabled.  There should be
  /// only one joystick output
  ///
  /// \param enabled
  void setEnabled(bool enabled);

  /// @brief Return whether this joystick is enabled.
  bool enabled() const noexcept;

  void setAllocation(int value);
  int getAllocation() const;
  void setController(int value);
  int getController() const;

  /// @brief  Activate another joystick.
  ///
  /// Hand off control to another joystick, disabling yourself.
  ///
  /// \param joystick
  void switchJoystick(uint64_t joystick);

  void setBiasAndGains(const GoalSettings& settings);
  const GoalSettings& biasAndGains() const noexcept;

  void setBiasAndGain(GoalDOF index, double bias, double gain);
  GoalDOFSettings biasAndGain(GoalDOF index);

  ds_nav_msgs::FlaggedDouble applyGain(JoystickBase::GoalDOF index, ds_nav_msgs::FlaggedDouble in) const;

  ds_nav_msgs::AggregatedState applyGains(const ds_nav_msgs::AggregatedState& in);

  void applyGainsAndPublish(const ds_nav_msgs::AggregatedState& in);

protected:
  void setupSubscriptions() override;
  void setupParameters() override;
  virtual void parameterSubscriptionCallback(const ds_param::ParamConnection::ParamCollection& params);

  const ds_param::ParamConnection::Ptr& get_param_conn() const;

  /// @brief Called after the controller is enabled
  virtual void postEnableHook()
  {
  }

  /// @brief Called before the controller is disabled
  virtual void preDisableHook()
  {
  }

private:
  std::unique_ptr<JoystickBasePrivate> d_ptr_;
};
}
#endif  // DS_CONTROL_JOYSTICK_BASE_H
