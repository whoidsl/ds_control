/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_CONTROL_SURVEY_CONTROLLER_H
#define DS_CONTROL_SURVEY_CONTROLLER_H

#include "ds_control/controller_base.h"
#include "ds_control_msgs/ControllerEnum.h"
#include "ds_control_msgs/BottomFollow1D.h"

namespace ds_util
{
class TrapezoidalSmoother;
}

namespace ds_control
{
class Pid;

struct SurveyControllerPrivate;

class SurveyController : public ControllerBase
{
  DS_DECLARE_PRIVATE(SurveyController);

public:
  SurveyController();
  SurveyController(int argc, char** argv, const std::string& name);
  ~SurveyController() override;
  DS_DISABLE_COPY(SurveyController);

  uint64_t type() const noexcept override
  {
    return ds_control_msgs::ControllerEnum::SURVEY;
  }
  ///@brief Get the PID controller for heading
  ///
  /// \return
  Pid* pidHeading();

  ///@brief Get the heading goal smoother
  ///
  /// \return
  ds_util::TrapezoidalSmoother* headingReferenceSmoother() noexcept;

  /// @brief Get the PID controller for forward speed
  ///
  /// \return
  Pid* pidSurgeU();

  ///@brief Get the surge_u goal smoother
  ///
  /// \return
  ds_util::TrapezoidalSmoother* surgeUSmoother() noexcept;

  /// @brief Get the PID controller for vertical speed
  ///
  /// \return
  Pid* pidDown();

  /// @brief Get the depth smoother
  ///
  /// \return
  ds_util::TrapezoidalSmoother* depthReferenceSmoother() noexcept;

  double defaultDepthSmootherVelocity() const noexcept;
  double defaultDepthSmootherAcceleration() const noexcept;

  geometry_msgs::WrenchStamped calculateForces(ds_nav_msgs::AggregatedState reference,
                                               ds_nav_msgs::AggregatedState state, ros::Duration dt) override;
  void updateState(const ds_nav_msgs::AggregatedState& msg) override;

  ds_nav_msgs::AggregatedState errorState(const ds_nav_msgs::AggregatedState& ref,
                                          const ds_nav_msgs::AggregatedState& state) override;

  void setup() override;

  void reset() override;

protected:
  /// @brief Setup additional parameters from the parameter server
  ///
  /// Gains for the closed-loop heading PID controllers are specified using
  /// the private namespaces: `~heading_pid`, `~surge_u_pid`, and `~down_pid`
  ///
  /// For example, the full heading pid parameter space looks like:
  ///
  /// ~heading_pid/
  ///    gains/
  ///      kp
  ///      ki
  ///      kd
  ///    integrated_error_limits/
  ///      min
  ///      max
  ///    output_saturation_limits/
  ///      min
  ///      max
  ///
  /// Smoothers for the heading, surge_u, and down axes each have maximum velocity and
  /// acceleration parameters:
  ///
  /// ~smoothers/
  ///    heading/
  ///      max_velocity
  ///      max_accel
  ///    surge_u/
  ///      max_velocity
  ///      max_accel
  ///    down/
  ///      max_velocity
  ///      max_accel
  ///
  /// The velocity and acceleration for the 'down' smoother are used when the corresponding
  /// parameters (depth_rate_d, depth_accel_d) in BottomFollower1d.msg are 0
  void setupParameters() override;

  /// @brief Setup reference sources for the controller.
  ///
  /// The survey controller expects two references:
  ///
  /// - 'leg' of type goal_leg::GoalLeg
  /// - 'bottom_follow' of type ds_control::GoalBottomFollowDepth
  void setupReferences() override;

  virtual void legGoalCallback(const ds_nav_msgs::AggregatedState& msg);
  virtual void bottomFollowerGoalCallback(const ds_nav_msgs::AggregatedState& msg);
  virtual void bottomFollowerStateCallback(const ds_control_msgs::BottomFollow1D& msg);

private:
  std::unique_ptr<SurveyControllerPrivate> d_ptr_;
};
}
#endif  // DS_CONTROL_SURVEY_CONTROLLER_H
