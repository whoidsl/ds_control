//
// Created by ivaughn on 3/12/19.
//

#ifndef DS_CONTROL_BODY_LIMITS_H
#define DS_CONTROL_BODY_LIMITS_H

#include <ds_base/ds_global.h>
#include <memory>
#include <ros/node_handle.h>

namespace ds_control {

struct BodyLimitsPrivate;

/// @brief A class to simplify loading and using body-frame limits
/// on force and velocity.  There's just a ton of overhead here,
/// and we may need it multiple places.  So centralize!
class BodyLimits {
  DS_DECLARE_PRIVATE(BodyLimits)

 public:
  BodyLimits();
  virtual ~BodyLimits();

  enum  Axis {
    FWD=0,
    STBD=1,
    DOWN=2,
    HEADING=3,
    N_AXIS=4,
  };

  double maxForce(Axis axis) const;
  double minForce(Axis axis) const;
  double minAbsForce(Axis axis) const;
  double maxAbsForce(Axis axis) const;
  double clampForce(Axis axis, double force) const;

  double maxVelocity(Axis axis) const;
  double minVelocity(Axis axis) const;
  double minAbsVelocity(Axis axis) const;
  double maxAbsVelocity(Axis axis) const;
  double clampVelocity(Axis axis, double velocity) const;

  void loadParameters(ros::NodeHandle& handle, const std::string& dynamics_ns);

 private:
  std::unique_ptr<BodyLimitsPrivate> d_ptr_;
};

}

#endif //DS_CONTROL_BODY_LIMITS_H
