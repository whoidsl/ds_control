/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_CONTROL_JOYSTICK_CONTROLLER_H
#define DS_CONTROL_JOYSTICK_CONTROLLER_H

#include "ds_control/controller_base.h"
#include "ds_control_msgs/ControllerEnum.h"
#include "ds_base/BoolCommand.h"
#include "geometry_msgs/Wrench.h"

namespace ds_control
{
class Pid;

struct JoystickControllerPrivate;

/// @brief 3-Axis controller for on-surface operations.
///
/// This controller provides open-loop control for the U and W DOF's, and
/// switchable closed- or open-loop control for heading (W-torque)
///
///
/// The controller emits it's body-frame forces different ways depending on
/// whether auto-heading is enabled or disabled:
///
/// - When auto-heading is DISABLED, forces are produced with each call to
///   updateReference()
///
/// - when auto-heading is ENABLED, forces are produced with each call to
///   updateState()
class JoystickController : public ControllerBase
{
  DS_DECLARE_PRIVATE(JoystickController)

public:
  JoystickController();
  JoystickController(int argc, char** argv, const std::string& name);
  ~JoystickController() override;

  uint64_t type() const noexcept override
  {
    return ds_control_msgs::ControllerEnum::JOYSTICK;
  }
  geometry_msgs::WrenchStamped calculateForces(ds_nav_msgs::AggregatedState reference,
                                               ds_nav_msgs::AggregatedState state, ros::Duration dt) override;

  /// @brief Set the PID gains for the heading DOF
  ///
  /// \param kp
  /// \param ki
  /// \param kd
  void setAutoHeadingGains(double kp, double ki, double kd);

  /// @brief Return the PID gains for the heading DOF
  ///
  ///
  /// \return  kp, ki, kd
  std::tuple<double, double, double> autoHeadingGains() const noexcept;

  /// @brief Enable/Disable auto-heading
  ///
  /// \param enable
  void setAutoHeadingEnabled(bool enable);

  /// @brief Enable/Disable auto-heading
  ///
  /// \param enable
  bool setAutoHeadingEnabled(ds_base::BoolCommand::Request& request, ds_base::BoolCommand::Response& res);

  bool autoHeadingEnabled() const noexcept;

  /// @brief Set the heading to hold when auto-heading mode is engaged
  ///
  /// \param heading
  void setAutoHeadingValueRadians(double radians);

  /// @brief Get the heading value to hold when auto-heading mode is engaged.
  ///
  /// \return
  double autoHeadingValueRadians() const noexcept;

  /// @brief Set the deadband value for auto-heading.
  ///
  /// Auto-heading will only be enforced if the last reference heading
  /// magintude is less than this value.
  ///
  /// \param deadband
  void setHeadingRateDeadband(double deadband);

  /// @brief Get the deadband value for auto-heading.
  double headingRateDeadband() const noexcept;

  ///
  /// The state message will also update the reference heading if EITHER of
  /// the following conditions apply:
  ///   - auto-heading is disabled OR
  ///   - the reference heading rate is greater than the heading rate deadband
  ///
  /// Then, if auto-heading is ENABLED, executeController() is called to produce
  /// forces.
  ///
  /// \param msg
  void updateState(const ds_nav_msgs::AggregatedState& msg) override;

  ///
  /// If auto-heading is DIABLED, executeController() is called to produce forces.
  ///
  /// This way we can produce actuator commands in pure open-loop schemes that have
  /// no need for estimated state
  ///
  /// \param msg
  void updateReference(const ds_nav_msgs::AggregatedState& msg) override;

  Pid* pidHeading();

  ds_nav_msgs::AggregatedState errorState(const ds_nav_msgs::AggregatedState& ref,
                                          const ds_nav_msgs::AggregatedState& state) override;

  void setForceScale(const geometry_msgs::Wrench& scale);
  geometry_msgs::Wrench forceScale() const noexcept;

  void setup() override;

  void reset() override;

protected:
  /// @brief Setup references
  ///
  /// This controller looks for a single reference named "joystick" that has a
  /// private topic named "reference_state" that emits a ds_nav_msgs::AggregatedState
  /// message.
  void setupReferences() override;

  /// @brief Update the controller reference
  ///
  /// Uses a reference generated by a user joystick to set the controller reference.
  ///
  /// Sets the reference U, W, and heading-rate parameters.
  /// Sets the reference's heading parameter from the last state estimate IF auto-heading
  /// is enabled AND the reference's heading rate is greater than the deadband value.
  ///
  /// \param msg
  virtual void joystickReferenceCallback(const ds_nav_msgs::AggregatedState& msg);

  /// @brief Additional controller parameters
  ///
  /// Gains for the closed-loop heading PID controller are specified using
  /// the `~heading_pid` namespace:
  ///
  /// ~heading_pid/
  ///    gains/
  ///      kp
  ///      ki
  ///      kd
  ///    integrated_error_limits/
  ///      min
  ///      max
  ///    output_saturation_limits/
  ///      min
  ///      max
  void setupParameters() override;

  void setupServices() override;

private:
  std::unique_ptr<JoystickControllerPrivate> d_ptr_;
};
}
#endif  // DS_CONTROL_JOYSTICK_CONTROLLER_H
