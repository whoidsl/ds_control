/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_BASE_CONTROLLERBASE_H
#define DS_BASE_CONTROLLERBASE_H

#include "ds_control/allocation_base.h"
#include "ds_base/ds_process.h"
#include "ds_nav_msgs/AggregatedState.h"
#include "ds_param/ds_param_conn.h"

#include "geometry_msgs/Wrench.h"
#include "geometry_msgs/WrenchStamped.h"

namespace ds_control
{
struct ControllerBasePrivate;

/// @brief Base class for controllers.
///
/// # Parameters
///
/// `ControllerBase` looks for the following parameters:
///
/// ## Private parameters
///   `~state_input_topic`  defaults to `state_input`
///   `~frame_id` defaults to `base_link`
///   `~references` a string:string map of reference types/node names.
///
/// ## Namespace parameters
///   `reference_ns` defaults to "", see the References section.
///
/// # Parameter Subscriptions
///
/// References all monitor the *namespace* integer parameter `active_reference`.
/// When this parameter changes the new value is checked against the value
/// returned by the `type()` method.  The controller is enabled if they match,
/// disabled if they don't.
///
/// # Topics
///
/// ## Subscriptions
/// `ControllerBase` automatically subscribes to the following topics:
///
///   - `state_input`     (`ds_nav_msgs::AggregatedState`):  State update messages
///
/// ## Publishers
///
///  Controllers by default publish 6 topics that are split into two groups: private and public.
///
///   - Public topics are published in the node's namespace only when the controller is enabled.  This allows
///     downstream consumers (e.g. allocations) to subscribe to a single set of topics without having to subscribe
///     to each individual controller.
///
///   - Private topics are published in the node name's namespace *at all times*
///
///  The private topics are intended for introspection and logging purposes.  The public topics are
///  intended to be the actual working output of the system at the current time.
///
///  There are three topic types, and each type has a public and private variant:
///   - `wrench` (`geometry_msgs::WrenchStamped`):  Output body-frame forces
///   - `reference` (`ds_nav_msgs::AggregatedState`):  Output the current reference values
///   - `error` (`ds_nav_msgs::AggregatedState`):  Output the current error values (reference - state)
///
/// # References
///
/// Most controllers require at least one reference to work.  Making those connections is handled
/// by the `~references` and `reference_ns` parameters, and the `setupReferences()` method.
///
/// The `~references` parameter should provide a string:string mapping of reference name/node name
/// pairs.  For example:
///
///   ~references
///     - joystick: /some/namespace/with/joystick_node
///
/// Alternatively, you can also provide the *non-private* parameter `reference_ns` to provide the
/// namespace for the reference nodes.  For the same example:
///
///   reference_ns: /some/namespace/with
///   ~references:
///     - joystick: joystick_node
///
/// The reference map is stored for use by derived classes to actually handle the logic of connecting
/// to the references.  This should happen in the `setupReferences`
///
class ControllerBase : public ds_base::DsProcess
{
  DS_DECLARE_PRIVATE(ControllerBase)

public:
  using ReferenceType = std::string;
  using ReferenceName = std::string;
  using ReferenceMap = std::map<ReferenceType, ReferenceName>;

  struct ForceOutputLimits
  {
    ForceOutputLimits()
    {
      const auto limit = std::numeric_limits<double>::infinity();
      min.force.x = -limit;
      min.force.y = -limit;
      min.force.z = -limit;

      min.torque.x = -limit;
      min.torque.y = -limit;
      min.torque.z = -limit;

      max.force.x = limit;
      max.force.y = limit;
      max.force.z = limit;

      max.torque.x = limit;
      max.torque.y = limit;
      max.torque.z = limit;
    }
    geometry_msgs::Wrench min;
    geometry_msgs::Wrench max;
  };

  ///@brief ControllerBase constructor
  ///
  /// You must call `ros::init` separately when using this method.
  explicit ControllerBase();

  /// @brief ControllerBase constructor
  ///
  /// Calls ros::init(argc, argv, name) for you.
  ///
  /// \param argc
  /// \param argv
  /// \param name
  ControllerBase(int argc, char* argv[], const std::string& name);
  ~ControllerBase() override;

  DS_DISABLE_COPY(ControllerBase);

  /// @brief Unique type ID for this controller
  ///
  /// The type ID is used for two reasons:
  ///
  ///   - Eases downcasting a base class to the derived class
  ///   - Enables context switching using a ds_param subscription
  ///
  /// \return
  virtual uint64_t type() const noexcept = 0;

  /// @brief Calculate body-frame forces
  ///
  /// This is called within the `executeController()` method and should
  /// return the desired body-frame forces that will be fed to sentry's
  /// thrusters and servos.
  ///
  /// This is where the bulk of controller specialization will occour
  ///
  /// \param reference
  /// \param state
  /// \param dt
  /// \return
  virtual geometry_msgs::WrenchStamped calculateForces(ds_nav_msgs::AggregatedState reference,
                                                       ds_nav_msgs::AggregatedState state, ros::Duration dt) = 0;

protected:
  /// @brief Setup reference subscriptions
  ///
  /// All controllers need at least one reference source (many have multiple
  /// references sources)
  ///
  /// This is an additional setup step that happens after DsProcess::setup()
  ///
  virtual void setupReferences() = 0;

  /// @brief Called after the controller is enabled
  virtual void postEnableHook()
  {
  }

  /// @brief Called before the controller is disabled
  virtual void preDisableHook()
  {
  }

public:
  /// @brief Enable controller output
  ///
  /// \param enabled
  virtual void setEnabled(bool enabled);

  /// @brief Is controller output enabled?
  ///
  /// \return
  virtual bool enabled() const noexcept;

  /// @brief Set the frame id string for outgoing WrenchStamped messages
  ///
  /// The default value is "base_link"
  ///
  /// \param name
  void setWrenchFrameId(const std::string& name);

  /// @brief get the frame id string for outgoing WrenchStamped messages
  ///
  /// The default value is "base_link"
  ///
  /// \return
  std::string wrenchFrameId() const noexcept;

  /// @brief Callback fired upon receipt of new reference messages
  ///
  /// Default implementation stores the reference message, and can be
  /// retrieved using the `reference()` method
  ///
  /// \param msg
  virtual void updateReference(const ds_nav_msgs::AggregatedState& msg);

  /// @breif Retrieve the last recorded reference message.
  ///
  /// Returns the last reference message stored using `setReference()`
  ///
  /// \return
  const ds_nav_msgs::AggregatedState& reference() const noexcept;

  /// @brief  Callback fired upon receipt of new state messages
  ///
  /// Default implementation stores the state message, and can be
  /// retrieved using the `state()` method
  ///
  /// \param msg
  virtual void updateState(const ds_nav_msgs::AggregatedState& msg);

  /// @brief Retrieve the last recorded state message.
  ///
  /// Returns the last state message stored using `setState()`
  ///
  /// \return
  const ds_nav_msgs::AggregatedState& state() const noexcept;

  /// @brief  Activate another controller.
  ///
  /// Hand off control to another controller, disabling yourself.
  ///
  /// \param controller
  void switchController(uint64_t controller);

  /// @brief Setup controller override.
  void setup() override;

  /// @brief Get the active allocation ID
  ///
  /// \return
  size_t activeAllocation() const noexcept;

  /// @brief Get the active controller ID
  size_t activeController() const noexcept;

  /// @brief Switch the active transfer function by ID
  ///
  /// You can switch transfer functions while disabled.  The new allocation only becomes active if the controller
  /// is also active.
  ///
  /// Returns false if no transfer function exists for the given ID.
  ///
  /// \param id
  /// \return
  // TODO: This should now return void.
  virtual bool switchAllocation(size_t id);

  /// @brief The map of reference type/reference name pairings.
  ///
  /// These parings are loaded from the parameter server under the
  /// private `~references` parameter.
  ///
  /// The keys of the map are reference "types" which the controller may
  /// impose logic on when creating a reference message from multiple
  /// reference sources.
  ///
  /// The values are the actual node names for the reference node generating
  /// this type of reference.
  ///
  /// \return
  const ReferenceMap& referenceMap() const noexcept;

  /// @brief Publish the provided forces
  ///
  /// Forces are always published on the private wrench topic.  Forces are only
  /// published on the non-private wrench topic if the controller is enabled.
  ///
  /// \param body_forces
  void publishBodyForces(geometry_msgs::WrenchStamped body_forces);

  /// @brief Get the current error between reference & estimated state
  ///
  /// \param ref
  /// \param state
  /// \return
  virtual ds_nav_msgs::AggregatedState errorState(const ds_nav_msgs::AggregatedState& ref,
                                                  const ds_nav_msgs::AggregatedState& state);

  /// @brief Reset the controller
  ///
  /// The default behavior is to clear the current values of state and reference.
  virtual void reset();

protected:
  /// @brief Setup parameters
  ///
  /// ControllerBase adds hooks to create the mappings returned by referenceMap() here.
  void setupParameters() override;

  /// @brief Setup subscriptions.
  ///
  /// The AllocationBase class sets up the following additional subscriptions:
  ///
  /// - `~state_input_topic`: The topic name for incoming ds_nav_msgs/AggregatedState messages for nav state.
  /// - `active_controller`: A `ds_param` parameter subscription of type 'int'.
  /// - `active_allocation`: A `ds_param` parameter subscription of type 'int'.
  void setupSubscriptions() override;

  /// @brief Setup publications.
  ///
  /// Sets up the controllers three types of output:
  ///
  /// - `wrench`:  produced body-force wrenches when the controller is active
  /// - `~wrench`: private wrench topic, always published.
  /// - `reference`:  reference used by the controller when active.
  /// - `~reference`: reference used by the controller, always published.
  /// - `error`:  error state (reference - state) used by controller when active.
  /// - `~error`: error state (reference - state) used by controller, always published.
  void setupPublishers() override;

  /// @brief Access the ds_param::ParamConnection object.
  ///
  /// Use this method to access the underlying ds_param::ParamConnectoin object to add additional
  /// parameter subscriptions.
  ///
  /// \return
  ds_param::ParamConnection* parameterSubscription();

  /// @brief Callback triggered by updates to subscribed parameters.
  ///
  /// The default behavior enables/disables the reference based on the value
  /// of the `active_reference` parameter
  ///
  /// \param params
  virtual void parameterSubscriptionCallback(const ds_param::ParamConnection::ParamCollection& params);

  /// @brief Execute the controller and issue new actuator commands.
  ///
  /// The default behavior is to do the following steps:
  ///
  /// - use `calculateForces()` to produce the desired body-frame forces
  /// - publish the forces on the private wrench topic
  /// - publish the error on the private error topic
  /// - ** if disabled, nothing further to be done. **
  /// - calculate the required thruster and servo parameters from the active
  ///   thruster allocation
  /// - publish the forces on the 'active' wrench topic
  /// - publish the error on the 'active' error topic
  virtual void executeController(ds_nav_msgs::AggregatedState reference, ds_nav_msgs::AggregatedState state,
                                 ros::Duration dt);

  /// @brief Publish error state
  ///
  /// Publishes the provided error state on public and private topics:
  ///
  /// `error`: only when the controller is active
  /// `~error`: always
  ///
  /// \param msg
  virtual void publishErrorState(const ds_nav_msgs::AggregatedState& msg);

  /// @brief Publish reference state
  ///
  /// Publishes the provided reference state on public and private topics:
  ///
  /// `reference`: only when the controller is active
  /// `~reference`: always
  ///
  /// \param msg
  virtual void publishReference(const ds_nav_msgs::AggregatedState& msg);

  /// @brief Get a reference to the current reference state
  ///
  /// This allows modification of the current reference value without calling
  /// updateReference() which may have side effects.
  /// \return
  ds_nav_msgs::AggregatedState& referenceRef();

  /// @brief Get a reference to the current state
  ///
  /// This allows modification of the current state value without calling
  /// updateState() which may have side effects.
  /// \return
  ds_nav_msgs::AggregatedState& stateRef();

private:
  std::unique_ptr<ControllerBasePrivate> d_ptr_;
};
}
#endif  // DS_BASE_CONTROLLERBASE_H
