/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_BASE_ALLOCATION_BASE_H
#define DS_BASE_ALLOCATION_BASE_H

#include "ds_base/ds_process.h"
#include "ds_param/ds_param_conn.h"

#include "ds_nav_msgs/AggregatedState.h"

#include "geometry_msgs/WrenchStamped.h"
#include "geometry_msgs/Wrench.h"

namespace ds_control
{
struct AllocationBasePrivate;

/// @brief Body-frame force to actuator commands
///
/// Allocations take the desired body-frame force produced by a controller and publish a set of actuator command
/// messages
/// to realize that force.  A single vehicle may have multiple allocations depending on the desired controll affect.
///
/// `ds_control::AllocationBase` has a core set of concepts:
///
/// - A constant number of actuator inputs for a class of allocations (e.g. NUM_ACTUATOR_INPUTS)
/// - TransferFunctions produce a vector of doubles of size NUM_ACTUATOR_INPUTS that can be used to construct actual
///   actuator commands.
/// - A set of limits, both on the input force and the output control values, that keep quantities within acceptable
///   ranges.
///
/// The recommended way of creating vehicle transfer functions is a two step process:
///
/// 1. Define a base class that derives from `ds_control::ActuatorBase` that provides a set of constants, common
/// methods,
/// and actuator indexcing values.  This maintains consistency across allocations and actuator-based parameter.
/// You want all of this indexing to be done in *one* place, so if  you end up changing things (add a new thruster,
/// for example) you don't have to track down a bunch of indexing issues in other bits of code.
///
/// 2. Specialize the class you create in 1. to create the actual force-to-command publications by overriding the
/// `publishActuatorCommands()` method.
///
/// If your vehicle only ever has one allocation you can combine points 1. and 2. into a single class.  However, for
/// some vehicles (like the NDSF Sentry which drove the development of this scheme) there may be multiple allocations
/// defined with various gains and limits.  In these cases there will be one base class (#1) and many derived classes
/// (#2).
///
/// Examples are a bit outside of the scope of this readme, but you can check out how Sentry created their transfer
/// functions (they call them allocations btw) in the `sentry_control` package.  You'll want to look at:
///
/// - `sentry_control::SentryThrusterAllocation`:  This is the base class outlined by #1 above.
/// - `sentry_control::SentryVerticalAllocation`:  A transfer function with wings vertical
/// - `sentry_control::SentryRovAllocation`:  A transfer function with the forward wing vertical, aft horizontal.
/// - `sentry_control::SentryFlightAllocation`:  A transfer function with the wings responding to desired upward forces.
class AllocationBase : public ds_base::DsProcess
{
  DS_DECLARE_PRIVATE(AllocationBase);

public:
  /// @brief A unique type ID for the allocation.
  ///
  /// Many controllers can have multiple allocations that are activated/deactivated depending
  /// on what the controller is trying to achieve.  This ID should be unique across allocations running
  /// within the ROS environment so that the controller can go get the allocation it wants.
  ///
  /// \return
  virtual uint64_t type() const = 0;

  struct ActuatorControlLimit
  {
    ActuatorControlLimit() : max(std::numeric_limits<double>::infinity()), min(-std::numeric_limits<double>::infinity())
    {
    }
    double min;
    double max;
  };

  /// @brief Force input limits.
  struct ForceInputLimits
  {
    ForceInputLimits()
    {
      const auto limit = std::numeric_limits<double>::infinity();
      min.force.x = -limit;
      min.force.y = -limit;
      min.force.z = -limit;

      min.torque.x = -limit;
      min.torque.y = -limit;
      min.torque.z = -limit;

      max.force.x = limit;
      max.force.y = limit;
      max.force.z = limit;

      max.torque.x = limit;
      max.torque.y = limit;
      max.torque.z = limit;
    }
    geometry_msgs::Wrench min;
    geometry_msgs::Wrench max;
  };

  ///@brief ControllerBase constructor
  ///
  /// You must call `ros::init` separately when using this method.
  explicit AllocationBase();

  /// @brief AllocationBase constructor
  ///
  /// Calls ros::init(argc, argv, name) for you.
  ///
  /// \param argc
  /// \param argv
  /// \param name
  AllocationBase(int argc, char* argv[], const std::string& name);
  ~AllocationBase() override;
  DS_DISABLE_COPY(AllocationBase);

  /// @brief Publish the actuator commands to realize the desired body-frame forces.
  ///
  /// IMPORTANT NOTE:  This method should ONLY publish commands if the allocation is
  /// enabled!
  ///
  /// \param body_wrench
  virtual void publishActuatorCommands(geometry_msgs::WrenchStamped body_wrench) = 0;

  /// @brief Enable/Disable this allocation
  ///
  /// Actuator commands should only be sent if the allocation is enabled.  There should be
  /// only one allocation for a given set of actuators enabled at any one time.
  ///
  /// \param enabled
  void setEnabled(bool enabled);

  /// @brief Return whether this allocation is enabled.
  bool enabled() const noexcept;

  /// @brief Get input force limits
  ///
  /// These limits can be used to clip the input force within `calculateActuatorInputs()`
  ///
  /// The default limits rae effectively unbounded with each force and torque limit set to -inf and inf.
  ///
  /// \return
  const ForceInputLimits& forceInputLimits() const;

  /// @brief Set input force limits
  ///
  /// These limits can be used to clip the input force within `publishActuatorCommands()`
  ///
  /// The default limits are effectively unbounded with each force and torque limit set to -inf and inf.
  ///
  /// \return
  void setForceInputLimits(ForceInputLimits limits);

  /// @brief Convenience method to clip wrench forces using the force input limits.
  ///
  /// Returns a new wrench message with all forces and torques clipped using the input force limits.
  ///
  /// \param wrench
  /// \return
  geometry_msgs::Wrench applyForceInputLimits(geometry_msgs::Wrench wrench);

  /// @brief A hook to update the allocation in response to new reference values used by the controller.
  ///
  /// \param ref
  virtual void referenceUpdate(const ds_nav_msgs::AggregatedState& ref)
  {
  }

  /// @brief A hook to update the transfer function responding to new estimated state values
  ///
  /// \param ref
  virtual void stateUpdate(const ds_nav_msgs::AggregatedState& state)
  {
  }

  /// @brief Setup the allocation.
  ///
  /// The AllocationBase setup directive adds an additional step that will enable the allocation
  /// if the subscribed active_allocation parameter matches the value returned by type()
  void setup() override;

protected:
  /// @brief Setup subscriptions.
  ///
  /// The AllocationBase class sets up the following additional subscriptions:
  ///
  /// - A subscription to the geometry_msgs::WrenchStamped on $controller_ns/wrench, where
  ///   $controller_ns is a namespace parameter that defaults to "/"
  /// - A `ds_param` parameter subscription of type 'int' to namespace parameter `active_controller`
  /// - A `ds_param` parameter subscription of type 'int' to namespace parameter `active_allocation`
  void setupSubscriptions() override;

  /// @brief Access the ds_param::ParamConnection object.
  ///
  /// Use this method to access the underlying ds_param::ParamConnectoin object to add additional
  /// parameter subscriptions.
  ///
  /// \return
  ds_param::ParamConnection* parameterSubscription();

  /// @brief ds_param::ParamConnection callback.
  ///
  /// The default behavior checks updates to the `active_allocation` subscription and enables/disables
  /// this allocation by matching the parameter's value to the value returned by the 'type()' method.
  /// \param params
  virtual void parameterSubscriptionCallback(const ds_param::ParamConnection::ParamCollection& params);

private:
  std::unique_ptr<AllocationBasePrivate> d_ptr_;
};
}
#endif  // DS_BASE_ALLOCATION_BASE_H
