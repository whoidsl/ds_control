/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_control/controller_base.h"
#include "ds_control/allocation_base.h"

#include "ds_util/angle.h"
#include "controller_base_private.h"

#include "ds_param/ds_param_conn.h"
#include "geometry_msgs/WrenchStamped.h"

namespace ds_control
{
ControllerBase::ControllerBase()
  : DsProcess(), d_ptr_(std::unique_ptr<ControllerBasePrivate>(new ControllerBasePrivate))
{
}
ControllerBase::ControllerBase(int argc, char** argv, const std::string& name)
  : DsProcess(argc, argv, name), d_ptr_(std::unique_ptr<ControllerBasePrivate>(new ControllerBasePrivate))
{
}

ControllerBase::~ControllerBase() = default;

void ControllerBase::setEnabled(bool enabled)
{
  DS_D(ControllerBase);

  // Call the pre-disable hook if we're turning off
  if (!enabled && d->is_enabled_)
  {
    preDisableHook();
  }

  // Switch the active controller ds param if we need to.
  if (enabled)
  {
    const auto type_ = static_cast<int>(type());
    if (type_ != d->active_controller_->Get())
    {
      d->active_controller_->Set(type_);
    }
  }

  // Call the post-enable hook if we're turning on.
  if (enabled && !d->is_enabled_)
  {
    d->is_enabled_ = enabled;
    postEnableHook();
    return;
  }

  d->is_enabled_ = enabled;
}

bool ControllerBase::enabled() const noexcept
{
  const DS_D(ControllerBase);
  return d->is_enabled_;
}

const ds_nav_msgs::AggregatedState& ControllerBase::reference() const noexcept
{
  const DS_D(ControllerBase);
  return d->last_reference_;
}

ds_nav_msgs::AggregatedState& ControllerBase::referenceRef()
{
  DS_D(ControllerBase);
  return d->last_reference_;
}

void ControllerBase::updateReference(const ds_nav_msgs::AggregatedState& msg)
{
  DS_D(ControllerBase);
  d->last_reference_ = msg;
  // publishReference(msg);
}

const ds_nav_msgs::AggregatedState& ControllerBase::state() const noexcept
{
  const DS_D(ControllerBase);
  return d->last_state_;
}

ds_nav_msgs::AggregatedState& ControllerBase::stateRef()
{
  DS_D(ControllerBase);
  return d->last_state_;
}

void ControllerBase::updateState(const ds_nav_msgs::AggregatedState& msg)
{
  DS_D(ControllerBase);
  d->last_state_ = msg;
}

void ControllerBase::switchController(uint64_t controller)
{
  setEnabled(controller == type());
  DS_D(ControllerBase);
  d->active_controller_->Set(static_cast<int>(controller));
}

void ControllerBase::setupSubscriptions()
{
  DsProcess::setupSubscriptions();
  DS_D(ControllerBase);
  const auto state_input_topic = ros::param::param<std::string>("~state_input_topic", "estimated_state");

  auto nh = nodeHandle();
  d->state_update_sub_ = nh.subscribe(state_input_topic, 1, &ControllerBase::updateState, this);

  d->param_sub_ = ds_param::ParamConnection::create(nh);
  d->active_controller_ = d->param_sub_->connect<ds_param::IntParam>("active_controller");
  d->active_allocation_ = d->param_sub_->connect<ds_param::IntParam>("active_allocation");
  d->param_sub_->setCallback(boost::bind(&ControllerBase::parameterSubscriptionCallback, this, _1));
}

void ControllerBase::setup()
{
  DsProcess::setup();
  setupReferences();
  DS_D(ControllerBase);
  if (!d->active_controller_)
  {
    ROS_FATAL("Parameter subscription for active controller is null!");
    ROS_BREAK();
  }
  setEnabled(d->active_controller_->Get() == type());
}

void ControllerBase::setupPublishers()
{
  DsProcess::setupPublishers();
  DS_D(ControllerBase);
  auto nh = nodeHandle();
  d->wrench_pub_ = nh.advertise<geometry_msgs::WrenchStamped>("wrench", 10, false);
  d->wrench_priv_pub_ = nh.advertise<geometry_msgs::WrenchStamped>(ros::this_node::getName() + "/wrench", 10, false);

  d->reference_pub_ = nh.advertise<ds_nav_msgs::AggregatedState>("reference", 10, false);
  d->reference_priv_pub_ =
      nh.advertise<ds_nav_msgs::AggregatedState>(ros::this_node::getName() + "/reference", 10, false);

  d->error_pub_ = nh.advertise<ds_nav_msgs::AggregatedState>("error", 10, false);
  d->error_priv_pub_ = nh.advertise<ds_nav_msgs::AggregatedState>(ros::this_node::getName() + "/error", 10, false);
}

void ControllerBase::setupParameters()
{
  DsProcess::setupParameters();

  const auto reference_ns = ros::param::param<std::string>("reference_ns", "");
  DS_D(ControllerBase);
  if (!ros::param::get("~references", d->reference_map_))
  {
    ROS_WARN_STREAM("Unable to load reference map from " << ros::this_node::getName() << "/references");
  }

  // Make names absolute if a reference namespace is provided.
  for (auto& it : d->reference_map_)
  {
    std::cout <<"Reference " <<it.second;
    it.second = ros::names::resolve(reference_ns, it.second);
    std::cout <<" --> " <<it.second <<"\n";
  }
}

ds_param::ParamConnection* ControllerBase::parameterSubscription()
{
  DS_D(ControllerBase);
  return d->param_sub_.get();
}

void ControllerBase::parameterSubscriptionCallback(const ds_param::ParamConnection::ParamCollection& params)
{
  DS_D(ControllerBase);
  for (auto it = std::begin(params); it != std::end(params); ++it)
  {
    if (*it == d->active_controller_)
    {
      setEnabled(d->active_controller_->Get() == type());
      break;
    }
  }
}

void ControllerBase::publishBodyForces(geometry_msgs::WrenchStamped body_forces)
{
  DS_D(ControllerBase);
  if (enabled())
  {
    d->wrench_pub_.publish(body_forces);
  }

  d->wrench_priv_pub_.publish(body_forces);
}

void ControllerBase::setWrenchFrameId(const std::string& name)
{
  DS_D(ControllerBase);
  d->frame_id_ = name;
}

std::string ControllerBase::wrenchFrameId() const noexcept
{
  const DS_D(ControllerBase);
  return d->frame_id_;
}

void ControllerBase::executeController(ds_nav_msgs::AggregatedState reference, ds_nav_msgs::AggregatedState state,
                                       ros::Duration dt)
{
  const auto stamped_wrench = calculateForces(reference, state, std::move(dt));
  const auto error = errorState(reference, state);

  publishBodyForces(stamped_wrench);
  publishReference(reference);
  publishErrorState(error);
}

bool ControllerBase::switchAllocation(size_t id)
{
  DS_D(ControllerBase);
  d->active_allocation_->Set(static_cast<int>(id));
  return true;
}

size_t ControllerBase::activeAllocation() const noexcept
{
  const DS_D(ControllerBase);
  return static_cast<size_t>(d->active_allocation_->Get());
}

size_t ControllerBase::activeController() const noexcept
{
  const DS_D(ControllerBase);
  return static_cast<size_t>(d->active_controller_->Get());
}

const ControllerBase::ReferenceMap& ControllerBase::referenceMap() const noexcept
{
  const DS_D(ControllerBase);
  return d->reference_map_;
}

void ControllerBase::publishErrorState(const ds_nav_msgs::AggregatedState& msg)
{
  DS_D(ControllerBase);
  d->error_priv_pub_.publish(msg);
  if (enabled())
  {
    d->error_pub_.publish(msg);
  }
}

void ControllerBase::publishReference(const ds_nav_msgs::AggregatedState& msg)
{
  DS_D(ControllerBase);
  d->reference_priv_pub_.publish(msg);
  if (enabled())
  {
    d->reference_pub_.publish(msg);
  }
}

ds_nav_msgs::AggregatedState ControllerBase::errorState(const ds_nav_msgs::AggregatedState& ref,
                                                        const ds_nav_msgs::AggregatedState& state)
{
  auto error = ds_nav_msgs::AggregatedState{};
  error.header = state.header;
  error.header.frame_id = wrenchFrameId();
  error.ds_header.io_time = ros::Time::now();

  if (ref.northing.valid && state.northing.valid)
  {
    error.northing.valid = true;
    error.northing.value = ref.northing.value - state.northing.value;
  }
  else
  {
    error.northing.valid = false;
  }

  if (ref.easting.valid && state.easting.valid)
  {
    error.easting.valid = true;
    error.easting.value = ref.easting.value - state.easting.value;
  }
  else
  {
    error.easting.valid = false;
  }

  if (ref.down.valid && state.down.valid)
  {
    error.down.valid = true;
    error.down.value = ref.down.value - state.down.value;
  }
  else
  {
    error.down.valid = false;
  }

  if (ref.surge_u.valid && state.surge_u.valid)
  {
    error.surge_u.valid = true;
    error.surge_u.value = ref.surge_u.value - state.surge_u.value;
  }
  else
  {
    error.surge_u.valid = false;
  }

  if (ref.du_dt.valid && state.du_dt.valid)
  {
    error.du_dt.valid = true;
    error.du_dt.value = ref.du_dt.value - state.du_dt.value;
  }
  else
  {
    error.du_dt.valid = false;
  }

  if (ref.sway_v.valid && state.sway_v.valid)
  {
    error.sway_v.valid = true;
    error.sway_v.value = ref.sway_v.value - state.sway_v.value;
  }
  else
  {
    error.sway_v.valid = false;
  }

  if (ref.dv_dt.valid && state.dv_dt.valid)
  {
    error.dv_dt.valid = true;
    error.dv_dt.value = ref.dv_dt.value - state.dv_dt.value;
  }
  else
  {
    error.dv_dt.valid = false;
  }

  if (ref.heave_w.valid && state.heave_w.valid)
  {
    error.heave_w.valid = true;
    error.heave_w.value = ref.heave_w.value - state.heave_w.value;
  }
  else
  {
    error.heave_w.valid = false;
  }

  if (ref.dw_dt.valid && state.dw_dt.valid)
  {
    error.dw_dt.valid = true;
    error.dw_dt.value = ref.dw_dt.value - state.dw_dt.value;
  }
  else
  {
    error.dw_dt.valid = false;
  }

  if (ref.roll.valid && state.roll.valid)
  {
    error.roll.valid = true;
    error.roll.value = ds_util::angular_separation_radians(state.roll.value, ref.roll.value);
  }
  else
  {
    error.roll.valid = false;
  }

  if (ref.p.valid && state.p.valid)
  {
    error.p.valid = true;
    error.p.value = ref.p.value - state.p.value;
  }
  else
  {
    error.p.valid = false;
  }

  if (ref.dp_dt.valid && state.dp_dt.valid)
  {
    error.dp_dt.valid = true;
    error.dp_dt.value = ref.dp_dt.value - state.dp_dt.value;
  }
  else
  {
    error.dp_dt.valid = false;
  }

  if (ref.pitch.valid && state.pitch.valid)
  {
    error.pitch.valid = true;
    error.pitch.value = ds_util::angular_separation_radians(state.pitch.value, ref.pitch.value);
  }
  else
  {
    error.pitch.valid = false;
  }

  if (ref.q.valid && state.q.valid)
  {
    error.q.valid = true;
    error.q.value = ref.q.value - state.q.value;
  }
  else
  {
    error.q.valid = false;
  }

  if (ref.dq_dt.valid && state.dq_dt.valid)
  {
    error.dq_dt.valid = true;
    error.dq_dt.value = ref.dq_dt.value - state.dq_dt.value;
  }
  else
  {
    error.dq_dt.valid = false;
  }

  if (ref.heading.valid && state.heading.valid)
  {
    error.heading.valid = true;
    error.heading.value = ds_util::angular_separation_radians(state.heading.value, ref.heading.value);
  }
  else
  {
    error.heading.valid = false;
  }

  if (ref.r.valid && state.r.valid)
  {
    error.r.valid = true;
    error.r.value = ref.r.value - state.r.value;
  }
  else
  {
    error.r.valid = false;
  }

  if (ref.dr_dt.valid && state.dr_dt.valid)
  {
    error.dr_dt.valid = true;
    error.dr_dt.value = ref.dr_dt.value - state.dr_dt.value;
  }
  else
  {
    error.dr_dt.valid = false;
  }

  return error;
}

void ControllerBase::reset()
{
  DS_D(ControllerBase);
  d->last_reference_ = ds_nav_msgs::AggregatedState{};
  d->last_state_ = ds_nav_msgs::AggregatedState{};
}
}
