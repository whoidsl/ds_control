//
// Created by ivaughn on 3/12/19.
//

#ifndef DS_CONTROL_BODY_LIMITS_PRIVATE_H
#define DS_CONTROL_BODY_LIMITS_PRIVATE_H

#include "ds_control/body_limits.h"
#include <vector>

namespace ds_control {

struct BodyLimitsPrivate {

  BodyLimitsPrivate() {
    max_force_limits_.resize(BodyLimits::Axis::N_AXIS, 0);
    min_force_limits_.resize(BodyLimits::Axis::N_AXIS, 0);

    max_velocity_limits_.resize(BodyLimits::Axis::N_AXIS, 0);
    min_velocity_limits_.resize(BodyLimits::Axis::N_AXIS, 0);
  }

  std::vector<double> max_force_limits_;
  std::vector<double> min_force_limits_;

  std::vector<double> max_velocity_limits_;
  std::vector<double> min_velocity_limits_;

};

}

#endif //DS_CONTROL_BODY_LIMITS_PRIVATE_H
