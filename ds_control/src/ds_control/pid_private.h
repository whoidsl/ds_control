/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 1/30/18.
//

#ifndef PROJECT_PID_PRIVATE_H
#define PROJECT_PID_PRIVATE_H

#include "control_msgs/PidState.h"
#include <ros/ros.h>
#include <boost/optional.hpp>

namespace ds_control
{
struct PidPrivate
{
  PidPrivate()
    : integral_(0)
    , prev_error_(0)
    , min_(-std::numeric_limits<double>::infinity())
    , max_(std::numeric_limits<double>::infinity())
    , integral_falloff_(1)
    , pid_pub_enabled_(false)
    , output_deadband_(0)
  {
    pid_state_msg_.i_min = -std::numeric_limits<double>::infinity();
    pid_state_msg_.i_max = std::numeric_limits<double>::infinity();
  }

  ~PidPrivate() = default;

  double calculate_impl(double x, double x_ref, double dt);
  double calculate_with_measured_rate_impl(double x, double x_ref, double x_dot, double x_ref_dot, double dt);

  double integral_;
  double prev_error_;

  ros::Time last_timestamp_;

  std::function<double(double actual, double reference)> error_wrap_func_;

  double min_;
  double max_;
  double output_deadband_;

  bool pid_pub_enabled_;
  ros::Publisher pid_pub_;
  control_msgs::PidState pid_state_msg_;

  double integral_falloff_;
};
}
#endif  // PROJECT_PID_PRIVATE_H
