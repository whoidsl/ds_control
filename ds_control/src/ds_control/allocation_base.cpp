/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_control/allocation_base.h"
#include "allocation_base_private.h"

#include <ros/ros.h>

namespace ds_control
{
AllocationBase::AllocationBase()
  : ds_base::DsProcess(), d_ptr_(std::unique_ptr<AllocationBasePrivate>(new AllocationBasePrivate))
{
}

AllocationBase::AllocationBase(int argc, char** argv, const std::string& name)
  : DsProcess(argc, argv, name), d_ptr_(std::unique_ptr<AllocationBasePrivate>(new AllocationBasePrivate))
{
}

AllocationBase::~AllocationBase() = default;

const AllocationBase::ForceInputLimits& AllocationBase::forceInputLimits() const
{
  const DS_D(AllocationBase);
  return d->force_limits_;
}

void AllocationBase::setForceInputLimits(AllocationBase::ForceInputLimits limits)
{
  DS_D(AllocationBase);
  d->force_limits_ = std::move(limits);
}

void AllocationBase::setEnabled(bool enabled)
{
  DS_D(AllocationBase);
  if (enabled)
  {
    const auto type_ = static_cast<int>(type());
    if (type_ != d->active_allocation_->Get())
    {
      d->active_allocation_->Set(type_);
    }
  }
  d->is_enabled_ = enabled;
}

bool AllocationBase::enabled() const noexcept
{
  const DS_D(AllocationBase);
  return d->active_allocation_->Get() == type();
}

geometry_msgs::Wrench AllocationBase::applyForceInputLimits(geometry_msgs::Wrench wrench)
{
  const auto& limits = forceInputLimits();
  wrench.force.x = std::min(wrench.force.x, limits.max.force.x);
  wrench.force.x = std::max(wrench.force.x, limits.min.force.x);

  wrench.force.y = std::min(wrench.force.y, limits.max.force.y);
  wrench.force.y = std::max(wrench.force.y, limits.min.force.y);

  wrench.force.z = std::min(wrench.force.z, limits.max.force.z);
  wrench.force.z = std::max(wrench.force.z, limits.min.force.z);

  wrench.torque.x = std::min(wrench.torque.x, limits.max.torque.x);
  wrench.torque.x = std::max(wrench.torque.x, limits.min.torque.x);

  wrench.torque.y = std::min(wrench.torque.y, limits.max.torque.y);
  wrench.torque.y = std::max(wrench.torque.y, limits.min.torque.y);

  wrench.torque.z = std::min(wrench.torque.z, limits.max.torque.z);
  wrench.torque.z = std::max(wrench.torque.z, limits.min.torque.z);

  return wrench;
}

ds_param::ParamConnection* AllocationBase::parameterSubscription()
{
  DS_D(AllocationBase);
  return d->param_sub_.get();
}

void AllocationBase::parameterSubscriptionCallback(const ds_param::ParamConnection::ParamCollection& params)
{
  DS_D(AllocationBase);
  for (auto it = std::begin(params); it != std::end(params); ++it)
  {
    if (*it == d->active_allocation_)
    {
      setEnabled(d->active_allocation_->Get() == type());
      break;
    }
  }
}

void AllocationBase::setupSubscriptions()
{
  DsProcess::setupSubscriptions();
  DS_D(AllocationBase);

  const auto controllers_ns = ros::param::param<std::string>("controller_ns", "");
  const auto wrench_topic = ros::names::resolve(controllers_ns, std::string{ "wrench" });
  auto nh = nodeHandle();

  d->body_wrench_sub_ = nh.subscribe(wrench_topic, 1, &AllocationBase::publishActuatorCommands, this);

  d->param_sub_ = ds_param::ParamConnection::create(nh);
  d->active_controller_ = d->param_sub_->connect<ds_param::IntParam>("active_controller");
  d->active_allocation_ = d->param_sub_->connect<ds_param::IntParam>("active_allocation");
  d->param_sub_->setCallback(boost::bind(&AllocationBase::parameterSubscriptionCallback, this, _1));
}

void AllocationBase::setup()
{
  DsProcess::setup();
  DS_D(AllocationBase);
  if (!d->active_allocation_)
  {
    ROS_FATAL("Parameter subscription for active allocation is null!");
    ROS_BREAK();
  }
  setEnabled(d->active_controller_->Get() == type());
}
}
