/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_control/joystick_base.h"
#include "joystick_base_private.h"

namespace ds_control
{
JoystickBase::JoystickBase()
  : ds_control::GoalBase(), d_ptr_(std::unique_ptr<JoystickBasePrivate>(new JoystickBasePrivate))
{
}
JoystickBase::JoystickBase(int argc, char** argv, const std::string& name)
  : ds_control::GoalBase(argc, argv, name), d_ptr_(std::unique_ptr<JoystickBasePrivate>(new JoystickBasePrivate))
{
}

JoystickBase::~JoystickBase() = default;

void JoystickBase::setupSubscriptions()
{
  GoalBase::setupSubscriptions();
  DS_D(JoystickBase);

  auto nh = nodeHandle();
  d->param_sub_ = ds_param::ParamConnection::create(nh);
  d->active_joystick_ = d->param_sub_->connect<ds_param::IntParam>(d->active_joystick_topic_);
  d->active_controller_ = d->param_sub_->connect<ds_param::IntParam>(d->active_controller_topic_);
  d->active_allocation_ = d->param_sub_->connect<ds_param::IntParam>(d->active_allocation_topic_);
  d->param_sub_->setCallback(boost::bind(&JoystickBase::parameterSubscriptionCallback, this, _1));
}

void JoystickBase::setupParameters()
{
  auto gains = GoalSettings{};
  gains[GoalDOF::POSITION_U].gain = ros::param::param<double>("~gain/position_u/scale", 1.0);
  gains[GoalDOF::POSITION_V].gain = ros::param::param<double>("~gain/position_v/scale", 1.0);
  gains[GoalDOF::POSITION_W].gain = ros::param::param<double>("~gain/position_w/scale", 1.0);

  gains[GoalDOF::VELOCITY_U].gain = ros::param::param<double>("~gain/velocity_u/scale", 1.0);
  gains[GoalDOF::VELOCITY_V].gain = ros::param::param<double>("~gain/velocity_v/scale", 1.0);
  gains[GoalDOF::VELOCITY_W].gain = ros::param::param<double>("~gain/velocity_w/scale", 1.0);

  gains[GoalDOF::ROTATION_U].gain = ros::param::param<double>("~gain/rotation_u/scale", 1.0);
  gains[GoalDOF::ROTATION_V].gain = ros::param::param<double>("~gain/rotation_v/scale", 1.0);
  gains[GoalDOF::ROTATION_W].gain = ros::param::param<double>("~gain/rotation_w/scale", 1.0);

  gains[GoalDOF::ROTATION_RATE_U].gain = ros::param::param<double>("~gain/rotation_rate_u/scale", 1.0);
  gains[GoalDOF::ROTATION_RATE_V].gain = ros::param::param<double>("~gain/rotation_rate_v/scale", 1.0);
  gains[GoalDOF::ROTATION_RATE_W].gain = ros::param::param<double>("~gain/rotation_rate_w/scale", 1.0);

  gains[GoalDOF::POSITION_U].bias = ros::param::param<double>("~gain/position_u/bias", 0.0);
  gains[GoalDOF::POSITION_V].bias = ros::param::param<double>("~gain/position_v/bias", 0.0);
  gains[GoalDOF::POSITION_W].bias = ros::param::param<double>("~gain/position_w/bias", 0.0);

  gains[GoalDOF::VELOCITY_U].bias = ros::param::param<double>("~gain/velocity_u/bias", 0.0);
  gains[GoalDOF::VELOCITY_V].bias = ros::param::param<double>("~gain/velocity_v/bias", 0.0);
  gains[GoalDOF::VELOCITY_W].bias = ros::param::param<double>("~gain/velocity_w/bias", 0.0);

  gains[GoalDOF::ROTATION_U].bias = ros::param::param<double>("~gain/rotation_u/bias", 0.0);
  gains[GoalDOF::ROTATION_V].bias = ros::param::param<double>("~gain/rotation_v/bias", 0.0);
  gains[GoalDOF::ROTATION_W].bias = ros::param::param<double>("~gain/rotation_w/bias", 0.0);

  gains[GoalDOF::ROTATION_RATE_U].bias = ros::param::param<double>("~gain/rotation_rate_u/bias", 0.0);
  gains[GoalDOF::ROTATION_RATE_V].bias = ros::param::param<double>("~gain/rotation_rate_v/bias", 0.0);
  gains[GoalDOF::ROTATION_RATE_W].bias = ros::param::param<double>("~gain/rotation_rate_w/bias", 0.0);

  DS_D(JoystickBase);
  d->active_joystick_topic_ = ros::param::param<std::string>("~active_joystick_dsparam", "active_joystick");
  d->active_controller_topic_ = ros::param::param<std::string>("~active_controller_dsparam", "active_controller");
  d->active_allocation_topic_ = ros::param::param<std::string>("~active_allocation_dsparam", "active_allocation");

  setBiasAndGains(gains);
}
void JoystickBase::setEnabled(bool enabled)
{
  DS_D(JoystickBase);

  // Call the pre-disable hook if we're turning off
  if (!enabled && d->is_enabled_)
  {
    preDisableHook();
  }

  if (enabled)
  {
    const auto type_ = static_cast<int>(type());
    if (type_ != d->active_joystick_->Get())
    {
      d->active_joystick_->Set(type_);
    }
  }

  d->is_enabled_ = enabled;

  // Call the post-enable hook if we're turning on.
  if (enabled && !d->is_enabled_)
  {
    d->is_enabled_ = enabled;
    postEnableHook();
    return;
  }
}

bool JoystickBase::enabled() const noexcept
{
  const DS_D(JoystickBase);
  return d->active_joystick_->Get() == type();
}

void JoystickBase::setAllocation(int value)
{
  DS_D(JoystickBase);

  if (d->is_enabled_)
  {
    if (value != d->active_allocation_->Get())
    {
      d->active_allocation_->Set(value);
    }
  }
  else
  {
    ROS_ERROR_STREAM("Joystick disabled!");
  }
}

int JoystickBase::getAllocation() const
{
  const DS_D(JoystickBase);
  return d->active_allocation_->Get();
}

void JoystickBase::setController(int value)
{
  DS_D(JoystickBase);

  if (d->is_enabled_)
  {
    if (value != d->active_controller_->Get())
    {
      d->active_controller_->Set(value);
    }
  }
}

int JoystickBase::getController() const
{
  const DS_D(JoystickBase);
  return d->active_controller_->Get();
}

void JoystickBase::parameterSubscriptionCallback(const ds_param::ParamConnection::ParamCollection& params)
{
  DS_D(JoystickBase);
  for (auto it = std::begin(params); it != std::end(params); ++it)
  {
    if (*it == d->active_joystick_)
    {
      setEnabled(d->active_joystick_->Get() == type());
      break;
    }
  }
}

void JoystickBase::setBiasAndGains(const JoystickBase::GoalSettings& settings)
{
  DS_D(JoystickBase);
  d->dof_settings_ = settings;
}

const JoystickBase::GoalSettings& JoystickBase::biasAndGains() const noexcept
{
  const DS_D(JoystickBase);
  return d->dof_settings_;
}
void JoystickBase::setBiasAndGain(JoystickBase::GoalDOF index, double bias, double gain)
{
  DS_D(JoystickBase);
  auto& val = d->dof_settings_.at(index);
  val.gain = gain;
  val.bias = bias;
}

JoystickBase::GoalDOFSettings JoystickBase::biasAndGain(JoystickBase::GoalDOF index)
{
  DS_D(JoystickBase);
  return d->dof_settings_.at(index);
}

ds_nav_msgs::FlaggedDouble JoystickBase::applyGain(JoystickBase::GoalDOF index, ds_nav_msgs::FlaggedDouble in) const
{
  const DS_D(JoystickBase);

  auto result = in;

  const auto s = d->dof_settings_.at(index);
  result.value = (result.value + s.bias) * s.gain;
  return result;
}

ds_nav_msgs::AggregatedState JoystickBase::applyGains(const ds_nav_msgs::AggregatedState& in)
{
  auto ref = ds_nav_msgs::AggregatedState{};

  ref.northing = applyGain(GoalDOF::POSITION_U, in.northing);
  ref.easting = applyGain(GoalDOF::POSITION_V, in.easting);
  ref.down = applyGain(GoalDOF::POSITION_W, in.down);

  ref.roll = applyGain(GoalDOF::ROTATION_U, in.roll);
  ref.pitch = applyGain(GoalDOF::ROTATION_V, in.pitch);
  ref.heading = applyGain(GoalDOF::ROTATION_W, in.heading);

  ref.surge_u = applyGain(GoalDOF::VELOCITY_U, in.surge_u);
  ref.sway_v = applyGain(GoalDOF::VELOCITY_V, in.sway_v);
  ref.heave_w = applyGain(GoalDOF::VELOCITY_W, in.heave_w);

  ref.p = applyGain(GoalDOF::ROTATION_RATE_U, in.p);
  ref.q = applyGain(GoalDOF::ROTATION_RATE_V, in.q);
  ref.r = applyGain(GoalDOF::ROTATION_RATE_W, in.r);

  return ref;
}

void JoystickBase::applyGainsAndPublish(const ds_nav_msgs::AggregatedState& in)
{
  if (enabled())
  {
    auto ref = applyGains(in);
    ref.header.stamp = ros::Time::now();
    publishGoal(ref);
  }
  else
  {
    return;
  }
}

void JoystickBase::switchJoystick(uint64_t joystick)
{
  setEnabled(joystick == type());
  DS_D(JoystickBase);
  d->active_joystick_->Set(static_cast<int>(joystick));
}
}
