/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_control/pid.h"
#include "pid_private.h"

#include <ros/param.h>

namespace ds_control
{
Pid::Pid() : Pid(0, 0, 0)
{
}

Pid::Pid(double kp, double ki, double kd) : d_ptr_(std::unique_ptr<PidPrivate>(new PidPrivate))
{
  DS_D(Pid);
  d->pid_state_msg_.p_term = kp;
  d->pid_state_msg_.i_term = ki;
  d->pid_state_msg_.d_term = kd;
}

Pid::Pid(double kp, double ki, double kd, double max, double min) : Pid(kp, ki, kd)
{
  DS_D(Pid);
  d->max_ = max;
  d->min_ = min;
}
Pid::Pid(Pid::PidGains gains) : Pid(gains.kp, gains.ki, gains.kd)
{
}

Pid::Pid(Pid::PidGains gains, double max, double min) : Pid(gains.kp, gains.ki, gains.kd, max, min)
{
}

Pid::~Pid() = default;

Pid::Pid(const Pid& other) : Pid(other.kp(), other.ki(), other.kd())
{
  DS_D(Pid);
  d->min_ = other.minSaturation();
  d->max_ = other.maxSaturation();
  d->pid_state_msg_.i_min = other.minIntegratedError();
  d->pid_state_msg_.i_max = other.maxIntegratedError();
  d->integral_falloff_ = other.integratedErrorFalloff();
}

Pid::Pid(Pid&& other) noexcept : d_ptr_(std::move(other.d_ptr_))
{
}

void Pid::setKp(double kp)
{
  DS_D(Pid);
  d->pid_state_msg_.p_term = kp;
}
double Pid::kp() const noexcept
{
  const DS_D(Pid);
  return d->pid_state_msg_.p_term;
}
void Pid::setKi(double ki)
{
  DS_D(Pid);
  d->pid_state_msg_.i_term = ki;
}

double Pid::ki() const noexcept
{
  const DS_D(Pid);
  return d->pid_state_msg_.i_term;
}

void Pid::setKd(double kd)
{
  DS_D(Pid);
  d->pid_state_msg_.d_term = kd;
}

void Pid::setPidGains(Pid::PidGains gains)
{
  DS_D(Pid);
  d->pid_state_msg_.p_term = gains.kp;
  d->pid_state_msg_.i_term = gains.ki;
  d->pid_state_msg_.d_term = gains.kd;
}

Pid::PidGains Pid::pidGains() const noexcept
{
  const DS_D(Pid);
  return {.kp = d->pid_state_msg_.p_term, .ki = d->pid_state_msg_.i_term, .kd = d->pid_state_msg_.d_term };
}

control_msgs::PidState Pid::pidState() const noexcept
{
  const DS_D(Pid);

  return d->pid_state_msg_;
}

double Pid::kd() const noexcept
{
  const DS_D(Pid);
  return d->pid_state_msg_.d_term;
}
void Pid::setMaxSaturation(double max)
{
  DS_D(Pid);
  d->max_ = max;
}

double Pid::maxSaturation() const noexcept
{
  const DS_D(Pid);
  return d->max_;
}

void Pid::setMinSaturation(double min)
{
  DS_D(Pid);
  d->min_ = min;
}

double Pid::minSaturation() const noexcept
{
  const DS_D(Pid);
  return d->min_;
}

void Pid::clearMinSaturation()
{
  DS_D(Pid);
  d->min_ = -std::numeric_limits<double>::infinity();
}

void Pid::clearMaxSaturation()
{
  DS_D(Pid);
  d->max_ = std::numeric_limits<double>::infinity();
}
void Pid::setErrorWrapper(std::function<double(double, double)> wrap_func)
{
  DS_D(Pid);
  d->error_wrap_func_ = std::move(wrap_func);
}
void Pid::setMaxIntegratedError(double value)
{
  DS_D(Pid);
  d->pid_state_msg_.i_max = value;
}
double Pid::maxIntegratedError() const noexcept
{
  const DS_D(Pid);
  return d->pid_state_msg_.i_max;
}
void Pid::setMinIntegratedError(double value)
{
  DS_D(Pid);
  d->pid_state_msg_.i_min = value;
}

double Pid::minIntegratedError() const noexcept
{
  const DS_D(Pid);
  return d->pid_state_msg_.i_min;
}
void Pid::clearMaxIntegratedError()
{
  DS_D(Pid);
  d->pid_state_msg_.i_max = std::numeric_limits<double>::infinity();
}
void Pid::clearMinIntegratedError()
{
  DS_D(Pid);
  d->pid_state_msg_.i_min = -std::numeric_limits<double>::infinity();
}

void Pid::setOutputDeadband(double deadband)
{
  DS_D(Pid);
  d->output_deadband_ = std::abs(deadband);
}

double Pid::outputDeadband() const noexcept
{
  const DS_D(Pid);
  return d->output_deadband_;
}

void Pid::setIntegratedErrorFalloff(double value)
{
  DS_D(Pid);
  d->integral_falloff_ = std::min(1.0, std::abs(value));
}

double Pid::integratedErrorFalloff() const noexcept
{
  const DS_D(Pid);
  return d->integral_falloff_;
}

double Pid::calculate(double x, double x_ref, ros::Time timestamp)
{
  DS_D(Pid);
  if (!d->last_timestamp_.isValid())
  {
    d->last_timestamp_ = std::move(timestamp);
    return 0;
  }

  const auto dt = (timestamp - d->last_timestamp_).toSec();
  d->last_timestamp_ = std::move(timestamp);

  return d->calculate_impl(x, x_ref, dt);
}

double Pid::calculate(double x, double x_ref)
{
  return calculate(x, x_ref, ros::Time::now());
}

double Pid::calculate_with_measured_rate(double x, double x_ref, double x_dot, double x_ref_dot, double dt)
{
  DS_D(Pid);
  return d->calculate_with_measured_rate_impl(x, x_ref, x_dot, x_ref_dot, dt);
}

double Pid::calculate(double x, double x_ref, double dt)
{
  if (dt == 0)
  {
    return 0;
  }

  DS_D(Pid);
  return d->calculate_impl(x, x_ref, dt);
}

void Pid::reset()
{
  DS_D(Pid);
  d->integral_ = 0;
  d->prev_error_ = 0;
  d->last_timestamp_ = ros::Time{};
}

void Pid::setupFromParameterServer(const std::string& ns)
{
  auto param_setter = [](const std::string& param_name, double value, std::function<void(double)> setter) {
    if (!ros::param::get(param_name, value))
    {
      ROS_INFO_STREAM("No parameter named '" << param_name << "', using value: " << value);
    }
    else
    {
      ROS_INFO_STREAM("Read value: " << value << " for parameter named '" << param_name << "'");
    }

    setter(value);
  };

  auto double_param = kp();
  auto param_name = ns + "/gains/kp";

  param_setter(param_name, double_param, std::bind(&Pid::setKp, this, std::placeholders::_1));

  double_param = kd();
  param_name = ns + "/gains/kd";
  param_setter(param_name, double_param, std::bind(&Pid::setKd, this, std::placeholders::_1));

  double_param = ki();
  param_name = ns + "/gains/ki";
  param_setter(param_name, double_param, std::bind(&Pid::setKi, this, std::placeholders::_1));

  double_param = maxIntegratedError();
  param_name = ns + "/integrated_error_limits/max";
  param_setter(param_name, double_param, std::bind(&Pid::setMaxIntegratedError, this, std::placeholders::_1));

  double_param = minIntegratedError();
  param_name = ns + "/integrated_error_limits/min";
  param_setter(param_name, double_param, std::bind(&Pid::setMinIntegratedError, this, std::placeholders::_1));

  double_param = integratedErrorFalloff();
  param_name = ns + "/integrated_error_falloff";
  param_setter(param_name, double_param, std::bind(&Pid::setIntegratedErrorFalloff, this, std::placeholders::_1));

  double_param = maxSaturation();
  param_name = ns + "/output_saturation_limits/max";
  param_setter(param_name, double_param, std::bind(&Pid::setMaxSaturation, this, std::placeholders::_1));

  double_param = minSaturation();
  param_name = ns + "/output_saturation_limits/min";
  param_setter(param_name, double_param, std::bind(&Pid::setMinSaturation, this, std::placeholders::_1));

  double_param = outputDeadband();
  param_name = ns + "/output_deadband";
  param_setter(param_name, double_param, std::bind(&Pid::setOutputDeadband, this, std::placeholders::_1));
}

void Pid::enableStatePublishing(ros::NodeHandle& nh, const std::string& topic)
{
  const auto topic_str = ros::names::resolve(ros::this_node::getName(), topic);

  DS_D(Pid);
  d->pid_pub_ = nh.advertise<control_msgs::PidState>(topic_str, 1, false);
  d->pid_pub_enabled_ = true;
}
}
