/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 3/12/19.
//

#include <cmath>
#include "body_limits_private.h"

namespace ds_control {

BodyLimits::BodyLimits() : d_ptr_(new BodyLimitsPrivate)
{
}

BodyLimits::~BodyLimits() = default;

/// The maximum force this vessel allows in the given axis
/// newtons for FWD/STBD/DOWN, newton*meters for HEADING
/// \param axis The axis to check limits for
/// \return The minimum velocity value
double BodyLimits::maxForce(BodyLimits::Axis axis) const {
  DS_D(const BodyLimits);
  return d->max_force_limits_[axis];
}

/// The minimum force this vessel allows in the given axis.  Usually quite negative.
/// newtons for FWD/STBD/DOWN, newton*meters for HEADING
/// \param axis The axis to check limits for
/// \return The minimum velocity value
double BodyLimits::minForce(BodyLimits::Axis axis) const {
  DS_D(const BodyLimits);
  return d->min_force_limits_[axis];
}

/// The smaller-magnitude of the minimum and maximum thrust limits
/// Effectively the maximum-available symmetric limit.
/// newtons for FWD/STBD/DOWN, newton*meters for HEADING
/// \param axis The axis to examine
/// \return The smaller-magnitude of the two force limits
double BodyLimits::minAbsForce(BodyLimits::Axis axis) const {
  DS_D(const BodyLimits);
  return std::min(std::fabs(d->max_force_limits_[axis]),
      std::fabs(d->min_force_limits_[axis]));
}

/// The larger-magnitude of the minimum and maximum thrust limits
/// newtons for FWD/STBD/DOWN, newton*meters for HEADING
/// \param axis The axis to examine
/// \return The larger-magnitude of the two force limits
double BodyLimits::maxAbsForce(BodyLimits::Axis axis) const {
  DS_D(const BodyLimits);
  return std::max(std::fabs(d->max_force_limits_[axis]),
      std::fabs(d->min_force_limits_[axis]));
}

/// Clamp a force to the limits specified for a given axis
/// newtons for FWD/STBD/DOWN, newton*meters for HEADING
///
/// \param axis The axis to clamp to
/// \param force The force to clamp
/// \return  The clamped force
double BodyLimits::clampForce(BodyLimits::Axis axis, double force) const  {
  DS_D(const BodyLimits);

  if (force > d->max_force_limits_[axis]) {
    return d->max_force_limits_[axis];
  }
  if (force < d->min_force_limits_[axis]) {
    return d->min_force_limits_[axis];
  }
  return force;
}

/// The maximum velocity this vessel allows in the given axis
/// m/s for FWD/STBD/DOWN, rad/s for HEADING
/// \param axis The axis to check limits for
/// \return The minimum velocity value
double BodyLimits::maxVelocity(BodyLimits::Axis axis) const {
  DS_D(const BodyLimits);
  return d->max_velocity_limits_[axis];
}

/// The minimum velocity this vessel allows in the given axis.
/// Usually, this is quite negative.
/// m/s for FWD/STBD/DOWN, rad/s for HEADING
/// \param axis The axis to check limits for
/// \return The minimum velocity value
double BodyLimits::minVelocity(BodyLimits::Axis axis) const {
  DS_D(const BodyLimits);
  return d->min_velocity_limits_[axis];
}

/// The minimum absolute velocity for a given axis
/// m/s for FWD/STBD/DOWN, rad/s for HEADING
/// \param axis The axis to check limits for
/// \return The smaller of the two velocity limits
double BodyLimits::minAbsVelocity(BodyLimits::Axis axis) const {
  DS_D(const BodyLimits);
  return std::min(std::fabs(d->max_velocity_limits_[axis]),
      std::fabs(d->min_velocity_limits_[axis]));
}

/// The maximum absolute velocity for a given axis
/// m/s for FWD/STBD/DOWN, rad/s for HEADING
/// \param axis The axis to check limits for
/// \return The larger of the two velocity limits
double BodyLimits::maxAbsVelocity(BodyLimits::Axis axis) const {
  DS_D(const BodyLimits);
  return std::max(std::fabs(d->max_velocity_limits_[axis]),
      std::fabs(d->min_velocity_limits_[axis]));
}

/// Clamp a value to the velocity bounds
/// m/s for FWD/STBD/DOWN, rad/s for HEADING
///
/// \param axis The axis to clamp to
/// \param velocity The velocity to clamp.
/// \return The clamped velocity
double BodyLimits::clampVelocity(BodyLimits::Axis axis, double velocity) const {
  DS_D(const BodyLimits);

  if (velocity > d->max_velocity_limits_[axis]) {
    return d->max_velocity_limits_[axis];
  }
  if (velocity < d->min_velocity_limits_[axis]) {
    return d->min_velocity_limits_[axis];
  }
  return velocity;
}

/// Load the body-frame limits from the given namespace.  The following parameters are loaded
///
/// All units in Netwons:
/// * <dynamics_ns>/total_limits/force_fwd
///   <dynamics_ns>/total_limits/force_aft
/// * <dynamics_ns>/total_limits/force_stbd
///   <dynamics_ns>/total_limits/force_port
/// * <dynamics_ns>/total_limits/force_down
///   <dynamics_ns>/total_limits/force_up
/// Torque limits in Newton*meters:
/// * <dynamics_ns>/total_limits/torque_right
///   <dynamics_ns>/total_limits/torque_left
///
/// All velocities in m/s
/// * <dynamics_ns>/total_limits/velocity_fwd
///   <dynamics_ns>/total_limits/velocity_aft
/// * <dynamics_ns>/total_limits/velocity_stbd
///   <dynamics_ns>/total_limits/velocity_port
/// * <dynamics_ns>/total_limits/velocity_down
///   <dynamics_ns>/total_limits/velocity_up
/// Turn rates are in DEGREES per second
///   <dynamics_ns>/total_limits/turn_rate_right
/// * <dynamics_ns>/total_limits/turn_rate_left
///
/// Note that all of these are the positive and negative values along a forward, port, down
/// reference frame.  In the above list, the starred values are postiive and the
/// unstarred ones are negative.  Negative values can be ommitted, in which case the
/// force limits are assumed to be symmetric

/// \param handle The ROS node handle to use
/// \param dynamics_ns The namespace to load parameters from
void BodyLimits::loadParameters(ros::NodeHandle &handle, const std::string& dynamics_ns) {
  DS_D(BodyLimits);

  std::string limits_ns = dynamics_ns + "/total_limits";

  // body-frame forces
  d->max_force_limits_[Axis::FWD] = handle.param<double>(limits_ns + "/force_fwd", 0);
  d->min_force_limits_[Axis::FWD] = handle.param<double>(limits_ns + "/force_aft",
                                                            -d->max_force_limits_[Axis::FWD]);

  d->max_force_limits_[Axis::STBD] = handle.param<double>(limits_ns + "/force_stbd", 0);
  d->min_force_limits_[Axis::STBD] = handle.param<double>(limits_ns + "/force_port",
                                                            -d->max_force_limits_[Axis::STBD]);

  d->max_force_limits_[Axis::DOWN] = handle.param<double>(limits_ns + "/force_down", 0);
  d->min_force_limits_[Axis::DOWN] = handle.param<double>(limits_ns + "/force_up",
                                                            -d->max_force_limits_[Axis::DOWN]);

  // moments
  d->max_force_limits_[Axis::HEADING] = handle.param<double>(limits_ns + "/torque_right", 0);
  d->min_force_limits_[Axis::HEADING] = handle.param<double>(limits_ns + "/torque_left",
                                                          -d->max_force_limits_[Axis::HEADING]);

  // now velocity
  d->max_velocity_limits_[Axis::FWD] = handle.param<double>(limits_ns + "/velocity_fwd", 0);
  d->min_velocity_limits_[Axis::FWD] = handle.param<double>(limits_ns + "/velocity_aft",
                                                            -d->max_velocity_limits_[Axis::FWD]);

  d->max_velocity_limits_[Axis::STBD] = handle.param<double>(limits_ns + "/velocity_stbd", 0);
  d->min_velocity_limits_[Axis::STBD] = handle.param<double>(limits_ns + "/velocity_port",
                                                             -d->max_velocity_limits_[Axis::STBD]);

  d->max_velocity_limits_[Axis::DOWN] = handle.param<double>(limits_ns + "/velocity_down", 0);
  d->min_velocity_limits_[Axis::DOWN] = handle.param<double>(limits_ns + "/velocity_up",
                                                             -d->max_velocity_limits_[Axis::DOWN]);
  // turn rates
  d->max_velocity_limits_[Axis::HEADING] = handle.param<double>(limits_ns + "/turn_rate_right", 0);
  d->min_velocity_limits_[Axis::HEADING] = handle.param<double>(limits_ns + "/turn_rate_left",
                                                             -d->max_velocity_limits_[Axis::HEADING]);

  // finally, convert velocity to radians
  d->max_velocity_limits_[Axis::HEADING] *= (M_PI/180.0);
  d->min_velocity_limits_[Axis::HEADING] *= (M_PI/180.0);
}

}