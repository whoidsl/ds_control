/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_util/angle.h"
#include "ds_control/joystick_controller.h"
#include "joystick_controller_private.h"

namespace ds_control
{
JoystickController::JoystickController()
  : ControllerBase(), d_ptr_(std::unique_ptr<JoystickControllerPrivate>(new JoystickControllerPrivate))
{
}

JoystickController::JoystickController(int argc, char** argv, const std::string& name)
  : ControllerBase(argc, argv, name), d_ptr_(std::unique_ptr<JoystickControllerPrivate>(new JoystickControllerPrivate))
{
}

JoystickController::~JoystickController() = default;

void JoystickController::setHeadingRateDeadband(double deadband)
{
  DS_D(JoystickController);
  d->heading_rate_deadband_ = std::abs(deadband);
}

double JoystickController::headingRateDeadband() const noexcept
{
  const DS_D(JoystickController);
  return d->heading_rate_deadband_;
}

void JoystickController::joystickReferenceCallback(const ds_nav_msgs::AggregatedState& msg)
{
  auto ref = msg;

  // If auto-heading is enabled, but we're actively turning vehicle, replace the reference
  // heading with the last nav heading so that when we stop turning the vehicle sticks to
  // current heading value.
  if (!autoHeadingEnabled() || (std::abs(msg.r.value) > headingRateDeadband()))
  {
    const auto last_state = state();
    ref.heading = last_state.heading;
    // ROS_DEBUG_STREAM("Updating reference heading to: " << ref.heading.value * 180 / M_PI);
  }
  else
  {
    auto last_ref = reference();
    ref.heading = last_ref.heading;
  }

  updateReference(ref);
}

void JoystickController::setAutoHeadingGains(double kp, double ki, double kd)
{
  DS_D(JoystickController);
  d->pid_heading.setKp(kp);
  d->pid_heading.setKi(ki);
  d->pid_heading.setKd(kd);
}

std::tuple<double, double, double> JoystickController::autoHeadingGains() const noexcept
{
  const DS_D(JoystickController);
  return std::make_tuple(d->pid_heading.kp(), d->pid_heading.ki(), d->pid_heading.kd());
}

void JoystickController::setAutoHeadingValueRadians(double radians)
{
  auto ref = referenceRef();
  ref.heading.value = std::fmod(radians, 2 * M_PI);
  ref.heading.valid = true;
}

double JoystickController::autoHeadingValueRadians() const noexcept
{
  return reference().heading.value;
}

void JoystickController::setAutoHeadingEnabled(bool enable)
{
  DS_D(JoystickController);

  if (!enable)
  {
    d->pid_heading.reset();
  }

  d->use_close_loop_heading_ = enable;
}

bool JoystickController::setAutoHeadingEnabled(ds_base::BoolCommand::Request& request,
                                               ds_base::BoolCommand::Response& res)
{
  setAutoHeadingEnabled(request.command);
  return true;
}

bool JoystickController::autoHeadingEnabled() const noexcept
{
  const DS_D(JoystickController);
  return d->use_close_loop_heading_;
}

ds_nav_msgs::AggregatedState JoystickController::errorState(const ds_nav_msgs::AggregatedState& ref,
                                                            const ds_nav_msgs::AggregatedState& state)
{
  auto error = ds_nav_msgs::AggregatedState{};
  error.header = state.header;
  error.header.frame_id = wrenchFrameId();
  error.ds_header.io_time = ros::Time::now();

  if (autoHeadingEnabled() && std::abs(ref.r.value) < headingRateDeadband())
  {
    if (ref.heading.valid && state.heading.valid)
    {
      error.heading.valid = true;
      error.heading.value = ds_util::angular_separation_radians(state.heading.value, ref.heading.value);
    }
  }
  else
  {
    error.heading.valid = true;
    error.heading.value = 0;
  }

  if (ref.surge_u.valid)
  {
    error.surge_u = ref.surge_u;
  }

  if (ref.heave_w.valid)
  {
    error.heave_w = ref.heave_w;
  }

  return error;
}

geometry_msgs::WrenchStamped JoystickController::calculateForces(ds_nav_msgs::AggregatedState reference,
                                                                 ds_nav_msgs::AggregatedState state, ros::Duration dt)
{
  auto wrench = geometry_msgs::WrenchStamped{};
  wrench.header.stamp = ros::Time::now();
  wrench.header.frame_id = wrenchFrameId();

  DS_D(JoystickController);
  if (autoHeadingEnabled() && std::abs(reference.r.value) <= headingRateDeadband())
  {
    if (state.heading.valid && reference.heading.valid)
    {
      auto dt_ = dt.toSec();
      ROS_DEBUG_STREAM_THROTTLE(2, "Actual heading: " << state.heading.value * 180 / M_PI
                                                      << " Desired: " << reference.heading.value * 180 / M_PI);
      if (dt.toSec() < 0.01 || dt.toSec() > 10.0)
      {
        ROS_ERROR_STREAM("Skipping joystick closed-loop heading control execution due to bad dt: " << dt.toSec());
      }
      else
      {
        // Use a measured rotational rate if provided.
        if (state.r.valid)
        {
          wrench.wrench.torque.z = d->pid_heading.calculate_with_measured_rate(
              state.heading.value, reference.heading.value, state.r.value, 0, dt_);
        }
        else
        {
          wrench.wrench.torque.z = d->pid_heading.calculate(state.heading.value, reference.heading.value, dt_);
        }
      }
    }
    else if (!state.heading.valid)
    {
      ROS_ERROR_STREAM("Last nav state heading invalid!");
    }
    else if (!reference.heading.valid)
    {
      ROS_ERROR_STREAM("No valid auto heading value set.");
    }
  }
  else
  {
    wrench.wrench.torque.z = reference.r.value * d->force_scale_.torque.z;
  }

  if (reference.surge_u.valid)
  {
    wrench.wrench.force.x = reference.surge_u.value * d->force_scale_.force.x;
  }
  else
  {
    ROS_DEBUG_STREAM("Invalid reference 'surge_u'");
  }

  if (reference.sway_v.valid)
  {
    wrench.wrench.force.y = reference.sway_v.value * d->force_scale_.force.y;
  }
  else
  {
    ROS_DEBUG_STREAM("Invalid reference 'heave_w'");
  }

  if (reference.heave_w.valid)
  {
    wrench.wrench.force.z = reference.heave_w.value * d->force_scale_.force.z;
  }
  else
  {
    ROS_DEBUG_STREAM("Invalid reference 'heave_w'");
  }

  return wrench;
}

void JoystickController::updateState(const ds_nav_msgs::AggregatedState& msg)
{
  // The reference heading needs to track the vehicle heading when closed-loop
  // heading control is disabled because the desired behavior is for the vehicle
  // to hold the last heading value once engaged.

  auto last_ref = reference();

  if (!enabled() || !autoHeadingEnabled() || std::abs(last_ref.r.value) > headingRateDeadband())
  {
    if (msg.heading.valid)
    {
      setAutoHeadingValueRadians(msg.heading.value);
    }
    else
    {
      ROS_ERROR_STREAM("Cannot update auto heading tracking; invalid heading from state message");
    }
  }

  const auto last_state = state();
  ControllerBase::updateState(msg);

  if (!last_state.header.stamp.isValid())
  {
    ROS_ERROR_STREAM("Received nav state estimate with invalid timestamp");
    return;
  }

  const auto dt = msg.header.stamp - last_state.header.stamp;
  if (autoHeadingEnabled())
  {
    executeController(reference(), msg, std::move(dt));
  }
}

void JoystickController::setup()
{
  ControllerBase::setup();
  auto nh = nodeHandle();
  DS_D(JoystickController);
  d->pid_heading.enableStatePublishing(nh, "heading_pid_state");
}

void JoystickController::setupReferences()
{
  const auto& ref_map = referenceMap();
  DS_D(JoystickController);

  auto it = ref_map.find("joystick");
  if (it != std::end(ref_map))
  {
    d->joystick_ref_sub_ =
        nodeHandle().subscribe(it->second + "/goal_state", 1, &JoystickController::joystickReferenceCallback, this);
  }
  else
  {
    ROS_FATAL("No 'joystick' reference defined for surface controller");
    ROS_BREAK();
  }
}

void JoystickController::updateReference(const ds_nav_msgs::AggregatedState& msg)
{
  const auto last_ref = reference();
  ControllerBase::updateReference(msg);

  if (autoHeadingEnabled())
  {
    return;
  }

  if (!last_ref.header.stamp.isValid())
  {
    ROS_ERROR_STREAM("Received reference with invalid timestamp");
    return;
  }

  const auto dt = msg.header.stamp - last_ref.header.stamp;
  executeController(msg, state(), std::move(dt));
}
void JoystickController::setForceScale(const geometry_msgs::Wrench& scale)
{
  DS_D(JoystickController);
  d->force_scale_ = scale;
}

geometry_msgs::Wrench JoystickController::forceScale() const noexcept
{
  const DS_D(JoystickController);
  return d->force_scale_;
}
void JoystickController::setupParameters()
{
  ControllerBase::setupParameters();

  DS_D(JoystickController);
  d->pid_heading.setupFromParameterServer("~heading_pid");

  d->force_scale_.force.x = ros::param::param<double>("~/force_scale/u", 10);
  d->force_scale_.force.y = ros::param::param<double>("~/force_scale/v", 10);
  d->force_scale_.force.z = ros::param::param<double>("~/force_scale/w", 10);
  d->force_scale_.torque.z = ros::param::param<double>("~/force_scale/heading", 10);
}

Pid* JoystickController::pidHeading()
{
  DS_D(JoystickController);
  return static_cast<Pid*>(&d->pid_heading);
}

void JoystickController::setupServices()
{
  DsProcess::setupServices();

  DS_D(JoystickController);

  // auto overload = static_cast<bool
  // (JoystickController::*)(ds_base::BoolCommand&)>(&JoystickController::setAutoHeadingEnabled);

  d->enable_close_loop_heading_service_ =
      nodeHandle().advertiseService(ros::this_node::getName() + "/enable_closed_loop_heading_control",
                                    &JoystickController::setAutoHeadingEnabled, this);
}

void JoystickController::reset()
{
  ControllerBase::reset();
  DS_D(JoystickController);
  d->pid_heading.reset();
}

}
