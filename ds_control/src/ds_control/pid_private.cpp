/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "pid_private.h"

namespace ds_control
{
double PidPrivate::calculate_impl(double x, double x_ref, double dt)
{
  const auto x_error = error_wrap_func_ ? error_wrap_func_(x, x_ref) : x_ref - x;

  integral_ += x_error * integral_falloff_ * dt;

  integral_ = std::min(integral_, pid_state_msg_.i_max);
  integral_ = std::max(integral_, pid_state_msg_.i_min);

  const auto x_error_dot = (x_error - pid_state_msg_.error) / dt;

  pid_state_msg_.header.stamp = ros::Time::now();
  pid_state_msg_.timestep.fromSec(dt);

  pid_state_msg_.p_error = pid_state_msg_.p_term * x_error;
  pid_state_msg_.i_error = pid_state_msg_.i_term * integral_;
  pid_state_msg_.d_error = pid_state_msg_.d_term * x_error_dot;

  pid_state_msg_.output = pid_state_msg_.p_error + pid_state_msg_.i_error + pid_state_msg_.d_error;
  pid_state_msg_.output = std::min(pid_state_msg_.output, max_);
  pid_state_msg_.output = std::max(pid_state_msg_.output, min_);

  if (std::abs(pid_state_msg_.output) < output_deadband_)
  {
    pid_state_msg_.output = 0;
  }

  pid_state_msg_.error = x_error;
  pid_state_msg_.error_dot = x_error_dot;

  if (pid_pub_enabled_)
  {
    pid_pub_.publish(pid_state_msg_);
  }

  return pid_state_msg_.output;
}

double PidPrivate::calculate_with_measured_rate_impl(double x, double x_ref, double x_dot, double x_ref_dot, double dt)
{
  const auto x_error = error_wrap_func_ ? error_wrap_func_(x, x_ref) : x_ref - x;

  integral_ += x_error * integral_falloff_ * dt;
  integral_ = std::min(integral_, pid_state_msg_.i_max);
  integral_ = std::max(integral_, pid_state_msg_.i_min);

  const auto x_error_dot = x_ref_dot - x_dot;

  pid_state_msg_.header.stamp = ros::Time::now();
  pid_state_msg_.timestep.fromSec(dt);

  pid_state_msg_.p_error = pid_state_msg_.p_term * x_error;
  pid_state_msg_.i_error = pid_state_msg_.i_term * integral_;
  pid_state_msg_.d_error = pid_state_msg_.d_term * x_error_dot;

  pid_state_msg_.output = pid_state_msg_.p_error + pid_state_msg_.i_error + pid_state_msg_.d_error;
  pid_state_msg_.output = std::min(pid_state_msg_.output, max_);
  pid_state_msg_.output = std::max(pid_state_msg_.output, min_);

  if (std::abs(pid_state_msg_.output) < output_deadband_)
  {
    pid_state_msg_.output = 0;
  }

  pid_state_msg_.error = x_error;
  pid_state_msg_.error_dot = x_error_dot;

  if (pid_pub_enabled_)
  {
    pid_pub_.publish(pid_state_msg_);
  }

  return pid_state_msg_.output;
}
}
