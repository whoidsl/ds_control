/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_CONTROL_SURVEY_CONTROLLER_PRIVATE_H
#define DS_CONTROL_SURVEY_CONTROLLER_PRIVATE_H

#include "ds_control/pid_heading.h"
#include "ds_control/pid.h"

#include "ds_util/reference_smoothing.h"

namespace ds_control
{
struct SurveyControllerPrivate
{
  SurveyControllerPrivate()
    : heading_smoother_(std::unique_ptr<ds_util::TrapezoidalSmoother>(new ds_util::TrapezoidalSmoother))
    , depth_smoother_(std::unique_ptr<ds_util::TrapezoidalSmoother>(new ds_util::TrapezoidalSmoother))
    , velocity_smoother_(std::unique_ptr<ds_util::TrapezoidalSmoother>(new ds_util::TrapezoidalSmoother))
    , default_depth_smoothing_velocity_(0.1)
    , default_depth_smoothing_acceleration_(0.01)
  {
    // heading_smoother_->setWrapFunction([](double x) {return std::fmod(x, 2*M_PI);});
  }

  ~SurveyControllerPrivate() = default;

  PidHeading pid_heading;
  Pid pid_surge_u;
  Pid pid_down;

  std::unique_ptr<ds_util::TrapezoidalSmoother> heading_smoother_;
  std::unique_ptr<ds_util::TrapezoidalSmoother> depth_smoother_;
  std::unique_ptr<ds_util::TrapezoidalSmoother> velocity_smoother_;

  ros::Subscriber trackline_ref_sub_;
  ds_nav_msgs::AggregatedState last_trackline_ref_;
  ros::Subscriber bottom_follower_ref_sub_;
  ros::Subscriber bottom_follower_state_sub_;
  ds_nav_msgs::AggregatedState last_bottom_follower_ref_;

  double default_depth_smoothing_velocity_;
  double default_depth_smoothing_acceleration_;
};

void SurveyController::reset()
{
  ControllerBase::reset();
  DS_D(SurveyController);
  d->pid_heading.reset();
  d->pid_down.reset();
  d->pid_surge_u.reset();
}
}
#endif  // DS_CONTROL_SURVEY_CONTROLLER_PRIVATE_H
