/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_BASE_CONTROLLER_BASE_PRIVATE_H
#define DS_BASE_CONTROLLER_BASE_PRIVATE_H

#include "ds_control/controller_base.h"
#include "ds_param/ds_param_conn.h"

namespace ds_control
{
struct ControllerBasePrivate
{
  ControllerBasePrivate() : is_enabled_(false), frame_id_("base_link")
  {
    auto invalid = ds_nav_msgs::FlaggedDouble{};
    invalid.valid = false;
    invalid.valid = 0;

    last_reference_.northing = invalid;
    last_reference_.easting = invalid;
    last_reference_.down = invalid;

    last_reference_.surge_u = invalid;
    last_reference_.sway_v = invalid;
    last_reference_.heave_w = invalid;

    last_reference_.du_dt = invalid;
    last_reference_.dv_dt = invalid;
    last_reference_.dw_dt = invalid;

    last_reference_.roll = invalid;
    last_reference_.pitch = invalid;
    last_reference_.heading = invalid;

    last_reference_.p = invalid;
    last_reference_.q = invalid;
    last_reference_.r = invalid;

    last_reference_.dp_dt = invalid;
    last_reference_.dq_dt = invalid;
    last_reference_.dr_dt = invalid;

    last_state_.northing = invalid;
    last_state_.easting = invalid;
    last_state_.down = invalid;

    last_state_.surge_u = invalid;
    last_state_.sway_v = invalid;
    last_state_.heave_w = invalid;

    last_state_.du_dt = invalid;
    last_state_.dv_dt = invalid;
    last_state_.dw_dt = invalid;

    last_state_.roll = invalid;
    last_state_.pitch = invalid;
    last_state_.heading = invalid;

    last_state_.p = invalid;
    last_state_.q = invalid;
    last_state_.r = invalid;

    last_state_.dp_dt = invalid;
    last_state_.dq_dt = invalid;
    last_state_.dr_dt = invalid;
  }
  ~ControllerBasePrivate() = default;

  ds_nav_msgs::AggregatedState last_reference_;
  ds_nav_msgs::AggregatedState last_state_;

  bool is_enabled_;
  ros::Subscriber state_update_sub_;
  ros::Publisher wrench_pub_;
  ros::Publisher wrench_priv_pub_;
  ros::Publisher reference_pub_;
  ros::Publisher reference_priv_pub_;
  ros::Publisher error_pub_;
  ros::Publisher error_priv_pub_;

  ds_param::ParamConnection::Ptr param_sub_;
  ds_param::IntParam::Ptr active_controller_;
  ds_param::IntParam::Ptr active_allocation_;

  ControllerBase::ReferenceMap reference_map_;
  std::string frame_id_;

  ControllerBase::ForceOutputLimits output_force_limits_;

};
}

#endif  // DS_BASE_CONTROLLER_BASE_PRIVATE_H
