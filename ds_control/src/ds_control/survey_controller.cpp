/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_util/angle.h"
#include "ds_control/survey_controller.h"
#include "survey_controller_private.h"

namespace ds_control
{
SurveyController::SurveyController()
  : ControllerBase(), d_ptr_(std::unique_ptr<SurveyControllerPrivate>(new SurveyControllerPrivate))
{
}

SurveyController::SurveyController(int argc, char** argv, const std::string& name)
  : ControllerBase(argc, argv, name), d_ptr_(std::unique_ptr<SurveyControllerPrivate>(new SurveyControllerPrivate))
{
}

SurveyController::~SurveyController() = default;

Pid* SurveyController::pidHeading()
{
  DS_D(SurveyController);
  return static_cast<Pid*>(&d->pid_heading);
}

Pid* SurveyController::pidSurgeU()
{
  DS_D(SurveyController);
  return &d->pid_surge_u;
}

Pid* SurveyController::pidDown()
{
  DS_D(SurveyController);
  return &d->pid_down;
}

void SurveyController::setup()
{
  ControllerBase::setup();
  auto nh = nodeHandle();

  DS_D(SurveyController);
  d->pid_heading.enableStatePublishing(nh, "heading_pid_state");
  d->pid_surge_u.enableStatePublishing(nh, "surge_pid_state");
  d->pid_down.enableStatePublishing(nh, "down_pid_state");
}

void SurveyController::setupParameters()
{
  ControllerBase::setupParameters();

  DS_D(SurveyController);
  d->pid_heading.setupFromParameterServer("~heading_pid");
  d->pid_down.setupFromParameterServer("~down_pid");
  d->pid_surge_u.setupFromParameterServer("~surge_u_pid");

  auto vel = ros::param::param<double>("~smoothers/heading/max_velocity", 10);
  auto accel = ros::param::param<double>("~smoothers/heading/max_accel", M_PI / 180.0);
  auto smoother = headingReferenceSmoother();
  smoother->setMaxAcceleration(accel);
  smoother->setMaxVelocity(vel);

  vel = ros::param::param<double>("~smoothers/depth/max_velocity", 0.2);
  accel = ros::param::param<double>("~smoothers/depth/max_accel", 0.01);
  d->default_depth_smoothing_velocity_ = vel;
  d->default_depth_smoothing_acceleration_ = accel;
  smoother = depthReferenceSmoother();
  smoother->setMaxAcceleration(accel);
  smoother->setMaxVelocity(vel);

  vel = ros::param::param<double>("~smoothers/surge_u/max_velocity", 0.1);
  accel = ros::param::param<double>("~smoothers/surge_u/max_accel", 0.01);
  smoother = surgeUSmoother();
  smoother->setMaxAcceleration(accel);
  smoother->setMaxVelocity(vel);
}

ds_nav_msgs::AggregatedState SurveyController::errorState(const ds_nav_msgs::AggregatedState& ref,
                                                          const ds_nav_msgs::AggregatedState& state)
{
  auto error = ds_nav_msgs::AggregatedState{};
  error.header = state.header;
  error.header.frame_id = wrenchFrameId();
  error.ds_header.io_time = ros::Time::now();

  if (ref.heading.valid && state.heading.valid)
  {
    error.heading.valid = true;
    error.heading.value = ds_util::angular_separation_radians(state.heading.value, ref.heading.value);
  }

  if (ref.r.valid && state.r.valid)
  {
    error.r.valid = true;
    error.r.value = ref.r.value - state.r.value;
  }

  if (ref.down.valid && state.down.valid)
  {
    error.down.valid = true;
    error.down.value = ref.down.value - state.down.value;
  }

  if (ref.heave_w.valid && state.heave_w.valid)
  {
    error.heave_w.valid = true;
    error.heave_w.value = ref.heave_w.value - state.heave_w.value;
  }

  if (ref.surge_u.valid && state.surge_u.valid)
  {
    error.surge_u.valid = true;
    error.surge_u.value = ref.surge_u.value - state.surge_u.value;
  }

  if (ref.du_dt.valid && state.du_dt.valid)
  {
    error.du_dt.valid = true;
    error.du_dt.value = ref.du_dt.value - state.du_dt.value;
  }

  return error;
}
geometry_msgs::WrenchStamped SurveyController::calculateForces(ds_nav_msgs::AggregatedState reference,
                                                               ds_nav_msgs::AggregatedState state, ros::Duration dt)
{
  auto wrench = geometry_msgs::WrenchStamped{};
  wrench.header.stamp = ros::Time::now();
  wrench.header.frame_id = wrenchFrameId();

  const auto dt_ = dt.toSec();
  if (reference.heading.valid && state.heading.valid)
  {
    auto pid = pidHeading();
    wrench.wrench.torque.z = pid->calculate_with_measured_rate(state.heading.value, reference.heading.value,
                                                               state.r.value, reference.r.value, dt_);
  }
  else if (!reference.heading.valid)
  {
    ROS_ERROR_STREAM_THROTTLE(0.5, "No heading torque produced: Invalid reference heading.");
  }
  else if (!state.heading.valid)
  {
    ROS_ERROR_STREAM_THROTTLE(0.5, "No heading torque produced: Invalid state heading.");
  }

  if (reference.surge_u.valid && state.surge_u.valid)
  {
    auto pid = pidSurgeU();
    wrench.wrench.force.x = pid->calculate_with_measured_rate(state.surge_u.value, reference.surge_u.value,
                                                              state.du_dt.value, reference.du_dt.value, dt_);
  }
  else if (!reference.surge_u.valid)
  {
    ROS_ERROR_STREAM_THROTTLE(0.5, "No U force produced: Invalid reference surge_u.");
  }
  else if (!state.surge_u.valid)
  {
    ROS_ERROR_STREAM_THROTTLE(0.5, "No U force produced: Invalid state surge_u.");
  }

  if (reference.down.valid && state.down.valid)
  {
    auto pid = pidDown();
    wrench.wrench.force.z = pid->calculate_with_measured_rate(state.down.value, reference.down.value,
                                                              state.heave_w.value, reference.heave_w.value, dt_);
  }
  else if (!reference.down.valid)
  {
    ROS_ERROR_STREAM_THROTTLE(0.5, "No W force produced: Invalid reference down.");
  }
  else if (!state.down.valid)
  {
    ROS_ERROR_STREAM_THROTTLE(0.5, "No W force produced: Invalid state down.");
  }

  return wrench;
}
void SurveyController::updateState(const ds_nav_msgs::AggregatedState& msg)
{
  const auto last_state = state();
  ControllerBase::updateState(msg);

  if (!enabled())
  {
    // Commenting out for now -- seems correct but untested
    // if(!msg.header.stamp.isValid())
    // {
    //   return;
    // }

    auto ref = reference();
    if (msg.heading.valid)
    {
      ref.heading = msg.heading;
    }

    if (msg.surge_u.valid)
    {
      ref.surge_u = msg.surge_u;
    }

    if (msg.down.valid)
    {
      ref.down = msg.down;
    }

    // Commenting out for now -- seems correct but untested
    ref.header.stamp = msg.header.stamp;
    updateReference(ref);

    return;
  }

  if (!last_state.header.stamp.isValid())
  {
    return;
  }

  const auto dt = msg.header.stamp - last_state.header.stamp;

  if (dt.toSec() < 0.01)
  {
    ROS_ERROR_STREAM("Skipping controller execution due to small dt: " << dt);
    return;
  }
  else if (dt.toSec() > 10)
  {
    ROS_ERROR_STREAM("Skipping controller executing due to large dt: " << dt);
    return;
  }

  const auto ref = reference();
  if (!ref.header.stamp.isValid())
  {
    ROS_ERROR_STREAM("Skipping controller execution, reference timestamp is invalid.");
    return;
  }
  // Execute the control loop when enabled
  executeController(reference(), msg, std::move(dt));
}

ds_util::TrapezoidalSmoother* SurveyController::headingReferenceSmoother() noexcept
{
  DS_D(SurveyController);
  return d->heading_smoother_.get();
}
ds_util::TrapezoidalSmoother* SurveyController::surgeUSmoother() noexcept
{
  DS_D(SurveyController);
  return d->velocity_smoother_.get();
}
ds_util::TrapezoidalSmoother* SurveyController::depthReferenceSmoother() noexcept
{
  DS_D(SurveyController);
  return d->depth_smoother_.get();
}

void SurveyController::legGoalCallback(const ds_nav_msgs::AggregatedState& msg)
{
  if (!msg.heading.valid)
  {
    ROS_ERROR_THROTTLE(0.5, "Trackline reference provided an invalid heading");
    return;
  }

  // Disabled, or this is the first leg reference we've received...  Just store the
  // message.
  DS_D(SurveyController);
  if (!d->last_trackline_ref_.header.stamp.isValid())
  {
    d->last_trackline_ref_ = msg;
    return;
  }

  const auto dt = (msg.header.stamp - d->last_trackline_ref_.header.stamp).toSec();
  d->last_trackline_ref_ = msg;

  // When the controller is not enabled, set the smoother reference value to the goal state.
  if (!enabled())
  {
    auto smoother = headingReferenceSmoother();
    smoother->setReference(msg.heading.value, 0, 0);
    return;
  }

  // update the heading reference with a smoothed version.
  auto heading_pos = 0.0;
  auto heading_vel = 0.0;
  auto heading_acc = 0.0;
  std::tie(heading_pos, heading_vel, heading_acc) = headingReferenceSmoother()->execute(msg.heading.value, dt);

  auto ref = reference();
  ref.heading.valid = true;
  ref.heading.value = heading_pos;
  ref.r.valid = true;
  ref.r.value = heading_vel;
  ref.header.stamp = msg.header.stamp;
  updateReference(ref);
}

void SurveyController::bottomFollowerGoalCallback(const ds_nav_msgs::AggregatedState& msg)
{
  if (!msg.header.stamp.isValid())
  {
    ROS_ERROR_STREAM_THROTTLE(0.5, "Bottom Follower referenced provided an invalid timestamp");
    return;
  }

  if (!msg.down.valid)
  {
    ROS_ERROR_STREAM_THROTTLE(0.5, "Bottom Follower reference provided an invalid depth");
    return;
  }

  if (!msg.surge_u.valid)
  {
    ROS_ERROR_STREAM_THROTTLE(0.5, "Bottom Follower reference provided an invalid surge_u");
    return;
  }

  DS_D(SurveyController);
  if (!d->last_bottom_follower_ref_.header.stamp.isValid())
  {
    d->last_bottom_follower_ref_ = msg;
    return;
  }

  const auto dt = (msg.header.stamp - d->last_bottom_follower_ref_.header.stamp).toSec();
  d->last_bottom_follower_ref_ = msg;

  // When the controller is not enabled, set the smoother reference value to the goal state.
  if (!enabled())
  {
    // The nav state callback automagically updates the reference to be the current
    // nav state when disabled.  Think of this code snippet as "reinitialize smoothing
    // with no velocity about the current reference"
    auto ref = reference();
    auto smoother = depthReferenceSmoother();
    if (ref.down.valid)
    {
      smoother->setReference(ref.down.value, 0, 0);
    }

    smoother = surgeUSmoother();
    if (ref.surge_u.valid)
    {
      smoother->setReference(ref.surge_u.value, 0, 0);
    }
    return;
  }

  auto depth_pos = 0.0;
  auto depth_vel = 0.0;
  auto depth_acc = 0.0;
  std::tie(depth_pos, depth_vel, depth_acc) = depthReferenceSmoother()->execute(msg.down.value, dt);

  ROS_DEBUG("dt: %f, BF depth: %.2f, Smoothed depth: %.2f", dt, msg.down.value, depth_pos);

  auto u_vel = 0.0;
  auto u_acc = 0.0;
  auto u_extra = 0.0;
  std::tie(u_vel, u_acc, u_extra) = surgeUSmoother()->execute(msg.surge_u.value, dt);

  // update the heading reference with a smoothed version.
  auto ref = reference();
  ref.down.valid = true;
  ref.down.value = depth_pos;
  ref.heave_w.valid = true;
  ref.heave_w.value = depth_vel;

  ref.surge_u.valid = true;
  ref.surge_u.value = u_vel;
  ref.du_dt.valid = true;
  ref.du_dt.value = u_acc;
  ref.header.stamp = msg.header.stamp;

  updateReference(ref);
}

void SurveyController::setupReferences()
{
  const auto& refs = referenceMap();

  auto it = refs.find("leg");
  if (it == std::end(refs))
  {
    ROS_FATAL("No 'leg' reference provided for survey controller");
    ROS_BREAK();
  }

  auto nh = nodeHandle();
  auto topic = ros::names::resolve(it->second, std::string{ "goal_state" });
  DS_D(SurveyController);
  d->trackline_ref_sub_ = nh.subscribe(topic, 1, &SurveyController::legGoalCallback, this);

  it = refs.find("bottom_follower");
  if (it == std::end(refs))
  {
    ROS_FATAL("No 'bottom_follower' reference provided for survey controller");
    ROS_BREAK();
  }
  topic = ros::names::resolve(it->second, std::string{ "goal_state" });
  d->bottom_follower_ref_sub_ = nh.subscribe(topic, 1, &SurveyController::bottomFollowerGoalCallback, this);

  topic = ros::names::resolve(it->second, std::string{ "bf_topic" });
  d->bottom_follower_state_sub_ = nh.subscribe(topic, 1, &SurveyController::bottomFollowerStateCallback, this);
}

void SurveyController::bottomFollowerStateCallback(const ds_control_msgs::BottomFollow1D& msg)
{
  if (!msg.header.stamp.isValid())
  {
    return;
  }

  DS_D(SurveyController);
  auto smoother = d->depth_smoother_.get();

  const auto vel = msg.depth_rate_d == 0.0 ? defaultDepthSmootherVelocity() : msg.depth_rate_d;
  const auto acc = msg.depth_accel_d == 0.0 ? defaultDepthSmootherAcceleration() : msg.depth_accel_d;

  if (smoother->maxVelocity() != vel)
  {
    ROS_INFO("Setting velocity smoother max velocity to %.3f", vel);
    smoother->setMaxVelocity(vel);
  }

  if (smoother->maxAcceleration() != acc)
  {
    ROS_INFO("Setting velocity smoother max acceleration to %.3f", acc);
    smoother->setMaxAcceleration(acc);
  }
}

double SurveyController::defaultDepthSmootherVelocity() const noexcept
{
  const DS_D(SurveyController);
  return d->default_depth_smoothing_velocity_;
}

double SurveyController::defaultDepthSmootherAcceleration() const noexcept
{
  const DS_D(SurveyController);
  return d->default_depth_smoothing_acceleration_;
}

}
