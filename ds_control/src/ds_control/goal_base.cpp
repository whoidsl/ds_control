/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_control/goal_base.h"
#include "goal_base_private.h"

namespace ds_control
{
GoalBase::GoalBase() : ds_base::DsProcess(), d_ptr_(std::unique_ptr<GoalBasePrivate>(new GoalBasePrivate))
{
}
GoalBase::GoalBase(int argc, char** argv, const std::string& name)
  : ds_base::DsProcess(argc, argv, name), d_ptr_(std::unique_ptr<GoalBasePrivate>(new GoalBasePrivate))
{
}

GoalBase::~GoalBase() = default;

void GoalBase::publishGoal(const ds_nav_msgs::AggregatedState& goal)
{
  DS_D(GoalBase);
  d->ref_pub_.publish(goal);
}

void GoalBase::setupPublishers()
{
  DsProcess::setupPublishers();
  DS_D(GoalBase);
  const auto output_topic = ros::param::param<std::string>("~goal_output_topic", "goal_state");
  d->ref_pub_ =
      nodeHandle().advertise<ds_nav_msgs::AggregatedState>(ros::this_node::getName() + "/" + output_topic, 10, false);
}
}
