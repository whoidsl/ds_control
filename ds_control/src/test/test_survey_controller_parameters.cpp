/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_control/survey_controller.h"
#include "ds_control/pid_heading.h"

#include <gtest/gtest.h>
#include <random>

using namespace ds_control;

class SurveyControllerTest: public SurveyController
{
 public:
  SurveyControllerTest(): SurveyController(){}
  ~SurveyControllerTest() override = default;

  uint64_t type() const noexcept override {
    return 0;
  }
};

class ControllerTest : public ::testing::Test
{
 protected:
  void SetUp() override
  {
    controller_ = std::unique_ptr<SurveyControllerTest>(new SurveyControllerTest);
  }

  std::unique_ptr<SurveyControllerTest> controller_;
};

TEST_F(ControllerTest, read_pid_parameters)
{
  std::random_device rd;  //Will be used to obtain a seed for the random number engine
  std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
  std::uniform_real_distribution<> dis(-100., 100.0);

  const auto heading_kp = dis(gen);
  const auto heading_kd = dis(gen);
  const auto heading_ki = dis(gen);

  const auto heading_max_output_saturation = dis(gen);
  const auto heading_min_output_saturation = dis(gen);

  const auto heading_min_integrated_error = dis(gen);
  const auto heading_max_integrated_error = dis(gen);

  ros::param::set("~heading_pid/gains/kp", heading_kp);
  ros::param::set("~heading_pid/gains/kd", heading_kd);
  ros::param::set("~heading_pid/gains/ki", heading_ki);

  ros::param::set("~heading_pid/integrated_error_limits/min", heading_min_integrated_error);
  ros::param::set("~heading_pid/integrated_error_limits/max", heading_max_integrated_error);

  ros::param::set("~heading_pid/output_saturation_limits/min", heading_min_output_saturation);
  ros::param::set("~heading_pid/output_saturation_limits/max", heading_max_output_saturation);

  const auto surge_u_kp = dis(gen);
  const auto surge_u_kd = dis(gen);
  const auto surge_u_ki = dis(gen);

  const auto surge_u_max_output_saturation = dis(gen);
  const auto surge_u_min_output_saturation = dis(gen);

  const auto surge_u_min_integrated_error = dis(gen);
  const auto surge_u_max_integrated_error = dis(gen);

  ros::param::set("~surge_u_pid/gains/kp", surge_u_kp);
  ros::param::set("~surge_u_pid/gains/kd", surge_u_kd);
  ros::param::set("~surge_u_pid/gains/ki", surge_u_ki);

  ros::param::set("~surge_u_pid/integrated_error_limits/min", surge_u_min_integrated_error);
  ros::param::set("~surge_u_pid/integrated_error_limits/max", surge_u_max_integrated_error);

  ros::param::set("~surge_u_pid/output_saturation_limits/min", surge_u_min_output_saturation);
  ros::param::set("~surge_u_pid/output_saturation_limits/max", surge_u_max_output_saturation);

  const auto heave_w_kp = dis(gen);
  const auto heave_w_kd = dis(gen);
  const auto heave_w_ki = dis(gen);

  const auto heave_w_max_output_saturation = dis(gen);
  const auto heave_w_min_output_saturation = dis(gen);

  const auto heave_w_min_integrated_error = dis(gen);
  const auto heave_w_max_integrated_error = dis(gen);

  ros::param::set("~down_pid/gains/kp", heave_w_kp);
  ros::param::set("~down_pid/gains/kd", heave_w_kd);
  ros::param::set("~down_pid/gains/ki", heave_w_ki);

  ros::param::set("~down_pid/integrated_error_limits/min", heave_w_min_integrated_error);
  ros::param::set("~down_pid/integrated_error_limits/max", heave_w_max_integrated_error);

  ros::param::set("~down_pid/output_saturation_limits/min", heave_w_min_output_saturation);
  ros::param::set("~down_pid/output_saturation_limits/max", heave_w_max_output_saturation);

  controller_->setup();

  auto pid = controller_->pidHeading();

  EXPECT_FLOAT_EQ(heading_kp, pid->kp());
  EXPECT_FLOAT_EQ(heading_kd, pid->kd());
  EXPECT_FLOAT_EQ(heading_ki, pid->ki());

  EXPECT_FLOAT_EQ(heading_min_integrated_error, pid->minIntegratedError());
  EXPECT_FLOAT_EQ(heading_max_integrated_error, pid->maxIntegratedError());

  EXPECT_FLOAT_EQ(heading_min_output_saturation, pid->minSaturation());
  EXPECT_FLOAT_EQ(heading_max_output_saturation, pid->maxSaturation());

  pid = controller_->pidSurgeU();

  EXPECT_FLOAT_EQ(surge_u_kp, pid->kp());
  EXPECT_FLOAT_EQ(surge_u_kd, pid->kd());
  EXPECT_FLOAT_EQ(surge_u_ki, pid->ki());

  EXPECT_FLOAT_EQ(surge_u_min_integrated_error, pid->minIntegratedError());
  EXPECT_FLOAT_EQ(surge_u_max_integrated_error, pid->maxIntegratedError());

  EXPECT_FLOAT_EQ(surge_u_min_output_saturation, pid->minSaturation());
  EXPECT_FLOAT_EQ(surge_u_max_output_saturation, pid->maxSaturation());

  pid = controller_->pidDown();

  EXPECT_FLOAT_EQ(heave_w_kp, pid->kp());
  EXPECT_FLOAT_EQ(heave_w_kd, pid->kd());
  EXPECT_FLOAT_EQ(heave_w_ki, pid->ki());

  EXPECT_FLOAT_EQ(heave_w_min_integrated_error, pid->minIntegratedError());
  EXPECT_FLOAT_EQ(heave_w_max_integrated_error, pid->maxIntegratedError());

  EXPECT_FLOAT_EQ(heave_w_min_output_saturation, pid->minSaturation());
  EXPECT_FLOAT_EQ(heave_w_max_output_saturation, pid->maxSaturation());
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  ros::init(argc, argv, "survey_controller");
  // ros::AsyncSpinner spinner(1);
  // spinner.start();
  testing::InitGoogleTest(&argc, argv);
  auto ret = RUN_ALL_TESTS();
  // spinner.stop();
  ros::shutdown();
  ros::waitForShutdown();
  return ret;
};
