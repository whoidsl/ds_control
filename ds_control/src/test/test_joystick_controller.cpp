/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_control/joystick_controller.h"
#include "ds_control/pid_heading.h"
#include "ds_util/time_validate.h"

#include <gtest/gtest.h>

using namespace ds_control;

class JoystickControllerTest: public JoystickController
{
 public:
  JoystickControllerTest(): JoystickController(){}
  ~JoystickControllerTest() override = default;

  uint64_t type() const noexcept override {
    return 0;
  }

};

class ControllerTest : public ::testing::Test
{
 protected:
  void SetUp() override
  {
    controller_ = std::unique_ptr<JoystickControllerTest>(new JoystickControllerTest);
  }

  std::unique_ptr<JoystickControllerTest> controller_;
};

TEST_F(ControllerTest, invalid_timestamp)
{
  auto time = ros::Time{};
  ASSERT_FLOAT_EQ(0, time.toSec());
  // This test does not mean what we think it means
  ASSERT_TRUE(time.isValid());
  EXPECT_FALSE(ds_util::isTimeInstanceValid(time));
}

TEST_F(ControllerTest, read_parameters)
{
  const auto kp = 34.0;
  const auto kd = -12345.123;
  const auto ki = -124.902;

  const auto max_output_saturation = -2.193752;
  const auto min_output_saturation = 201.2018;

  const auto min_integrated_error = -37.01827;
  const auto max_integrated_error = 872.1027;

  const auto integration_falloff = 0.5;

  ros::param::set("active_controller", 0);
  ros::param::set("~heading_pid/gains/kp", kp);
  ros::param::set("~heading_pid/gains/kd", kd);
  ros::param::set("~heading_pid/gains/ki", ki);

  ros::param::set("~heading_pid/integrated_error_falloff", integration_falloff);

  ros::param::set("~heading_pid/integrated_error_limits/min", min_integrated_error);
  ros::param::set("~heading_pid/integrated_error_limits/max", max_integrated_error);

  ros::param::set("~heading_pid/output_saturation_limits/min", min_output_saturation);
  ros::param::set("~heading_pid/output_saturation_limits/max", max_output_saturation);

  // controller_ = std::unique_ptr<JoystickControllerTest>(new JoystickControllerTest);
  controller_->setup();

  auto pid = controller_->pidHeading();

  EXPECT_FLOAT_EQ(kp, pid->kp());
  EXPECT_FLOAT_EQ(kd, pid->kd());
  EXPECT_FLOAT_EQ(ki, pid->ki());

  EXPECT_FLOAT_EQ(integration_falloff, pid->integratedErrorFalloff());
  EXPECT_FLOAT_EQ(min_integrated_error, pid->minIntegratedError());
  EXPECT_FLOAT_EQ(max_integrated_error, pid->maxIntegratedError());

  EXPECT_FLOAT_EQ(min_output_saturation, pid->minSaturation());
  EXPECT_FLOAT_EQ(max_output_saturation, pid->maxSaturation());

}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  ros::init(argc, argv, "surface_joystick_controller");
  // ros::AsyncSpinner spinner(1);
  // spinner.start();
  testing::InitGoogleTest(&argc, argv);
  auto ret = RUN_ALL_TESTS();
  // spinner.stop();
  ros::shutdown();
  ros::waitForShutdown();
  return ret;
};
