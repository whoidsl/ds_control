/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_control/survey_controller.h"
#include "ds_control/pid_heading.h"
#include "ds_control/pid.h"

#include <gtest/gtest.h>

using namespace ds_control;

class Controller : public SurveyController
{
public:
  Controller() : SurveyController()
  {
  }

public:

  uint64_t type() const noexcept override
  {
    return 0;
  }

};

class ControllerTest : public ::testing::Test
{

protected:
  void SetUp() override
  {
    controller_ = std::unique_ptr<Controller>(new Controller);
    controller_->setup();
  }

  std::unique_ptr<Controller> controller_;
};

TEST_F(ControllerTest, zero_force)
{
  controller_->setEnabled(true);

  auto reference = ds_nav_msgs::AggregatedState{};
  reference.header.stamp = ros::Time::now();

  auto dof = ds_nav_msgs::FlaggedDouble{};
  dof.valid = true;
  dof.value = 1;

  // The reference is 1 in heading, surge_U and down
  reference.heading = dof;
  reference.down = dof;
  reference.surge_u = dof;

  auto state = reference;

  auto pid = controller_->pidSurgeU();
  // Give unity gains.
  pid->setKp(1.0);
  pid->setKd(0);
  pid->setKi(0);

  pid = controller_->pidDown();
  pid->setKp(1.0);
  pid->setKd(0);
  pid->setKi(0);

  pid =controller_->pidHeading();
  pid->setKp(1.0);
  pid->setKd(0);
  pid->setKi(0);

  // 1-second timestep
  auto dt = ros::Duration{1};

  auto wrench = controller_->calculateForces(reference, state, dt);

  EXPECT_FLOAT_EQ(0, wrench.wrench.force.x);
  EXPECT_FLOAT_EQ(0, wrench.wrench.force.z);
  EXPECT_FLOAT_EQ(0, wrench.wrench.torque.z);

  EXPECT_FLOAT_EQ(0, wrench.wrench.force.y);
  EXPECT_FLOAT_EQ(0, wrench.wrench.torque.x);
  EXPECT_FLOAT_EQ(0, wrench.wrench.torque.y);

}
TEST_F(ControllerTest, expected_forces)
{
  controller_->setEnabled(true);

  auto reference = ds_nav_msgs::AggregatedState{};
  reference.header.stamp = ros::Time::now();

  auto dof = ds_nav_msgs::FlaggedDouble{};
  dof.valid = true;
  dof.value = 1;

  // The reference is 1 in heading, surge_U and down
  reference.heading = dof;
  reference.down = dof;
  reference.surge_u = dof;

  auto state = ds_nav_msgs::AggregatedState{};
  dof.valid = true;
  dof.value = 0;
  state.header.stamp = reference.header.stamp;

  // The state is 0's
  state.heading = dof;
  state.down = dof;
  state.surge_u = dof;

  auto pid = controller_->pidSurgeU();
  // Give unity gains.
  pid->setKp(1.0);
  pid->setKd(0);
  pid->setKi(0);

  const auto expected_forward_force = pid->kp()*(reference.surge_u.value - state.surge_u.value);

  pid = controller_->pidDown();
  pid->setKp(1.0);
  pid->setKd(0);
  pid->setKi(0);

  const auto expected_down_force = pid->kp()*(reference.down.value - state.down.value);

  pid =controller_->pidHeading();
  pid->setKp(1.0);
  pid->setKd(0);
  pid->setKi(0);

  const auto expected_heading_torque = pid->kp()*(reference.heading.value - state.heading.value);

  // 1-second timestep
  auto dt = ros::Duration{1};

  auto wrench = controller_->calculateForces(reference, state, dt);

  EXPECT_FLOAT_EQ(expected_forward_force, wrench.wrench.force.x);
  EXPECT_FLOAT_EQ(expected_down_force, wrench.wrench.force.z);
  EXPECT_FLOAT_EQ(expected_heading_torque, wrench.wrench.torque.z);

  EXPECT_FLOAT_EQ(0, wrench.wrench.force.y);
  EXPECT_FLOAT_EQ(0, wrench.wrench.torque.x);
  EXPECT_FLOAT_EQ(0, wrench.wrench.torque.y);

}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  ros::init(argc, argv, "survey_controller");
  // ros::AsyncSpinner spinner(1);
  // spinner.start();
  testing::InitGoogleTest(&argc, argv);
  auto ret = RUN_ALL_TESTS();
  // spinner.stop();
  ros::shutdown();
  return ret;
};
