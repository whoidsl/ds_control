/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_control/pid.h"

#include <ros/ros.h>
#include <gtest/gtest.h>
#include <future>

using namespace ds_control;

class PidParamServerTest : public ::testing::Test
{
 protected:
  void SetUp() override
  {
    pid_ = std::unique_ptr<Pid>(new Pid);
  }

  std::unique_ptr<Pid> pid_;
};

TEST_F(PidParamServerTest, read_parameters)
{
  const auto kp = 34.0;
  const auto kd = -12345.123;
  const auto ki = -124.902;

  const auto max_output_saturation = -2.193752;
  const auto min_output_saturation = 201.2018;

  const auto min_integrated_error = -37.01827;
  const auto max_integrated_error = 872.1027;

  ros::param::set("gains/kp", kp);
  ros::param::set("gains/kd", kd);
  ros::param::set("gains/ki", ki);

  ros::param::set("integrated_error_limits/min", min_integrated_error);
  ros::param::set("integrated_error_limits/max", max_integrated_error);

  ros::param::set("output_saturation_limits/min", min_output_saturation);
  ros::param::set("output_saturation_limits/max", max_output_saturation);

  pid_->setupFromParameterServer(ros::this_node::getNamespace());

  EXPECT_FLOAT_EQ(kp, pid_->kp());
  EXPECT_FLOAT_EQ(kd, pid_->kd());
  EXPECT_FLOAT_EQ(ki, pid_->ki());

  EXPECT_FLOAT_EQ(min_integrated_error, pid_->minIntegratedError());
  EXPECT_FLOAT_EQ(max_integrated_error, pid_->maxIntegratedError());

  EXPECT_FLOAT_EQ(min_output_saturation, pid_->minSaturation());
  EXPECT_FLOAT_EQ(max_output_saturation, pid_->maxSaturation());
}

TEST_F(PidParamServerTest, read_infinities)
{


  const auto inf = std::numeric_limits<double>::infinity();
  ros::param::set("integrated_error_limits/min", -inf);
  ros::param::set("integrated_error_limits/max", inf);

  ros::param::set("output_saturation_limits/min", -inf);
  ros::param::set("output_saturation_limits/max", inf);

  pid_->setMaxSaturation(0);
  pid_->setMinSaturation(0);

  pid_->setMaxIntegratedError(0);
  pid_->setMinIntegratedError(0);

  ASSERT_FLOAT_EQ(0, pid_->maxIntegratedError());
  ASSERT_FLOAT_EQ(0, pid_->minIntegratedError());

  ASSERT_FLOAT_EQ(0, pid_->maxSaturation());
  ASSERT_FLOAT_EQ(0, pid_->minSaturation());

  pid_->setupFromParameterServer(ros::this_node::getNamespace());

  EXPECT_TRUE(std::isinf(pid_->maxIntegratedError()));
  EXPECT_GT(pid_->maxIntegratedError(), std::numeric_limits<double>::max());
  EXPECT_TRUE(std::isinf(pid_->minIntegratedError()));
  EXPECT_LT(pid_->minIntegratedError(), std::numeric_limits<double>::min());

  EXPECT_TRUE(std::isinf(pid_->maxSaturation()));
  EXPECT_GT(pid_->maxSaturation(), std::numeric_limits<double>::max());
  EXPECT_TRUE(std::isinf(pid_->minSaturation()));
  EXPECT_LT(pid_->minSaturation(), std::numeric_limits<double>::min());
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  ros::init(argc, argv, "pid_controller");
  // ros::AsyncSpinner spinner(1);
  // spinner.start();
  testing::InitGoogleTest(&argc, argv);
  auto ret = RUN_ALL_TESTS();
  // spinner.stop();
  ros::shutdown();
  return ret;
};
