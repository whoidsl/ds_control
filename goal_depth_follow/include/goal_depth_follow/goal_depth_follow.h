/**
 * Copyright 2018 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef DS_CONTROL_GOAL_DEPTH_FOLLOW_H
#define DS_CONTROL_GOAL_DEPTH_FOLLOW_H

#include <goal_bottom_follow/goal_bottom_follow.h>
#include <ds_sensor_msgs/Ranges3D.h>
#include <ds_control_msgs/DepthGoalEnum.h>
#include <ds_nav_msgs/AggregatedState.h>

namespace ds_control
{
struct GoalDepthFollowPrivate;

/// Simple depth following node
///
/// GoalDepthFollow provides a reference for following a constant depth
/// with a few basic safeguards.  It is intended to be used at depths high
/// enough above the seafloor that risk of impacting the bottom is minimal
/// (in practice, at altitudes > 100m minimum). Altitude information, if available,
/// is still used to prevent the vehicle from driving into the seafloor, but not
/// to the concervative degree that GoalBottomFollow1D asserts.
/// An external alarm input can cooerce the reference to produce setpoints
/// to drive the vehicle shallower to avoid objects in the flight path.
///
/// This node is essentially a stripped down and simplified version of
/// GoalBottomFollow1D with the following major changes:
///
/// - No attempts are made to determine whether the received altitude values
///   are legit beyond comparing against a known "invalid" value (typ. -3).
///   There is no running history of altitudes to help decipher if the vehicle
///   is too high or too low for the DVL to return valid ranges.
///
/// - No attempts are made to derive if the vehicle is "stuck" based on altitude
///   values.
///
/// # PARAMETER SUBSCRIPTIONS
///
/// GoalDepthFollow subscribes to a set of parameters within it's private namespace:
///
/// - ~depth :: desired depth
/// - ~minimum_altitude :: Minimum allowed altitude.
/// - ~speed :: desired speed
/// - ~minimum_speed :: minimum allowed speed
/// - ~speed_gain :: parameter used for reference calculation
/// - ~envelope :: depth range centered around desired depth
/// - ~depth_rate :: parameter used for reference calculation
/// - ~depth_acceleration :: parameter used for reference calculation
///
///
class GoalDepthFollow : public GoalBottomFollow
{
  DS_DECLARE_PRIVATE(GoalDepthFollow)

public:
  /// Depth Follower Parameter Subscriptions
  typedef struct
  {
    /// Desired depth.   This is DFP index 0
    ds_param::DoubleParam::Ptr depth;
    /// Total envelope width (m), e.g. a 10 m envelope means 5 m above and below the
    /// desired depth.   This is DFP index 1
    ds_param::DoubleParam::Ptr envelope;
    /// Desired speed.   This is DFP index 2
    ds_param::DoubleParam::Ptr speed;
    /// Minimum allowed altitude.  This is DFP index 3
    ds_param::DoubleParam::Ptr speed_gain;
    /// Non-dimensional.  Sets how aggressively speed is backed off when the bottom
    /// rises or falls away more steeply than the vehicle can handle.  The portion
    /// of the total envelope width taken up by speed padding is env/speed_gain.
    /// Values <= 1.0 are invalid. This is DFP index 4
    ds_param::DoubleParam::Ptr depth_rate;
    /// Depth acceleration rate (assumed constant).  This is used along with
    /// depth_rate_d to determine how much padding is required to make up for
    /// the time required by the vehicle to accelerate.  This is DFP index 5
    ds_param::DoubleParam::Ptr depth_acceleration;
    /// Minimum allowed speed.  This is DFP index 6
    ds_param::DoubleParam::Ptr minimum_speed;
    /// Maximum depth rate as used by the trajectory smoother.  For maximum performance
    /// this should be roughly the vehicle's maximum climb rate at the nominal forward
    /// speed.  This is DFP index 7
    ds_param::DoubleParam::Ptr minimum_altitude;
    /// Maximum commanded depth.  This is DFP index 8
    ds_param::DoubleParam::Ptr depth_floor;
  } ParamSubscriptions_t;

  /// @brief Construct a new GoalBottomFollow
  ///
  /// When using this constructor you must call ros::init elsewhere in your code
  GoalDepthFollow();

  /// @brief Construct a new GoalBottomFollow
  ///
  /// This constructor calls ros::init(argc, argv, name) for you
  ///
  /// @param[in] argc
  /// @param[in] argv
  /// @param[in] name The name of the process type
  GoalDepthFollow(int argc, char** argv, const std::string& name);

  /// @brief Destroys a GoalDepthFollow object
  ~GoalDepthFollow() override;

  //
  //  GoalBottomFollow Overrides
  //
  uint64_t type() const noexcept override
  {
    return ds_control_msgs::DepthGoalEnum::CONSTANT_DEPTH;
  }

protected:
  //
  // ds_base::DsProcess overrides
  //
  void setupParameters() override;
  void setupSubscriptions() override;
  void setupPublishers() override;

  // Called with a new ranges message from altitude sources
  void handleRangeUpdate(ds_sensor_msgs::Ranges3D in);

private:
  std::unique_ptr<GoalDepthFollowPrivate> d_ptr_;
};
}  // namespace ds_control

#endif  // DEFINE_GOAL_DEPTH_FOLLOW_H
