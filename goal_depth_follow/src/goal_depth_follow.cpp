/**
 * Copyright 2018 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "goal_depth_follow/goal_depth_follow.h"
#include "goal_depth_follow_private.hpp"
#include <ds_param/ds_param.h>

namespace ds_control
{
GoalDepthFollow::GoalDepthFollow() : GoalBottomFollow()
{
  d_ptr_ = std::unique_ptr<GoalDepthFollowPrivate>(new GoalDepthFollowPrivate(this));
}

GoalDepthFollow::GoalDepthFollow(int argc, char** argv, const std::string& name)
  : GoalBottomFollow(argc, argv, name)
  , d_ptr_(std::unique_ptr<GoalDepthFollowPrivate>(new GoalDepthFollowPrivate(this)))

{
}

GoalDepthFollow::~GoalDepthFollow() = default;

void GoalDepthFollow::setupParameters(void)
{
  GoalBottomFollow::setupParameters();
  DS_D(GoalDepthFollow);
  auto nh = nodeHandle();
  d->setupParameters(nh);
  setRangeCallback(boost::bind(&GoalDepthFollow::handleRangeUpdate, this, _1));
}

void GoalDepthFollow::setupPublishers(void)
{
  GoalBottomFollow::setupPublishers();
  DS_D(GoalDepthFollow);
  auto nh = nodeHandle();
  d->setupPublishers(nh);
}

void GoalDepthFollow::setupSubscriptions(void)
{
  GoalBottomFollow::setupSubscriptions();
  auto nh = nodeHandle();

  DS_D(GoalDepthFollow);
  d->setupSubscriptions(nh);
}

void GoalDepthFollow::handleRangeUpdate(ds_sensor_msgs::Ranges3D in)
{
  DS_D(GoalDepthFollow);
  d->handleAltitude(in);
}

}  // namespace ds_control
