/**
 * Copyright 2018 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "ds_param/ds_param.h"
#include "ds_param/ds_param_conn.h"
#include "goal_bottom_follow_depth/goal_bottom_follow_depth.h"
#include <gtest/gtest.h>

ds_sensor_msgs::Ranges3D gen_ranges(double altitude)
{
  ds_sensor_msgs::Ranges3D dvl_ranges;
  dvl_ranges.ranges.resize(4);
  for (int i = 0; i < 4; ++i)
  {
    dvl_ranges.ranges[i].range.header.frame_id = "rdi300_link";
    dvl_ranges.ranges[i].range.point.x = 0.0;
    dvl_ranges.ranges[i].range.point.y = 0.0;
    dvl_ranges.ranges[i].range.point.z = -altitude;
    dvl_ranges.ranges[i].range_validity = ds_sensor_msgs::Range3D::RANGE_VALID;
  }
  return dvl_ranges;
}

void run_one(ds_control::GoalBottomFollowDepth& in)
{
  for (int i = 0; i < 10; ++i)
  {
    usleep(100);
    in.asio()->io_service.reset();
    boost::system::error_code ec;
    int n = in.asio()->io_service.run_one(ec);
    ROS_INFO_STREAM("Handlers executed: " << n << " Error code: " << ec);
    if (n > 0)
      return;
  }
}

class ParameterClient
{
public:
  ds_control::prm_t m_prm;
  ds_param::ParamConnection::Ptr m_conn;

  ParameterClient()
  {
    ros::NodeHandle nh("/parameter_client");
    m_conn = ds_param::ParamConnection::create(nh);

    m_prm.alt_d = m_conn->connect<ds_param::DoubleParam>("/goal_bottom_follow_depth/altitude");
    m_prm.env = m_conn->connect<ds_param::DoubleParam>("/goal_bottom_follow_depth/envelope");
    m_prm.speed_d = m_conn->connect<ds_param::DoubleParam>("/goal_bottom_follow_depth/speed");
    m_prm.speed_gain = m_conn->connect<ds_param::DoubleParam>("/goal_bottom_follow_depth/speed_gain");
    m_prm.depth_rate_d = m_conn->connect<ds_param::DoubleParam>("/goal_bottom_follow_depth/depth_rate");
    m_prm.depth_accel_d = m_conn->connect<ds_param::DoubleParam>("/goal_bottom_follow_depth/depth_acceleration");
    m_prm.speed_min = m_conn->connect<ds_param::DoubleParam>("/goal_bottom_follow_depth/speed_minimum");
    m_prm.alarm_timeout = m_conn->connect<ds_param::DoubleParam>("/goal_bottom_follow_depth/alarm_timeout");
    m_prm.depth_floor = m_conn->connect<ds_param::DoubleParam>("/goal_bottom_follow_depth/depth_floor");
    m_prm.alt_max_range = m_conn->connect<ds_param::DoubleParam>("/goal_bottom_follow_depth/alt_max_range");
    m_prm.alt_bad_max_range = m_conn->connect<ds_param::DoubleParam>("/goal_bottom_follow_depth/alt_bad_max_range");
    m_prm.alt_min_range = m_conn->connect<ds_param::DoubleParam>("/goal_bottom_follow_depth/alt_min_range");
    m_prm.alt_bad_min_range = m_conn->connect<ds_param::DoubleParam>("/goal_bottom_follow_depth/alt_bad_min_range");
    m_prm.alt_bad_indeterminant = m_conn->connect<ds_param::DoubleParam>("/goal_bottom_follow_depth/"
                                                                         "alt_bad_indeterminant");
    m_prm.alt_bad_timeout = m_conn->connect<ds_param::DoubleParam>("/goal_bottom_follow_depth/alt_bad_timeout");
    m_prm.speed_outside = m_conn->connect<ds_param::DoubleParam>("/goal_bottom_follow_depth/speed_outside");
    m_prm.median_tol = m_conn->connect<ds_param::DoubleParam>("/goal_bottom_follow_depth/median_tol");
    m_prm.time_low_depth_rate_limit = m_conn->connect<ds_param::DoubleParam>("/goal_bottom_follow_depth/"
                                                                             "time_low_depth_rate_limit");
    m_prm.depth_rate_alpha = m_conn->connect<ds_param::DoubleParam>("/goal_bottom_follow_depth/depth_rate_alpha");
    m_prm.depth_rate_threshold = m_conn->connect<ds_param::DoubleParam>("/goal_bottom_follow_depth/"
                                                                        "depth_rate_threshold");
    m_prm.alt_fly_down = m_conn->connect<ds_param::DoubleParam>("/goal_bottom_follow_depth/alt_fly_down");
    m_prm.slow_above_env = m_conn->connect<ds_param::BoolParam>("/goal_bottom_follow_depth/slow_above_env");
  }
};

class GoalBottomFollowDepthTest : public ::testing::Test
{
protected:
  void SetUp() override
  {
    ;
  }

  ds_nav_msgs::AggregatedState invalidState() const
  {
    ds_nav_msgs::AggregatedState ret;
    ret.northing.value = 0;
    ret.northing.valid = false;
    ret.easting.value = 0;
    ret.easting.valid = false;
    ret.down.value = 0;
    ret.down.valid = false;
    ret.roll.value = 0;
    ret.roll.valid = false;
    ret.pitch.value = 0;
    ret.pitch.valid = false;
    ret.heading.value = 0;
    ret.heading.valid = false;
    ret.surge_u.value = 0;
    ret.surge_u.valid = false;
    ret.sway_v.value = 0;
    ret.sway_v.valid = false;
    ret.heave_w.value = 0;
    ret.heave_w.valid = false;
    ret.r.value = 0;
    ret.r.valid = false;
    ret.p.value = 0;
    ret.p.valid = false;
    ret.q.value = 0;
    ret.q.valid = false;

    return ret;
  }
};

TEST(GoalBottomFollowDepthTest, computeSpeedPadding)
{
  ds_control::GoalBottomFollowDepth goal;
  goal.setup();
  double env_padding_speed;

  env_padding_speed = goal.compute_speed_padding(10.0, 2.0, 0.0);
  ASSERT_EQ(env_padding_speed, 0.0);

  env_padding_speed = goal.compute_speed_padding(10.0, 2.0, 1.0);
  ASSERT_EQ(env_padding_speed, 2.5);
}

TEST(GoalBottomFollowDepthTest, computeAccelPadding)
{
  ds_control::GoalBottomFollowDepth goal;
  goal.setup();
  double env_padding_accel;

  env_padding_accel = goal.compute_accel_padding(0.0, 0.0);
  ASSERT_EQ(env_padding_accel, 0.0);

  env_padding_accel = goal.compute_accel_padding(2.0, 1.0);
  ASSERT_EQ(env_padding_accel, 2.0);
}

TEST(GoalBottomFollowDepthTest, computeStep)
{
  ds_control::GoalBottomFollowDepth goal;
  goal.setup();
  double step;

  step = goal.compute_step(0.0, 0.0, 0.0);
  ASSERT_EQ(step, 0.0);

  step = goal.compute_step(10.0, 0.0, 0.0);
  ASSERT_EQ(step, 5.0);

  step = goal.compute_step(10.0, 1.0, 2.0);
  ASSERT_EQ(step, 2.0);
}

TEST(GoalBottomFollowDepthTest, computeStepSize)
{
  ds_control::GoalBottomFollowDepth goal;
  goal.setup();
  double step;
  double env_padding_speed;
  double env_padding_accel;

  std::tie(env_padding_speed, env_padding_accel, step) = goal.compute_step_size(0.0, 2.0, 0.0, 0.0);
  ASSERT_EQ(step, 0.0);
  ASSERT_EQ(env_padding_speed, 0.0);
  ASSERT_EQ(env_padding_accel, 0.0);

  std::tie(env_padding_speed, env_padding_accel, step) = goal.compute_step_size(10.0, 2.0, 0.0, 0.0);
  ASSERT_EQ(step, 5.0);
  ASSERT_EQ(env_padding_speed, 0.0);
  ASSERT_EQ(env_padding_accel, 0.0);

  std::tie(env_padding_speed, env_padding_accel, step) = goal.compute_step_size(10.0, 2.0, 1.0, 1.0);
  ASSERT_EQ(step, 2.0);
  ASSERT_EQ(env_padding_speed, 2.5);
  ASSERT_EQ(env_padding_accel, 0.5);
}

TEST(GoalBottomFollowDepthTest, computeBottomDepth)
{
  ds_control::GoalBottomFollowDepth goal;
  goal.setup();

  ds_control_msgs::BottomFollow1D bf;
  double depth_floor_bot;
  bool good_altitude;

  std::tie(depth_floor_bot, good_altitude) = goal.computeBottomDepth(ros::Time::now(), 0.0, 3000.0);
  EXPECT_EQ(depth_floor_bot, 6045);
  EXPECT_EQ(goal.getBf().depth_bot, 3000);

  bf.pseudo_bot_code = (1 << ds_control::BOTTOM_FOLLOW_PSEUDO_BOT_CODE_ALT_BAD_MIN_RANGE);
  goal.setBf(bf);
  std::tie(depth_floor_bot, good_altitude) = goal.computeBottomDepth(ros::Time::now(), 0.0, 3000.0);
  EXPECT_EQ(depth_floor_bot, 6045);
  EXPECT_EQ(goal.getBf().depth_bot, 3000);
}

TEST(GoalBottomFollowDepthTest, computeDepthRateFiltered)
{
  ds_control::GoalBottomFollowDepth goal;
  goal.setup();

  double depth_rate_filtered = 0.0;
  double heave_w = 0.5;  // Descending at 0.5 m/s
  ros::Time now = ros::Time::now();
  depth_rate_filtered = goal.computeDepthRateFiltered(depth_rate_filtered, heave_w, now);
  EXPECT_NEAR(depth_rate_filtered, 0.025, 0.000000001);
  now = ros::Time::now();
  depth_rate_filtered = goal.computeDepthRateFiltered(depth_rate_filtered, heave_w, now);
  EXPECT_NEAR(depth_rate_filtered, 0.04875, 0.000000001);
  now = ros::Time::now();
  depth_rate_filtered = goal.computeDepthRateFiltered(depth_rate_filtered, heave_w, now);
  EXPECT_NEAR(depth_rate_filtered, 0.0713125, 0.000000001);
  // time_low_depth_rate is actually the last time at which the depth rate filtered was above
  // the threshold specified, so it is the time at which the low depth rate began
  EXPECT_EQ(goal.getBf().time_low_depth_rate, now);
}

TEST(GoalBottomFollowDepthTest, processAltitude)
{
  ds_control::GoalBottomFollowDepth goal;
  goal.setup();
  ds_control_msgs::BottomFollow1D bf;

  ros::Time now;
  now = ros::Time::now();
  goal.add_altitude(50.0, now);
  goal.processAltitude(true, 2000, now, 2050);

  now = ros::Time::now();
  goal.add_altitude(50.0, now);
  goal.processAltitude(true, 2000, now, 2050);

  now = ros::Time::now();
  goal.add_altitude(50.0, now);
  goal.processAltitude(true, 2000, now, 2050);

  now = ros::Time::now();
  goal.add_altitude(50.0, now);
  goal.processAltitude(true, 2000, now, 2050);

  now = ros::Time::now();
  goal.add_altitude(50.0, now);
  goal.processAltitude(true, 2000, now, 2050);

  bf = goal.getBf();
  ASSERT_EQ(bf.median_altitude, 50.0);
}

TEST(GoalBottomFollowDepthTest, computeEnvelope)
{
  ds_control::GoalBottomFollowDepth goal;
  goal.setup();

  double env_upper, env_lower;

  std::tie(env_upper, env_lower) = goal.computeEnvelope(3000);
  EXPECT_EQ(env_upper, 2945);
  EXPECT_EQ(env_lower, 2955);
}

TEST(GoalBottomFollowDepthTest, computeDepthGoal)
{
  ds_control::GoalBottomFollowDepth goal;
  goal.setup();
  ds_control_msgs::BottomFollow1D bf;
  double depth_goal;

  // Compute step size and envelope
  std::tie(bf.env_padding_speed, bf.env_padding_accel, bf.step) = goal.compute_step_size(10.0, 2.0, 1.0, 1.0);
  ASSERT_EQ(bf.step, 2.0);
  ASSERT_EQ(bf.env_padding_speed, 2.5);
  ASSERT_EQ(bf.env_padding_accel, 0.5);
  std::tie(bf.depth_env[0], bf.depth_env[1]) = goal.computeEnvelope(3000);
  EXPECT_EQ(bf.depth_env[0], 2945);
  EXPECT_EQ(bf.depth_env[1], 2955);
  goal.setBf(bf);

  // Too high
  bf = goal.getBf();
  bf.depth_goal = 0;
  goal.setBf(bf);
  depth_goal = goal.computeDepthGoal();
  ASSERT_EQ(depth_goal, 2.0);

  // Too low
  bf = goal.getBf();
  bf.depth_goal = 3000;
  goal.setBf(bf);
  depth_goal = goal.computeDepthGoal();
  ASSERT_EQ(depth_goal, 2998);

  // In the middle of the envelope
  bf = goal.getBf();
  bf.depth_goal = 2950;
  goal.setBf(bf);
  depth_goal = goal.computeDepthGoal();
  ASSERT_EQ(depth_goal, 2950);

  // Check the alarm behavior
  bf = goal.getBf();
  bf.alarm = true;
  bf.depth_goal = 3000;
  goal.setBf(bf);
  depth_goal = goal.computeDepthGoal();
  ASSERT_EQ(depth_goal, 2950);
}

TEST(GoalBottomFollowDepthTest, computeSpeedGoal)
{
  ds_control::GoalBottomFollowDepth goal;
  goal.setup();
  ds_control_msgs::BottomFollow1D bf;
  double depth;
  double ref_speed;
  bool alarm;

  // Compute step size and envelope
  std::tie(bf.env_padding_speed, bf.env_padding_accel, bf.step) = goal.compute_step_size(10.0, 2.0, 1.0, 1.0);
  ASSERT_EQ(bf.step, 2.0);
  ASSERT_EQ(bf.env_padding_speed, 2.5);
  ASSERT_EQ(bf.env_padding_accel, 0.5);
  std::tie(bf.depth_env[0], bf.depth_env[1]) = goal.computeEnvelope(3000);
  EXPECT_EQ(bf.depth_env[0], 2945);
  EXPECT_EQ(bf.depth_env[1], 2955);
  goal.setBf(bf);

  // In the middle of the envelope for 65 seconds, alarm should be false
  bf = goal.getBf();
  depth = 2950;
  goal.setBf(bf);
  std::tie(ref_speed, alarm) = goal.computeSpeedGoal(depth, ros::Duration(65));
  ASSERT_EQ(ref_speed, 1.0);
  ASSERT_EQ(alarm, false);

  // Outside the envelope (under the envelope), should be speed_outside. Alarm is true
  bf = goal.getBf();
  depth = 3000;
  goal.setBf(bf);
  std::tie(ref_speed, alarm) = goal.computeSpeedGoal(depth, ros::Duration(0.1));
  ASSERT_EQ(ref_speed, 0.2);
  ASSERT_EQ(alarm, true);

  // In the middle of the envelope but stuck. ref speed should be zero
  bf = goal.getBf();
  depth = 2950;
  bf.stuck = true;
  goal.setBf(bf);
  std::tie(ref_speed, alarm) = goal.computeSpeedGoal(depth, ros::Duration(65));
  ASSERT_EQ(ref_speed, 0.0);
}

TEST(GoalBottomFollowDepthTest, constantDepthSpeed)
{
  geometry_msgs::TransformStamped msg;
  ds_control::GoalBottomFollowDepth goal;
  goal.setup();

  // Create a publisher that can publish messages
  auto nh = goal.nodeHandle();
  ros::Publisher pub = nh.advertise<ds_sensor_msgs::Ranges3D>("/ranges", 1, false);

  // Verify that we have one subscriber in the goal class for this topic
  EXPECT_EQ(pub.getNumSubscribers(), 1U);

  // Set base_link the same as rdi300_link for easy introspection, with identitiy rotation
  msg.header.stamp = ros::Time(0);
  msg.header.frame_id = "base_link";
  msg.child_frame_id = "rdi300_link";
  msg.transform.rotation = tf2::toMsg(tf2::Quaternion::getIdentity());
  goal.setTransform(msg);

  pub.publish(gen_ranges(5));

  ROS_INFO_STREAM("Publishing dvl ranges");

  // Give DsProcess a chance to process it and return
  run_one(goal);
}

// Setting bottom follower parameters to value that create a negative
// step size should be rejected
TEST(GoalBottomFollowDepthTest, rejectParamChangeCausingInvalidStepsize)
{
  ROS_DEBUG_STREAM("Starting reject test");
  ds_control::GoalBottomFollowDepth goal;
  goal.setup();

  ParameterClient param_client;

  // This combination of values caused a negative step size to go unchecked during sentry665
  // when the envelope for do_MB_high was set to 0.5.  The rollback feature for the param
  // subscriptions didn't rollback the bad envelope setting.

  // These are the relavent BF parameters for MB_high as run on sentry 665:
  const auto envelope = 0.5;
  const auto speed_gain = 4.0;
  const auto depth_rate = 0.33;
  const auto depth_acceleration = 0.07;

  // these SHOULD be declared static, but they're not.  Here we just make sure the values we're going to
  // feed will in fact induce a negative step size.  This is just to make sure that this combination of
  // values will indeed produce a negative step
  const auto speed_padding = goal.compute_speed_padding(envelope, speed_gain, depth_rate);
  const auto acceleration_padding = goal.compute_accel_padding(depth_rate, depth_acceleration);
  const auto step = goal.compute_step(envelope, speed_padding, acceleration_padding);
  ASSERT_TRUE(step < 0);

  // Set the parameters except for the envelope
  param_client.m_prm.speed_gain->Set(speed_gain);
  param_client.m_prm.depth_rate_d->Set(depth_rate);
  param_client.m_prm.depth_accel_d->Set(depth_acceleration);
  run_one(goal);
  ros::spinOnce();

  // Make sure the node got them
  const auto bf_init = goal.getBf();
  ASSERT_DOUBLE_EQ(bf_init.speed_gain, speed_gain);
  ASSERT_DOUBLE_EQ(bf_init.depth_rate_d, depth_rate);
  ASSERT_DOUBLE_EQ(bf_init.depth_accel_d, depth_acceleration);
  ASSERT_TRUE(bf_init.step > 0);

  // Try setting the envelope to 0.5.  This would create a negative step size, and the envelope value
  // SHOULD be rejected
  const auto orig_envelope = param_client.m_prm.env->Get();
  param_client.m_prm.env->Set(envelope);
  run_one(goal);
  ros::spinOnce();

  // Envelope should have returned to the original value
  const auto new_envelope = param_client.m_prm.env->Get();
  EXPECT_DOUBLE_EQ(new_envelope, orig_envelope);

  // Step size is > 0 as required.
  const auto bf_after = goal.getBf();
  EXPECT_TRUE(bf_after.step > 0);
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  ros::init(argc, argv, "goal_bottom_follow_depth");
  testing::InitGoogleTest(&argc, argv);
  auto ret = RUN_ALL_TESTS();
  ros::shutdown();
  return ret;
};
