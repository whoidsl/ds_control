/**
 * Copyright 2018 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "goal_depth_follow_private.hpp"
#include <ros/time.h>

#include <gtest/gtest.h>
using namespace ds_control;

TEST(goal_handle_altitude, add_invalid_range)
{
  const auto timestamp = ros::Time::now();
  auto range3d = ds_sensor_msgs::Ranges3D{};
  const auto z_value = -50;
  range3d.ranges.resize(4);
  std::for_each(std::begin(range3d.ranges), std::end(range3d.ranges), [](ds_sensor_msgs::Range3D& it) {
    it.range_validity = ds_sensor_msgs::Range3D::RANGE_INDETERMINANT;
    it.range.point.z = z_value;
  });
  range3d.header.stamp = timestamp;
  range3d.ds_header.io_time = timestamp;

  auto goal = GoalDepthFollowPrivate{ nullptr };
  const auto prev_altitude = goal.altitude_;

  goal.handleAltitude(range3d);

  // Invalid range should not update current altitude
  EXPECT_EQ(prev_altitude, goal.altitude_);
}

TEST(goal_handle_altitude, add_valid_range)
{
  const auto timestamp = ros::Time::now();
  auto range3d = ds_sensor_msgs::Ranges3D{};
  range3d.ranges.resize(4);
  const auto z_value = -50;
  std::for_each(std::begin(range3d.ranges), std::end(range3d.ranges), [](ds_sensor_msgs::Range3D& it) {
    it.range_validity = ds_sensor_msgs::Range3D::RANGE_VALID;
    it.range.point.z = z_value;
  });
  range3d.header.stamp = timestamp;
  range3d.ds_header.io_time = timestamp;

  auto goal = GoalDepthFollowPrivate{ nullptr };
  const auto prev_altitude = goal.altitude_;

  goal.handleAltitude(range3d);

  EXPECT_NE(prev_altitude, goal.altitude_);
  EXPECT_EQ(goal.altitude_.first, timestamp);
  EXPECT_EQ(goal.altitude_.second, -z_value);
}

// a single valid beam will count as an altitude
TEST(goal_handle_altitude, add_valid_range_from_single_beam)
{
  const auto timestamp = ros::Time::now();
  auto range3d = ds_sensor_msgs::Ranges3D{};
  range3d.ranges.resize(4);
  const auto z_value = -50;
  range3d.ranges[0].range_validity = ds_sensor_msgs::Range3D::RANGE_VALID;
  range3d.ranges[0].range.point.z = z_value;
  std::for_each(std::next(std::begin(range3d.ranges), 1), std::end(range3d.ranges), [](ds_sensor_msgs::Range3D& it) {
    it.range_validity = ds_sensor_msgs::Range3D::RANGE_INDETERMINANT;
    it.range.point.z = z_value;
  });
  range3d.header.stamp = timestamp;
  range3d.ds_header.io_time = timestamp;

  auto goal = GoalDepthFollowPrivate{ nullptr };
  const auto prev_altitude = goal.altitude_;

  goal.handleAltitude(range3d);

  EXPECT_NE(prev_altitude, goal.altitude_);
  EXPECT_EQ(goal.altitude_.first, timestamp);
  EXPECT_EQ(goal.altitude_.second, -z_value);
}

// Add multiple valid ranges
TEST(goal_handle_altitude, add_valid_ranges)
{
  const auto t = ros::Time::now();
  const auto data = std::array<std::pair<ros::Time, double>, 6>{ std::make_pair(t, 40),
                                                                 std::make_pair(t + ros::Duration(1), 45),
                                                                 std::make_pair(t + ros::Duration(2), 80),
                                                                 std::make_pair(t + ros::Duration(3), 90),
                                                                 std::make_pair(t + ros::Duration(4), 100),
                                                                 std::make_pair(t + ros::Duration(5), 120) };

  auto goal = GoalDepthFollowPrivate{ nullptr };
  auto avg = 0;
  for (auto i = 0; i < GoalDepthFollowPrivate::NUM_ALTITUDE_VALUES; ++i)
  {
    auto range3d = ds_sensor_msgs::Ranges3D{};
    range3d.ranges.resize(4);
    const auto z_value = -data[i].second;
    std::for_each(std::begin(range3d.ranges), std::end(range3d.ranges), [&](ds_sensor_msgs::Range3D& it) {
      it.range_validity = ds_sensor_msgs::Range3D::RANGE_VALID;
      it.range.point.z = z_value;
    });
    range3d.header.stamp = data[i].first;
    range3d.ds_header.io_time = data[i].first;

    const auto prev_altitude = goal.altitude_;

    goal.handleAltitude(range3d);

    EXPECT_NE(prev_altitude, goal.altitude_);
    avg += data[i].second;
  }

  // After filling up the altitude array our expected altitude is just the average of the first terms
  const auto expected_altitude = avg / GoalDepthFollowPrivate::NUM_ALTITUDE_VALUES;
  const auto expected_time = data[GoalDepthFollowPrivate::NUM_ALTITUDE_VALUES - 1].first;
  EXPECT_EQ(goal.altitude_.first, expected_time);
  EXPECT_EQ(goal.altitude_.second, expected_altitude);
  auto prev_it = std::begin(goal.prev_altitudes_);

  for (auto i = 0; i < GoalDepthFollowPrivate::NUM_ALTITUDE_VALUES; i++)
  {
    EXPECT_EQ(*prev_it, data[i]) << "mismatch in goal altitude history for index " << i;
    ++prev_it;
  }

  // Now add another term
  auto range3d = ds_sensor_msgs::Ranges3D{};
  range3d.ranges.resize(4);
  const auto z_value = -data[GoalDepthFollowPrivate::NUM_ALTITUDE_VALUES].second;
  std::for_each(std::begin(range3d.ranges), std::end(range3d.ranges), [&](ds_sensor_msgs::Range3D& it) {
    it.range_validity = ds_sensor_msgs::Range3D::RANGE_VALID;
    it.range.point.z = z_value;
  });
  range3d.header.stamp = data[GoalDepthFollowPrivate::NUM_ALTITUDE_VALUES].first;
  range3d.ds_header.io_time = data[GoalDepthFollowPrivate::NUM_ALTITUDE_VALUES].first;
  goal.handleAltitude(range3d);

  // Timestamp should be the same as the last data entered
  EXPECT_EQ(goal.altitude_.first, data[GoalDepthFollowPrivate::NUM_ALTITUDE_VALUES].first);

  // altitude value returned by current_alititude_ should be the average of values 1-NUM_ALTITUDE_VALUES
  avg = 0;
  for (auto i = 1; i < data.size(); ++i)
  {
    avg += data[i].second / GoalDepthFollowPrivate::NUM_ALTITUDE_VALUES;
  }
  EXPECT_EQ(goal.altitude_.second, avg);

  // Last entry in altitudes_ should be the last entry in the data
  EXPECT_EQ(goal.prev_altitudes_.back(), data[GoalDepthFollowPrivate::NUM_ALTITUDE_VALUES]);
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  ros::Time::init();
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
};
