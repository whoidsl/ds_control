/**
 * Copyright 2018 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <ds_base/ds_global.h>
#include <ds_param/ds_param.h>
#include <ds_param/ds_param_conn.h>
#include <ds_nav_msgs/FlaggedDouble.h>
#include <ds_nav_msgs/AggregatedState.h>
#include <ds_control_msgs/DepthFollow.h>
#include "goal_depth_follow/goal_depth_follow.h"

#include <ros/console.h>

namespace ds_control
{
namespace detail
{
typedef struct
{
  double depth;
  double minimum_altitude;
  double speed;
  double speed_gain;
  double minimum_speed;
  double envelope;
  double depth_rate;
  double depth_acceleration;
  double depth_floor;
  // double active_depth_goal;
  double envelope_padding_speed;
  double envelope_padding_acceleration;
  double envelope_step;
} Param_t;

// The depth goal is:
//
// - if there's no altitude data: just the depth given in param_depth_
// - if there's valid altitude, then the depth goal is either the depth in param_depth_ or
//   the depth 'param_minimum_altitude_' above the measured seafloor depth, whichever is shallower
auto calculate_depth_goal_with_altitude(double desired_depth, double current_depth, double current_altitude,
                                        double minimum_altitude) -> double
{
  // No altitude? Just use the depth goal
  if (current_altitude < 0)
  {
    return desired_depth;
  }

  // Estimate of the seafloor depth given current nav state and measured altitude
  const auto seafloor_depth = current_depth + current_altitude;

  // Maximum depth allowed when we have valid ranges to the bottom is the seafloor depth minus minimum altitude
  const auto maximum_depth = seafloor_depth - minimum_altitude;
  return std::min(desired_depth, maximum_depth);
}

auto calculate_depth_step(double depth_goal, double current_depth, double depth_envelope, double maximum_depth_step,
                          double depth_floor) -> double
{
  const auto goal = [&] {
    if (depth_goal < current_depth - depth_envelope / 2.0)
    {
      return current_depth - maximum_depth_step;
    }
    else if (depth_goal > current_depth + depth_envelope / 2.0)
    {
      return current_depth + maximum_depth_step;
    }
    else
    {
      return depth_goal;
    }
  }();
  return std::min(goal, depth_floor);
}

auto calculate_speed_goal(double depth_goal, double current_depth, double desired_speed, double minimum_speed,
                          double speed_gain, double envelope, double envelope_padding_speed) -> double
{
  const auto error = std::fabs(current_depth - depth_goal);
  if (error <= envelope / 2.0 + envelope_padding_speed)
  {
    return desired_speed;
  }
  const auto alpha = 2 * speed_gain / envelope * (desired_speed - minimum_speed);
  const auto interpolated_speed = desired_speed - alpha * error;
  return std::max(interpolated_speed, minimum_speed);
}

inline auto calculate_envelope_padding_speed(double envelope, double speed_gain) -> double
{
  return envelope / (2 * speed_gain);
}

inline auto calculate_envelope_padding_acceleration(double depth_rate, double depth_acceleration) -> double
{
  if (depth_acceleration != 0)
  {
    return 0.5 * std::pow(depth_rate, 2) / depth_acceleration;
  }
  else
  {
    return 0;
  }
}

inline auto calculate_envelope_step(double envelope, double padding_speed, double padding_acceleration) -> double
{
  return envelope / 2.0 - padding_speed - padding_acceleration;
}

}  // namespace detail

struct GoalDepthFollowPrivate
{
  DS_DECLARE_PUBLIC(GoalDepthFollow);

  GoalDepthFollow* q_ptr_;

  ds_param::ParamConnection::Ptr param_connection_;
  ds_param::IntParam::Ptr active_depth_goal_;
  GoalDepthFollow::ParamSubscriptions_t param_subscriptions_;
  detail::Param_t param_;

  bool is_enabled_ = false;

  /// Number of altitude values to keep
  static constexpr auto NUM_ALTITUDE_VALUES = static_cast<size_t>(ds_control_msgs::DepthFollow::NUM_ALTITUDE_VALUES);
  /// Maximum age of altitude values to consider
  static constexpr auto MAX_ALTITUDE_AGE_SECONDS = 5;
  /// Invalid altitude value sentinal
  static constexpr auto INVALID_ALTITUDE_VALUE = double{ -3 };

  std::list<std::pair<ros::Time, double>> prev_altitudes_;
  std::pair<ros::Time, double> altitude_ = {};

  // Reference goal output
  ds_nav_msgs::AggregatedState goal_;

  // Internal state for logging
  ds_control_msgs::DepthFollow state_ = {};
  ros::Publisher state_pub_;

  // Last received nav state
  ros::Subscriber nav_state_sub_;
  ds_nav_msgs::AggregatedState last_nav_state_;

  GoalDepthFollowPrivate(GoalDepthFollow* public_) : q_ptr_(public_)
  {
    goal_.northing.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
    goal_.easting.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
    goal_.down.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
    goal_.heading.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
    goal_.pitch.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
    goal_.roll.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
    goal_.surge_u.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
    goal_.sway_v.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
    goal_.heave_w.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
    goal_.p.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
    goal_.q.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
    goal_.r.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
    prev_altitudes_.resize(NUM_ALTITUDE_VALUES);
    ROS_ASSERT_MSG(NUM_ALTITUDE_VALUES == state_.previous_altitude_stamps.size(), "size mismatch between internal "
                                                                                  "altitude history size and size "
                                                                                  "defined in DepthFollow.msg");
    ROS_ASSERT_MSG(NUM_ALTITUDE_VALUES == state_.previous_altitudes.size(), "size mismatch between internal altitude "
                                                                            "history size and size defined in "
                                                                            "DepthFollow.msg");
  };

  ~GoalDepthFollowPrivate() = default;

  void setupParameters(ros::NodeHandle& nh)
  {
    const auto path = ros::this_node::getName();
    param_connection_ = ds_param::ParamConnection::create(nh);

    // Depth follower parameters
    param_subscriptions_.depth = param_connection_->connect<ds_param::DoubleParam>(path + "/depth");
    param_subscriptions_.minimum_altitude = param_connection_->connect<ds_param::DoubleParam>(path + "/minimum_"
                                                                                                     "altitude");
    param_subscriptions_.speed = param_connection_->connect<ds_param::DoubleParam>(path + "/speed");
    param_subscriptions_.speed_gain = param_connection_->connect<ds_param::DoubleParam>(path + "/speed_gain");

    param_subscriptions_.minimum_speed = param_connection_->connect<ds_param::DoubleParam>(path + "/minimum_"
                                                                                                  "speed");
    param_subscriptions_.envelope = param_connection_->connect<ds_param::DoubleParam>(path + "/envelope");
    param_subscriptions_.depth_rate = param_connection_->connect<ds_param::DoubleParam>(path + "/depth_rate");
    param_subscriptions_.depth_acceleration = param_connection_->connect<ds_param::DoubleParam>(path + "/depth_"
                                                                                                       "accelerat"
                                                                                                       "ion");
    param_subscriptions_.depth_floor = param_connection_->connect<ds_param::DoubleParam>(path + "/depth_floor");

    // Active depth goal subscription
    active_depth_goal_ = param_connection_->connect<ds_param::IntParam>("active_depth_"
                                                                        "goal");
    const auto params_valid = updateAndVerifyParameters(param_);
    ROS_ASSERT_MSG(params_valid, "DepthFollow node initiated with invalid set of parameters.  Check logs and correct.");
    param_connection_->setCallback(boost::bind(&GoalDepthFollowPrivate::handleParameterUpdate, this, _1));
  }

  void setupSubscriptions(ros::NodeHandle& nh)
  {
    // Nav state subscription
    const auto state_topic = ros::param::param<std::string>("~aggregated_state_topic", "/state");
    nav_state_sub_ = nh.subscribe<ds_nav_msgs::AggregatedState>(
        state_topic, 1, boost::bind(&GoalDepthFollowPrivate::updateGoal, this, _1));
  }

  void setupPublishers(ros::NodeHandle& nh)
  {
    auto path = ros::this_node::getName();
    state_pub_ = nh.advertise<ds_control_msgs::DepthFollow>(path + "/state", 10, false);
  }

  [[nodiscard]] auto updateAndVerifyParameters(detail::Param_t& param) -> bool
  {
    auto valid = true;
    ROS_INFO_STREAM("updating depth follower parameters....");
    param.depth = param_subscriptions_.depth->Get();
    ROS_INFO_STREAM("   depth: " << param.depth);

    param.minimum_altitude = param_subscriptions_.minimum_altitude->Get();
    ROS_INFO_STREAM("   minimum_altitude: " << param.minimum_altitude);

    param.speed = param_subscriptions_.speed->Get();
    ROS_INFO_STREAM("   speed: " << param.speed);

    param.minimum_speed = param_subscriptions_.minimum_speed->Get();
    ROS_INFO_STREAM("   minimum_speed: " << param.minimum_speed);

    param.speed_gain = param_subscriptions_.speed_gain->Get();
    ROS_INFO_STREAM("   speed_gain: " << param.speed_gain);

    param.envelope = param_subscriptions_.envelope->Get();
    ROS_INFO_STREAM("   envelope: " << param.envelope);

    param.depth_rate = param_subscriptions_.depth_rate->Get();
    ROS_INFO_STREAM("   depth_rate: " << param.depth_rate);

    param.depth_acceleration = param_subscriptions_.depth_acceleration->Get();
    ROS_INFO_STREAM("   depth_acceleration: " << param.depth_acceleration);

    param.depth_floor = param_subscriptions_.depth_floor->Get();
    ROS_INFO_STREAM("   depth_floor: " << param.depth_floor);

    ROS_INFO_STREAM("newly calculated aux parameters....");
    param.envelope_padding_speed = detail::calculate_envelope_padding_speed(param.envelope, param.speed_gain);
    ROS_INFO_STREAM("   envelope_padding_speed: " << param.envelope_padding_speed);

    param.envelope_padding_acceleration =
        detail::calculate_envelope_padding_acceleration(param.depth_rate, param.depth_acceleration);
    ROS_INFO_STREAM("   envelope_padding_acceleration: " << param.envelope_padding_acceleration);

    param.envelope_step = detail::calculate_envelope_step(param.envelope, param.envelope_padding_speed,
                                                          param.envelope_padding_acceleration);
    ROS_INFO_STREAM("   envelope_step: " << param.envelope_step);

    if (param.speed_gain < 1.0)
    {
      ROS_ERROR_STREAM("speed gain of " << param.speed_gain << " is invalid.  speed gain must be >= 1.0");
      valid = false;
    }

    if (param.envelope_step < 0.0)
    {
      ROS_ERROR_STREAM("Current parameters generate a negative depth step size.  Check values for envelope, speed "
                       "gain, depth rate, and depth acceleration");
      valid = false;
    }
    return valid;
  }

  void setParameterValues(const detail::Param_t& param)
  {
    ds_param::ParamGuard lock(param_connection_);

    ROS_INFO_STREAM("setting depth follower parameters on server...");
    ROS_INFO_STREAM("   depth: " << param.depth);
    param_subscriptions_.depth->Set(param.depth);

    ROS_INFO_STREAM("   minimum_altitude: " << param.minimum_altitude);
    param_subscriptions_.minimum_altitude->Set(param.minimum_altitude);

    ROS_INFO_STREAM("   speed: " << param.speed);
    param_subscriptions_.speed->Set(param.speed);

    ROS_INFO_STREAM("   minimum_speed: " << param.minimum_speed);
    param_subscriptions_.minimum_speed->Set(param.minimum_speed);

    ROS_INFO_STREAM("   speed_gain: " << param.speed_gain);
    param_subscriptions_.speed_gain->Set(param.speed_gain);

    ROS_INFO_STREAM("   envelope: " << param.envelope);
    param_subscriptions_.envelope->Set(param.envelope);

    ROS_INFO_STREAM("   depth_rate: " << param.depth_rate);
    param_subscriptions_.depth_rate->Set(param.depth_rate);

    ROS_INFO_STREAM("   depth_acceleration: " << param.depth_acceleration);
    param_subscriptions_.depth_acceleration->Set(param.depth_acceleration);

    ROS_INFO_STREAM("   depth_floor: " << param.depth_floor);
    param_subscriptions_.depth_floor->Set(param.depth_floor);
  }

  // React to parameter subscription changes
  void handleParameterUpdate(const ds_param::ParamConnection::ParamCollection& params)
  {
    // empty param set, nothing to do
    if (params.empty())
    {
      return;
    }

    for (const auto& it : params)
    {
      const auto type_ = it->TypeNum();
      if (type_ == ds_param::UpdatingParamTypes::PARAM_DOUBLE)
      {
        auto p = static_cast<ds_param::DoubleParam*>(it.get());
        const auto current = p->Get();
        const auto previous = p->GetPrevious();
        if (previous)
        {
          ROS_WARN_STREAM("  " << it->Name() << " from " << *previous << " to " << current);
        }
        else
        {
          ROS_WARN_STREAM("  " << it->Name() << " from <UNSET> to " << current);
        }
      }
      else if (type_ == ds_param::UpdatingParamTypes::PARAM_INT)
      {
        auto p = static_cast<ds_param::IntParam*>(it.get());
        const auto current = p->Get();
        const auto previous = p->GetPrevious();
        if (previous)
        {
          ROS_WARN_STREAM("  " << p->Name() << " from " << std::boolalpha << *previous << " to " << std::boolalpha
                               << current);
        }
        else
        {
          ROS_WARN_STREAM("  " << p->Name() << "from <UNSET> to " << current);
        }
      }
      else
      {
        // At time of fix only DoubleParam and BoolParam are used.  Shouldn't get any other types, but someone may add
        // one in the future.
        ROS_ERROR_STREAM("Unhandled ds_param::UpdatingParamType: " << type_ << " (" << it->Type() << ")");
        ROS_ERROR_STREAM("This is a bug and is probably cause by a new ds_param type added to GoalDepthFollow");
      }
    }

    DS_Q(GoalDepthFollow);
    auto new_parameters = detail::Param_t{};
    const auto valid = updateAndVerifyParameters(new_parameters);
    if (valid)
    {
      param_ = std::move(new_parameters);
    }
    else
    {
      ROS_WARN_STREAM("rolling back depth follower parameters to prevoius values");
      setParameterValues(param_);
    }
    is_enabled_ = active_depth_goal_->Get() == q_ptr_->type();
  }

  auto updateGoal(const ds_nav_msgs::AggregatedState::ConstPtr& nav_state) -> bool
  {
    // don't do anything if we don't have a previous nav state to estimate speed against
    if (last_nav_state_.header.stamp.isZero())
    {
      ROS_WARN_STREAM("unable to update goal: no previous nav state");
      last_nav_state_ = *nav_state;
      return false;
    }

    // Check for valid nav data
    if (nav_state->down.valid != ds_nav_msgs::FlaggedDouble::VALUE_VALID)
    {
      ROS_WARN_STREAM("unable to update goal: depth value from nav is invalid");
      last_nav_state_ = *nav_state;
      return false;
    }

    // Starting depth goal takes into account any altitude information we've got
    // and enforces the minimum altitude value.  Otherwise just use the desired depth set point.
    const auto altitude_age = (nav_state->header.stamp - altitude_.first).toSec();

    const auto current_depth_goal =
        altitude_age <= MAX_ALTITUDE_AGE_SECONDS ?
            detail::calculate_depth_goal_with_altitude(param_.depth, nav_state->down.value, altitude_.second,
                                                       param_.minimum_altitude) :
            param_.depth;
    const auto current_altitude =
        altitude_age <= MAX_ALTITUDE_AGE_SECONDS ? altitude_.second : ds_control_msgs::DepthFollow::INVALID_ALTITUDE;

    // If the depth goal is outside our envelope only adjust by the maximum envelope_step_ value
    // Otherwise leave the depth goal unchanged
    const auto next_depth_goal = detail::calculate_depth_step(
        current_depth_goal, nav_state->down.value, param_.envelope, param_.envelope_step, param_.depth_floor);

    // calculate speed goal based on the new depth goal
    const auto next_speed_goal =
        detail::calculate_speed_goal(current_depth_goal, nav_state->down.value, param_.speed, param_.minimum_speed,
                                     param_.speed_gain, param_.envelope, param_.envelope_padding_speed);

    // update state
    state_.desired_depth = param_.depth;
    state_.minimum_altitude = param_.minimum_altitude;
    state_.desired_speed = param_.speed;
    state_.speed_gain = param_.speed_gain;
    state_.minimum_speed = param_.minimum_speed;
    state_.envelope = param_.envelope;
    state_.depth_rate = param_.depth_rate;
    state_.depth_acceleration = param_.depth_acceleration;
    state_.depth_floor = param_.depth_floor;
    state_.envelope_padding_speed = param_.envelope_padding_speed;
    state_.envelope_padding_acceleration = param_.envelope_padding_acceleration;
    state_.envelope_step = param_.envelope_step;
    state_.enabled = is_enabled_;
    auto state_alt_stamp_it = std::begin(state_.previous_altitude_stamps);
    auto state_alt_value_it = std::begin(state_.previous_altitudes);
    for (const auto& alt : prev_altitudes_)
    {
      *state_alt_stamp_it = alt.first;
      *state_alt_value_it = alt.second;
      state_alt_stamp_it++;
      state_alt_value_it++;
    }
    state_.current_altitude = current_altitude;
    state_.current_depth_goal = current_depth_goal;
    state_.next_depth_goal = next_depth_goal;
    state_.next_speed_goal = next_speed_goal;
    state_.header = nav_state->header;
    state_.ds_header = nav_state->ds_header;

    // update goal reference
    goal_.down.value = next_depth_goal;
    goal_.down.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;

    goal_.surge_u.value = next_speed_goal;
    goal_.surge_u.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;

    goal_.header = nav_state->header;
    goal_.ds_header = nav_state->ds_header;

    last_nav_state_ = *nav_state;
    state_pub_.publish(state_);
    DS_Q(GoalDepthFollow);
    if (q)
    {
      q->publishGoal(goal_);
    }
    return true;
  }

  auto handleAltitude(const ds_sensor_msgs::Ranges3D& ranges) -> void
  {
    auto minimum_range = std::numeric_limits<double>::infinity();
    for (const auto& range : ranges.ranges)
    {
      if (range.range_validity != ds_sensor_msgs::Range3D::RANGE_VALID)
      {
        continue;
      }
      const auto beam_range = -range.range.point.z;
      if (beam_range < minimum_range)
      {
        minimum_range = beam_range;
      }
    }

    if (std::isinf(minimum_range))
    {
      ROS_DEBUG_STREAM("no valid ranges in range message");
      return;
    }

    const auto timestamp = ranges.header.stamp;
    prev_altitudes_.pop_front();
    prev_altitudes_.emplace_back(timestamp, minimum_range);

    // Update current altitude value.  We will always have at least 1 valid range
    auto valid_ranges = 0;
    auto mean_range = 0;
    for (const auto& altitude : prev_altitudes_)
    {
      if ((timestamp - altitude.first).toSec() <= MAX_ALTITUDE_AGE_SECONDS)
      {
        valid_ranges += 1;
        mean_range += altitude.second;
      }
    }
    altitude_ = { timestamp, mean_range / valid_ranges };
    ROS_DEBUG_STREAM("updated avg altitude estimate to " << altitude_.second);
  }
};

}  // namespace ds_control
