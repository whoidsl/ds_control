/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef GOAL_LEG_GOAL_LEG_H
#define GOAL_LEG_GOAL_LEG_H

#include "ds_control/goal_base.h"
#include <ds_control_msgs/GoalLegState.h>
#include <boost/optional.hpp>

namespace goal_leg
{
struct GoalLegPrivate;

/// @brief A goal that computes its value based on distance to a specified leg between two X/Y points
///
/// # Parameters
///
/// # TODO-- fill this in!
class GoalLeg : public ds_control::GoalBase
{
  DS_DECLARE_PRIVATE(GoalLeg);

public:
  /// @brief Construct a new Goal Leg
  ///
  /// When using this constructor you must call ros::init elsewhere in your code
  GoalLeg();

  /// @brief Construct a new DsGoalLeg
  ///
  /// This constructor calls ros::init(argc, argv, name) for you
  ///
  /// @param[in] argc
  /// @param[in] argv
  /// @param[in] name The name of the process type
  GoalLeg(int argc, char** argv, const std::string& name);

  /// @brief Destroys a DsGoalJoystick
  ~GoalLeg() override;
  DS_DISABLE_COPY(GoalLeg);
  uint64_t type() const noexcept override
  {
    return 0;
  }

  /// \brief Recompute the goal values based on nav data from the nav aggregator
  ds_nav_msgs::AggregatedState computeGoal(const ds_nav_msgs::AggregatedState& vehicleState);
  ds_nav_msgs::FlaggedDouble computeHeadingGoal(const ds_nav_msgs::AggregatedState& vehicleState);

  /// \brief Callback for when vehicle state messages are received
  void vehicleStateCallback(const ds_nav_msgs::AggregatedState& vehicleState);

  const ds_control_msgs::GoalLegState& getState() const;

protected:
  void setupParameters() override;
  void setupSubscriptions() override;

private:
  std::unique_ptr<GoalLegPrivate> d_ptr_;
};
}
#endif  // GOAL_JOYSTICK_GOAL_JOYSTICK_H
