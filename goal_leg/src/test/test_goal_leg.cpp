/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 2/14/18.
//

#include "../goal_leg/goal_leg_private.h"
#include <gtest/gtest.h>
#include <ros/ros.h>

using namespace goal_leg;

const std::string PREFIX = "/goalleg/";

// little class to expose some protected functionality for testing
class GoalLegShim : public GoalLeg
{
public:
  void setupParams()
  {
    GoalLeg::setupParameters();
  }
};

class GoalLegTest : public ::testing::Test
{
protected:
  void SetUp() override
  {
    goal_leg_ = std::unique_ptr<GoalLegShim>(new GoalLegShim);
  }

  void basicParamSetup()
  {
    ros::param::set(PREFIX + "leg_number", -1);
    ros::param::set(PREFIX + "start_northing", 1.0);
    ros::param::set(PREFIX + "start_easting", 2.0);
    ros::param::set(PREFIX + "start_down", 3.0);

    ros::param::set(PREFIX + "end_northing", 2.0);
    ros::param::set(PREFIX + "end_easting", 4.0);
    ros::param::set(PREFIX + "end_down", 9.0);

    ros::param::set(PREFIX + "kappa", 1.0);
    expected_hdg = atan2(2.0, 1.0);  // DEFINITELY atan2(easting, northing)
  }

  ds_nav_msgs::AggregatedState invalidState() const
  {
    ds_nav_msgs::AggregatedState ret;

    ret.northing.value = 0;
    ret.northing.valid = false;

    ret.easting.value = 0;
    ret.easting.valid = false;

    ret.down.value = 0;
    ret.down.valid = false;

    ret.roll.value = 0;
    ret.roll.valid = false;

    ret.pitch.value = 0;
    ret.pitch.valid = false;

    ret.heading.value = 0;
    ret.heading.valid = false;

    ret.surge_u.value = 0;
    ret.surge_u.valid = false;

    ret.sway_v.value = 0;
    ret.sway_v.valid = false;

    ret.heave_w.value = 0;
    ret.heave_w.valid = false;

    ret.r.value = 0;
    ret.r.valid = false;

    ret.p.value = 0;
    ret.p.valid = false;

    ret.q.value = 0;
    ret.q.valid = false;

    return ret;
  }

  double expected_hdg;
  std::unique_ptr<GoalLegShim> goal_leg_;
};

TEST_F(GoalLegTest, calculateHeadingRef0)
{
  basicParamSetup();
  goal_leg_->setupParams();

  ds_nav_msgs::AggregatedState vehicleState = invalidState();
  vehicleState.northing.value = 1.0;
  vehicleState.northing.valid = true;

  vehicleState.easting.value = 2.0;
  vehicleState.easting.valid = true;

  ds_nav_msgs::FlaggedDouble goal = goal_leg_->computeHeadingGoal(vehicleState);

  EXPECT_TRUE(goal.valid);
  EXPECT_DOUBLE_EQ(expected_hdg, goal.value);
}

TEST_F(GoalLegTest, calculateHeadingRefMinus90)
{
  basicParamSetup();
  goal_leg_->setupParams();

  // this test puts us *just* too far off to one side, so it clamps heading distance to PI/3.0
  double dist = 1.1 * M_PI / 3.0;

  ds_nav_msgs::AggregatedState vehicleState = invalidState();
  vehicleState.northing.value = (-1.0 / sqrt(5.0)) * dist;
  vehicleState.northing.valid = true;

  vehicleState.easting.value = (3.0 / sqrt(5.0)) * dist;
  vehicleState.easting.valid = true;

  ds_nav_msgs::FlaggedDouble goal = goal_leg_->computeHeadingGoal(vehicleState);

  EXPECT_TRUE(goal.valid);
  EXPECT_DOUBLE_EQ(expected_hdg - M_PI / 3.0, goal.value);
}

TEST_F(GoalLegTest, calculateHeadingRefPlus90)
{
  basicParamSetup();
  goal_leg_->setupParams();

  double dist = 1.1 * M_PI / 3.0;

  ds_nav_msgs::AggregatedState vehicleState = invalidState();
  vehicleState.northing.value = (3.0 / sqrt(5.0)) * dist;
  vehicleState.northing.valid = true;

  vehicleState.easting.value = (1.0 / sqrt(5.0)) * dist;
  vehicleState.easting.valid = true;

  ds_nav_msgs::FlaggedDouble goal = goal_leg_->computeHeadingGoal(vehicleState);

  EXPECT_TRUE(goal.valid);
  EXPECT_DOUBLE_EQ(expected_hdg + M_PI / 3.0, goal.value);
}

TEST_F(GoalLegTest, calculateHeadingInvalid)
{
  basicParamSetup();
  goal_leg_->setupParams();

  double dist = 1.1 * M_PI / 3.0;

  ds_nav_msgs::AggregatedState vehicleState = invalidState();
  vehicleState.northing.value = (3.0 / sqrt(5.0)) * dist;
  vehicleState.northing.valid = false;

  vehicleState.easting.value = (1.0 / sqrt(5.0)) * dist;
  vehicleState.easting.valid = false;

  ds_nav_msgs::FlaggedDouble goal = goal_leg_->computeHeadingGoal(vehicleState);

  EXPECT_TRUE(!goal.valid);
  EXPECT_DOUBLE_EQ(expected_hdg, goal.value);
}

TEST_F(GoalLegTest, rovExample)
{
  // Looks like x/y are flipped in the reference generator
  ros::param::set(PREFIX + "start_easting", 7811.479);
  ros::param::set(PREFIX + "start_northing", 2599.903);
  ros::param::set(PREFIX + "start_down", 3.0);

  ros::param::set(PREFIX + "end_easting", 7693.079);
  ros::param::set(PREFIX + "end_northing", 2458.803);
  ros::param::set(PREFIX + "end_down", 9.0);

  ros::param::set(PREFIX + "kappa", 0.03);

  goal_leg_->setupParams();

  ds_nav_msgs::AggregatedState vehicleState = invalidState();
  vehicleState.northing.value = 2556.239;
  vehicleState.northing.valid = true;

  vehicleState.easting.value = 7774.284;
  vehicleState.easting.valid = true;

  ds_nav_msgs::FlaggedDouble goal = goal_leg_->computeHeadingGoal(vehicleState);
  ds_control_msgs::GoalLegState state = goal_leg_->getState();

  EXPECT_TRUE(goal.valid);
  EXPECT_DOUBLE_EQ(state.new_goal, goal.value);
  EXPECT_NEAR(-2.443, state.angle_line_segment, 0.001);
  EXPECT_NEAR(-0.426, state.off_line_vect, 0.001);
  EXPECT_NEAR(0.03, state.kappa, 0.001);
  EXPECT_NEAR(-1.000, state.sign_of_vect, 0.1);
  EXPECT_NEAR(-2.456, state.new_goal, 0.001);
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  ros::init(argc, argv, "goal_leg_test");

  testing::InitGoogleTest(&argc, argv);
  auto ret = RUN_ALL_TESTS();
  ros::shutdown();
  return ret;
}
