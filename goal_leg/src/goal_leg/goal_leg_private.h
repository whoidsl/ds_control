/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef GOAL_LEG_GOAL_LEG_PRIVATE_H
#define GOAL_LEG_GOAL_LEG_PRIVATE_H
#include "goal_leg/goal_leg.h"
#include <ds_param/ds_param.h>
#include <ds_control_msgs/GoalLegState.h>
#include <visualization_msgs/Marker.h>

namespace goal_leg
{
struct GoalLegPrivate
{
  GoalLegPrivate() = default;
  ~GoalLegPrivate() = default;

  ds_param::ParamConnection::Ptr params;

  // Starting point for the line
  ds_param::DoubleParam::Ptr start_northing;
  ds_param::DoubleParam::Ptr start_easting;
  ds_param::DoubleParam::Ptr start_down;

  // Endpoint for the line
  ds_param::DoubleParam::Ptr end_northing;
  ds_param::DoubleParam::Ptr end_easting;
  ds_param::DoubleParam::Ptr end_down;

  // internal state
  ds_control_msgs::GoalLegState state;

  // Gains
  ds_param::DoubleParam::Ptr kappa;

  // Leg number
  ds_param::IntParam::Ptr leg_number;

  // other internal params
  std::string frame_name;
  float color_r, color_g, color_b;

  // ROS subscription stuff
  ros::Subscriber state_update_sub;
  ros::Publisher ref_visualization_pub;
  ros::Publisher leg_state_pub;

  // private functions
  // fill the vehicle marker in, and convert our local x/y/z in NED to ROS-style ENU for visualization
  visualization_msgs::Marker fillMarker(const ds_nav_msgs::AggregatedState& vehicleState)
  {
    // fill from the state message, uses vehicleState for depth
    visualization_msgs::Marker ret;
    ret.header.frame_id = frame_name;
    ret.header.stamp = ros::Time::now();
    ret.ns = "reference";
    ret.type = visualization_msgs::Marker::LINE_STRIP;
    ret.action = visualization_msgs::Marker::ADD;  // also modifies

    // you HAVE to fill in a pose.  Grrr.
    ret.pose.position.x = 0;
    ret.pose.position.y = 0;
    ret.pose.position.z = 0;

    ret.pose.orientation.x = 0;
    ret.pose.orientation.y = 0;
    ret.pose.orientation.z = 0;
    ret.pose.orientation.w = 1;  // identity quaternion

    // scale
    // this marker type uses only the x-component of scale, for line width
    ret.scale.x = 0.05;
    ret.scale.y = 0.0;
    ret.scale.z = 0.0;

    ret.color.r = 1.0;
    ret.color.g = 1.0;
    ret.color.b = 1.0;
    ret.color.a = 1.0;

    ret.lifetime.sec = 0;
    ret.lifetime.nsec = 0;

    ret.frame_locked = true;

    // draw our line
    double z = 0;
    if (vehicleState.down.valid)
    {
      z = -vehicleState.down.value;
    }
    ret.points.resize(2);
    ret.points[0].x = state.line_start.x;
    ret.points[0].y = state.line_start.y;
    ret.points[0].z = z;
    ret.points[1].x = state.line_end.x;
    ret.points[1].y = state.line_end.y;
    ret.points[1].z = z;

    // add some colors
    ret.colors.resize(2);
    ret.colors[0].r = 0.0;
    ret.colors[0].g = 1.0;
    ret.colors[0].b = 0.0;
    ret.colors[0].a = 1.0;

    ret.colors[1].r = 0.0;
    ret.colors[1].g = 0.0;
    ret.colors[1].b = 1.0;
    ret.colors[1].a = 1.0;

    return ret;
  }
};
}
#endif  // GOAL_LEG_GOAL_LEG_PRIVATE_H
