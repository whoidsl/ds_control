/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "goal_leg/goal_leg.h"
#include "goal_leg_private.h"

#include <boost/algorithm/string.hpp>
#include <geometry_msgs/Point.h>
#include <visualization_msgs/Marker.h>
#include <algorithm>
#include <ds_util/reference_smoothing.h>

namespace goal_leg
{
GoalLeg::GoalLeg() : ds_control::GoalBase(), d_ptr_(std::unique_ptr<GoalLegPrivate>(new GoalLegPrivate))
{
}
GoalLeg::GoalLeg(int argc, char** argv, const std::string& name)
  : ds_control::GoalBase(argc, argv, name), d_ptr_(std::unique_ptr<GoalLegPrivate>(new GoalLegPrivate))
{
}

GoalLeg::~GoalLeg() = default;

void GoalLeg::setupSubscriptions()
{
  GoalBase::setupSubscriptions();
  DS_D(GoalLeg);

  // connect to nav state
  std::string state_input_topic = ros::param::param<std::string>("~state_input_topic", "estimated_state");
  ROS_INFO_STREAM(ros::this_node::getName() << " listening for vehicle state on topic \"" << state_input_topic << "\"");

  auto nh = nodeHandle();
  d->state_update_sub = nh.subscribe(state_input_topic, 1, &GoalLeg::vehicleStateCallback, this);

  d->ref_visualization_pub =
      nh.advertise<visualization_msgs::Marker>(ros::this_node::getName() + "/leg_goal", 10, false);

  d->leg_state_pub =
      nh.advertise<ds_control_msgs::GoalLegState>(ros::this_node::getName() + "/internal_state", 10, false);
}

void GoalLeg::setupParameters()
{
  DsProcess::setupParameters();
  DS_D(GoalLeg);

  ros::NodeHandle handle = nodeHandle();
  d->params = ds_param::ParamConnection::create(handle);
  std::string prefix = ros::this_node::getName() + "/";

  d->start_northing = d->params->connect<ds_param::DoubleParam>(prefix + "start_northing");
  d->start_easting = d->params->connect<ds_param::DoubleParam>(prefix + "start_easting");
  d->start_down = d->params->connect<ds_param::DoubleParam>(prefix + "start_down");

  d->end_northing = d->params->connect<ds_param::DoubleParam>(prefix + "end_northing");
  d->end_easting = d->params->connect<ds_param::DoubleParam>(prefix + "end_easting");
  d->end_down = d->params->connect<ds_param::DoubleParam>(prefix + "end_down");
  d->leg_number = d->params->connect<ds_param::IntParam>(prefix + "leg_number");

  d->kappa = d->params->connect<ds_param::DoubleParam>(prefix + "kappa");
  d->frame_name = ros::param::param<std::string>("~frame_id", "odom_dr");
}

void setInvalid(ds_nav_msgs::FlaggedDouble& var)
{
  var.value = 0;
  var.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
}

ds_nav_msgs::AggregatedState GoalLeg::computeGoal(const ds_nav_msgs::AggregatedState& vehicleState)
{
  ds_nav_msgs::AggregatedState ret;

  // zero out almost all of our state
  setInvalid(ret.northing);
  setInvalid(ret.easting);
  setInvalid(ret.down);
  setInvalid(ret.roll);
  setInvalid(ret.pitch);
  setInvalid(ret.surge_u);
  setInvalid(ret.sway_v);
  setInvalid(ret.heave_w);
  setInvalid(ret.p);
  setInvalid(ret.q);
  setInvalid(ret.r);
  ret.heading = computeHeadingGoal(vehicleState);
  ret.header.stamp = ros::Time::now();

  return ret;
}

double oline(const geometry_msgs::Point& end_pt, const geometry_msgs::Point& start_pt, double pos_x, double pos_y)
{
  double alpha;
  alpha = atan2(end_pt.x - start_pt.x, end_pt.y - start_pt.y);
  return -cos(alpha) * (pos_x - start_pt.x) + sin(alpha) * (pos_y - start_pt.y);
}

ds_nav_msgs::FlaggedDouble GoalLeg::computeHeadingGoal(const ds_nav_msgs::AggregatedState& vehicleState)
{
  DS_D(GoalLeg);

  ds_nav_msgs::FlaggedDouble ret;
  ret.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;

  // Update the timestamp for the state
  d->state.header.stamp = vehicleState.header.stamp;

  // fill in state from parameters
  // Existing reference goal seems to be using East/North instead of North / East.
  d->state.line_start.x = d->start_easting->Get();
  d->state.line_start.y = d->start_northing->Get();
  d->state.line_start.z = -d->start_down->Get();

  d->state.line_end.x = d->end_easting->Get();
  d->state.line_end.y = d->end_northing->Get();
  d->state.line_end.z = -d->end_down->Get();

  d->state.kappa = d->kappa->Get();
  d->state.leg_number = d->leg_number->Get();

  // original:
  d->state.old_goal = d->state.new_goal;

  // step 1 compute alpha [DEFINITELY atan2(easting, northing) ]
  d->state.angle_line_segment =
      atan2(d->state.line_end.x - d->state.line_start.x, d->state.line_end.y - d->state.line_start.y);

  // if we don't have a valid position, we can't compute an offlien distance.
  // Just return the direction of the line, BUT flag it as invalid
  if (!(vehicleState.northing.valid && vehicleState.easting.valid))
  {
    ret.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
    ret.value = d->state.angle_line_segment;

    return ret;
  }

  d->state.off_line_vect =
      oline(d->state.line_end, d->state.line_start, vehicleState.easting.value, vehicleState.northing.value);
  d->state.sign_of_vect = ds_util::sgn(d->state.off_line_vect);

  // Set the new goal to the sum of the off track distance and the new goal
  d->state.new_goal =
      d->state.angle_line_segment +
      d->state.sign_of_vect * std::min<double>(M_PI / 3.0, fabs(d->state.off_line_vect) * d->state.kappa);

  /*
  ROS_ERROR_STREAM("Computing heading goal.  Vehicle state: "
                       <<vehicleState.heading.value*180/M_PI <<" desire: " <<d->state.new_goal*180/M_PI
                       << " line_segment: " <<d->state.angle_line_segment*180/M_PI);
                       */
  // Take the new wrapped heading that goes from -PI to PI and add it to ref.pos
  ret.value = d->state.new_goal;

  return ret;
}

void GoalLeg::vehicleStateCallback(const ds_nav_msgs::AggregatedState& vehicleState)
{
  DS_D(GoalLeg);

  ds_nav_msgs::AggregatedState ref = computeGoal(vehicleState);
  publishGoal(ref);

  d->leg_state_pub.publish(d->state);

  visualization_msgs::Marker marker = d->fillMarker(vehicleState);
  d->ref_visualization_pub.publish(marker);
}

const ds_control_msgs::GoalLegState& GoalLeg::getState() const
{
  const DS_D(GoalLeg);
  return d->state;
}
}
