/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "goal_bottom_follow_depth/goal_bottom_follow_depth.h"

namespace ds_control
{
GoalBottomFollowDepth::GoalBottomFollowDepth() : GoalBottomFollow()
{
}

GoalBottomFollowDepth::GoalBottomFollowDepth(int argc, char** argv, const std::string& name)
  : GoalBottomFollow(argc, argv, name)
{
}

GoalBottomFollowDepth::~GoalBottomFollowDepth() = default;

void GoalBottomFollowDepth::initialize(void)
{
  bf_.bad_altitude_hits = 0;
  ext_alarm_.alarm = false;
  state_.northing.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  state_.easting.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  state_.down.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  state_.heading.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  state_.pitch.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  state_.roll.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  state_.surge_u.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  state_.sway_v.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  state_.heave_w.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  state_.p.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  state_.q.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  state_.r.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;

  if (prm.alt_bad_indeterminant->Get() == prm.alt_bad_min_range->Get() ||
      prm.alt_bad_indeterminant->Get() == prm.alt_bad_max_range->Get() ||
      prm.alt_bad_min_range->Get() == prm.alt_bad_max_range->Get())
  {
    ROS_ERROR("Bottom-follower bad altitude sentinel values are not unique. Aborting.");
  }

  if (prm.speed_gain->Get() < 1.0)
  {
    ROS_ERROR("Speed gain must be greater than 1.0");
    ROS_BREAK();
  }

  std::tie(bf_.env_padding_speed, bf_.env_padding_accel, bf_.step) =
      compute_step_size(prm.env->Get(), prm.speed_gain->Get(), prm.depth_rate_d->Get(), prm.depth_accel_d->Get());

  if (bf_.step < 0.0)
  {
    ROS_ERROR("The current parameters generate a negative depth step size. Verify envelope, speed gain, depth rate, "
              "and depth acceleration");
    ROS_BREAK();
  }

  last_agg_msg_ = ros::Time::now();
  bf_.time_inside_env = ros::Duration(prm.alarm_timeout->Get());
  bf_.alarm = 0;
  bf_.pseudo_bot_code = 0;
  bf_.alt_bad_timer = ros::Duration(0);
  bf_.last_good_altitude_time = ros::Time(0);
  bf_.last_good_altitude = 0;
  bf_.last_good_depth = 0;
  bf_.last_good_depth_bot = 0;
  bf_.stuck = false;
  bf_.depth_rate_filtered = 0;
  bf_.time_low_depth_rate = ros::Time(0);
  for (int i = 0; i < 5; i++)
    bf_.past_altitude_values[i] = 0;
  bf_.median_altitude = 0;
  bf_.last_altitude_time = ros::Time(0);
  bf_.depth_rate_d = 0.0;
  bf_.depth_accel_d = 0.0;

  bf_.userDepthOverrideEngageTime = ros::Time(0);
  bf_.userDepthOverrideEngaged = false;
  bf_.userDepthOverrideEndTime = ros::Time(0);
  bf_.override_depth_direction = 0;
  bf_.depth_bot = 0;
  bf_.depth_goal = 0;
  bf_.raw_altitude = 0;
}

void GoalBottomFollowDepth::setupParameters(void)
{
  GoalBottomFollow::setupParameters();

  auto nh = nodeHandle();
  conn_ = ds_param::ParamConnection::create(nh);
  conn_->setCallback(boost::bind(&GoalBottomFollowDepth::reconfigureCallback, this, _1));

  prm.alt_d = conn_->connect<ds_param::DoubleParam>(ros::this_node::getName() + "/altitude");
  prm.env = conn_->connect<ds_param::DoubleParam>(ros::this_node::getName() + "/envelope");
  prm.speed_d = conn_->connect<ds_param::DoubleParam>(ros::this_node::getName() + "/speed");
  prm.speed_gain = conn_->connect<ds_param::DoubleParam>(ros::this_node::getName() + "/speed_gain");
  prm.depth_rate_d = conn_->connect<ds_param::DoubleParam>(ros::this_node::getName() + "/depth_rate");
  prm.depth_accel_d = conn_->connect<ds_param::DoubleParam>(ros::this_node::getName() + "/depth_acceleration");
  prm.speed_min = conn_->connect<ds_param::DoubleParam>(ros::this_node::getName() + "/speed_minimum");
  prm.alarm_timeout = conn_->connect<ds_param::DoubleParam>(ros::this_node::getName() + "/alarm_timeout");
  prm.depth_floor = conn_->connect<ds_param::DoubleParam>(ros::this_node::getName() + "/depth_floor");
  prm.alt_max_range = conn_->connect<ds_param::DoubleParam>(ros::this_node::getName() + "/alt_max_range");
  prm.alt_bad_max_range = conn_->connect<ds_param::DoubleParam>(ros::this_node::getName() + "/alt_bad_max_range");
  prm.alt_min_range = conn_->connect<ds_param::DoubleParam>(ros::this_node::getName() + "/alt_min_range");
  prm.alt_bad_min_range = conn_->connect<ds_param::DoubleParam>(ros::this_node::getName() + "/alt_bad_min_range");
  prm.alt_bad_indeterminant =
      conn_->connect<ds_param::DoubleParam>(ros::this_node::getName() + "/alt_bad_indeterminant");
  prm.alt_bad_timeout = conn_->connect<ds_param::DoubleParam>(ros::this_node::getName() + "/alt_bad_timeout");
  prm.speed_outside = conn_->connect<ds_param::DoubleParam>(ros::this_node::getName() + "/speed_outside");
  prm.median_tol = conn_->connect<ds_param::DoubleParam>(ros::this_node::getName() + "/median_tol");
  prm.time_low_depth_rate_limit =
      conn_->connect<ds_param::DoubleParam>(ros::this_node::getName() + "/time_low_depth_rate_limit");
  prm.depth_rate_alpha = conn_->connect<ds_param::DoubleParam>(ros::this_node::getName() + "/depth_rate_alpha");
  prm.depth_rate_threshold = conn_->connect<ds_param::DoubleParam>(ros::this_node::getName() + "/depth_rate_threshold");
  prm.alt_fly_down = conn_->connect<ds_param::DoubleParam>(ros::this_node::getName() + "/alt_fly_down");
  prm.slow_above_env = conn_->connect<ds_param::BoolParam>(ros::this_node::getName() + "/slow_above_env");

  initialize();
  setRangeCallback(boost::bind(&GoalBottomFollowDepth::rangesCallback, this, _1));

  bf_.depth_rate_d = prm.depth_rate_d->Get();
  bf_.depth_accel_d = prm.depth_accel_d->Get();
}

void GoalBottomFollowDepth::setupPublishers(void)
{
  GoalBottomFollow::setupPublishers();

  const auto output_topic = ros::param::param<std::string>("~bf_output_topic", "bf_topic");
  bf_pub_ =
      nodeHandle().advertise<ds_control_msgs::BottomFollow1D>(ros::this_node::getName() + "/" + output_topic, 1, false);
}

void GoalBottomFollowDepth::setupSubscriptions(void)
{
  GoalBottomFollow::setupSubscriptions();

  const auto state_topic = ros::param::param<std::string>("~aggregated_state_topic", "/state");
  nav_state_ = nodeHandle().subscribe<ds_nav_msgs::AggregatedState>(
      state_topic, 1, boost::bind(&GoalBottomFollowDepth::onAggregatedStateMsg, this, _1));
  const auto ext_alarm_topic = ros::param::param<std::string>("~external_alarm_topic", "/alarm");
  ext_alarm_sub_ = nodeHandle().subscribe<ds_control_msgs::ExternalBottomFollowAlarm>(
      ext_alarm_topic, 1, boost::bind(&GoalBottomFollowDepth::onExternalAlarmMsg, this, _1));
  const auto ext_override_topic = ros::param::param<std::string>("~external_depth_override_topic", "/override");
  ext_override_sub_ = nodeHandle().subscribe<ds_control_msgs::ExternalBottomFollowTimedOverride>(
      ext_override_topic, 1, boost::bind(&GoalBottomFollowDepth::onExternalBottomFollowTimedOverrideMsg, this, _1));
}

void GoalBottomFollowDepth::rangesCallback(ds_sensor_msgs::Ranges3D in)
{
  ROS_DEBUG_STREAM("Processing ranges");
  double minimum_beam = std::numeric_limits<double>::infinity();
  bool range_valid = false;
  for (auto const& range : in.ranges)
  {
    if (range.range_validity == ds_sensor_msgs::Range3D::RANGE_VALID)
    {
      range_valid = true;
      if (-range.range.point.z < minimum_beam)
      {
        // The range vector we got is already rotated to body frame by the base class, so z is the vertical component
        minimum_beam = -range.range.point.z;
        altitude_ = minimum_beam;
        ROS_DEBUG_STREAM("Altitude: " << altitude_);
        altitude_time_ = ros::Time::now();
        bf_.bad_altitude_hits = 0;
      }
    }
  }
  // There is not any valid range in the vector of ranges received
  if (!range_valid)
  {
    altitude_ = prm.alt_bad_indeterminant->Get();
    altitude_time_ = ros::Time::now();
    bf_.bad_altitude_hits++;
  }
}

void GoalBottomFollowDepth::add_altitude(double altitude, ros::Time alt_time)
{
  altitude_ = altitude;
  altitude_time_ = alt_time;
}

double GoalBottomFollowDepth::compute_speed_padding(double env, double speed_gain, double depth_rate_d)
{
  double env_padding_speed;

  env_padding_speed = 0.0;
  if (depth_rate_d != 0.0)
    env_padding_speed = env / (2 * speed_gain);

  return env_padding_speed;
}

double GoalBottomFollowDepth::compute_accel_padding(double depth_rate_d, double depth_accel_d)
{
  double env_padding_accel;

  env_padding_accel = 0.0;
  if (depth_accel_d != 0.0)
    env_padding_accel = 0.5 * pow(depth_rate_d, 2) / depth_accel_d;

  return env_padding_accel;
}

double GoalBottomFollowDepth::compute_step(double env, double env_padding_speed, double env_padding_accel)
{
  double step;

  step = env / 2.0;
  step -= env_padding_speed;
  step -= env_padding_accel;

  return step;
}

std::tuple<double, double, double> GoalBottomFollowDepth::compute_step_size(double env, double speed_gain,
                                                                            double depth_rate_d, double depth_accel_d)
{
  double env_padding_speed = compute_speed_padding(env, speed_gain, depth_rate_d);

  double env_padding_accel = compute_accel_padding(depth_rate_d, depth_accel_d);

  double step = compute_step(env, env_padding_speed, env_padding_accel);

  return std::make_tuple(env_padding_speed, env_padding_accel, step);
}

void GoalBottomFollowDepth::push_altitude_buffer(double altitude)
{
  for (int i = 5 - 1; i > 0; i--)
  {
    bf_.past_altitude_values[i] = bf_.past_altitude_values[i - 1];
  }
  bf_.past_altitude_values[0] = altitude;
}

void GoalBottomFollowDepth::reconfigureCallback(const ds_param::ParamConnection::ParamCollection& params)
{
  // This callback gets triggered periodically with an empty parameter set.
  if (params.empty()) {
    return;
  }
  // Lock the param guard
  ds_param::ParamGuard lock(conn_);

  ROS_WARN_STREAM("Updating " << params.size() << " parameters:");
  std::for_each(params.cbegin(), params.cend(), [](const std::shared_ptr<ds_param::UpdatingParam> &it){
    const auto type_ = it->TypeNum();
    if(type_ == ds_param::UpdatingParamTypes::PARAM_DOUBLE) {
      auto p = static_cast<ds_param::DoubleParam*>(it.get());
      const auto current =  p->Get();
      const auto previous = p->GetPrevious();
      if(previous) {
        ROS_WARN_STREAM("  " << it->Name() << " from " << *previous << " to " << current);
      }
      else {
        ROS_WARN_STREAM("  " << it->Name() << " from <UNSET> to " << current);
      }
    }
    else if(type_ == ds_param::UpdatingParamTypes::PARAM_BOOL) {
      auto p = static_cast<ds_param::BoolParam*>(it.get());
      const auto current =  p->Get();
      const auto previous = p->GetPrevious();
      if(previous) {
        ROS_WARN_STREAM("  " << p->Name() << " from " << std::boolalpha << *previous << " to " << std::boolalpha << current);
      }
      else {
        ROS_WARN_STREAM("  " << p->Name() << "from <UNSET> to " << current);
      }
    }
    else {
      // At time of fix only DoubleParam and BoolParam are used.  Shouldn't get any other types, but someone may add one in the future.
      ROS_ERROR_STREAM("Unhandled ds_param::UpdatingParamType: " << type_);
      ROS_ERROR_STREAM("This is a bug and is probably cause by a new ds_param type added to GoalBottomFollowerDepth.");
    }
  });


  for (auto iter = params.begin(); iter != params.end(); iter++)
  {
    if (*iter == prm.speed_gain)
    {
      ROS_INFO_STREAM("\t\t\tspeed_gain Changed from \"" << prm.speed_gain->GetPrevious() << "\" to \""
                                                         << prm.speed_gain->Get() << "\"");
      if (prm.speed_gain->Get() < 1.0)
      {
        if (prm.speed_gain->GetPrevious())  // Check that there is a previous value
        {
          ROS_WARN_STREAM("New speed_gain invalid, restoring previous value! speed_gain must be >= 1.0");
          prm.speed_gain->Set(*prm.speed_gain->GetPrevious());
        }
      }
    }
  }

  std::tie(bf_.env_padding_speed, bf_.env_padding_accel, bf_.step) =
      compute_step_size(prm.env->Get(), prm.speed_gain->Get(), prm.depth_rate_d->Get(), prm.depth_accel_d->Get());

  // ROS_ERROR_STREAM("depth_rate_d before: " << prm.depth_rate_d->Get() << " depth_accel_d before: " <<
  // prm.depth_accel_d->Get() << " step before: " << bf_.step);
  // Restore params and recompute step size if step size result was less than 0
  if (bf_.step <= 0.0)
  {
    ROS_WARN_STREAM("Bottom following parameter update produces negative step value: " << bf_.step);
    ROS_WARN_STREAM("Rolling back parameter update and recalculating a new step value.");
    for(auto it=params.begin(); it != params.end(); ++it ){
        const auto type_ = (*it)->TypeNum();
        if(type_ == ds_param::UpdatingParamTypes::PARAM_DOUBLE) {
          auto p = static_cast<ds_param::DoubleParam*>(it->get());
          const auto current =  p->Get();
          const auto previous = p->GetPrevious();
          if(previous) {
            ROS_WARN_STREAM("Resetting " << p->Name() << " from " << current << " to " << *previous);
            p->Set(*previous);
          }
          else {
            ROS_ERROR_STREAM("Unable to reset " << p->Name() << "from " << current << ", no previous value stored");
          }
        }
        else if(type_ == ds_param::UpdatingParamTypes::PARAM_BOOL) {
          auto p = static_cast<ds_param::BoolParam*>(it->get());
          const auto current =  p->Get();
          const auto previous = p->GetPrevious();
          if(previous) {
            ROS_WARN_STREAM("Resetting " << p->Name() << " from " << std::boolalpha << current << " to " << std::boolalpha << *previous);
            p->Set(*previous);
          }
          else {
            ROS_ERROR_STREAM("Unable to reset " << p->Name() << "from " << std::boolalpha << current << ", no previous value stored");
          }
        }
        else {
          // At time of fix only DoubleParam and BoolParam are used.  Shouldn't get any other types, but someone may add one in the future.
          ROS_ERROR_STREAM("Unhandled ds_param::UpdatingParamType: " << type_);
          ROS_ERROR_STREAM("This is a bug and is probably cause by a new ds_param type added to GoalBottomFollowerDepth.");
        }
    }
    std::tie(bf_.env_padding_speed, bf_.env_padding_accel, bf_.step) =
        compute_step_size(prm.env->Get(), prm.speed_gain->Get(), prm.depth_rate_d->Get(), prm.depth_accel_d->Get());
    ROS_WARN_STREAM("Bottom following step value after rollback: " << bf_.step);
  }

  // ROS_ERROR_STREAM("depth_rate_d after: " << prm.depth_rate_d->Get() << " depth_accel_d after: " <<
  // prm.depth_accel_d->Get() << " step before: " << bf_.step);

  bf_.depth_rate_d = prm.depth_rate_d->Get();
  bf_.depth_accel_d = prm.depth_accel_d->Get();
  bf_.depth_floor = prm.depth_floor->Get();
  bf_.alarm_timeout = prm.alarm_timeout->Get();
  bf_.min_speed = prm.speed_min->Get();
  bf_.speed_gain = prm.speed_gain->Get();
}

ds_control_msgs::BottomFollow1D& GoalBottomFollowDepth::getBf(void)
{
  return bf_;
}

void GoalBottomFollowDepth::setBf(ds_control_msgs::BottomFollow1D in)
{
  bf_ = in;
}

std::pair<double, bool> GoalBottomFollowDepth::computeBottomDepth(ros::Time time_now, double heave_w, double depth)
{
  double depth_floor_bot = prm.depth_floor->Get() + prm.alt_d->Get() - prm.env->Get() / 2.0;
  bool good_altitude = false;
  bf_.pseudo_bot_code = 0;

  if (altitude_ == prm.alt_bad_indeterminant->Get())
  {
    bf_.pseudo_bot_code = (1 << BOTTOM_FOLLOW_PSEUDO_BOT_CODE_ALT_BAD_MIN_RANGE);
  }

  if ((bf_.pseudo_bot_code & (1 << BOTTOM_FOLLOW_PSEUDO_BOT_CODE_ALT_BAD_MIN_RANGE)) &&
      (bf_.bad_altitude_hits > N_BAD_ALTITUDES))
    bf_.depth_bot = std::min(depth, depth_floor_bot);
  else if ((bf_.pseudo_bot_code & (1 << BOTTOM_FOLLOW_PSEUDO_BOT_CODE_ALT_BAD_MAX_RANGE)) &&
           (bf_.bad_altitude_hits > N_BAD_ALTITUDES))
    bf_.depth_bot = std::min(depth + prm.alt_max_range->Get(), depth_floor_bot);
  else if ((bf_.bad_altitude_hits <= N_BAD_ALTITUDES) && (altitude_ != prm.alt_bad_indeterminant->Get()))
  {
    bf_.depth_bot = std::min(depth + altitude_, depth_floor_bot);
    good_altitude = true;
  }

  return std::make_pair(depth_floor_bot, good_altitude);
}

double GoalBottomFollowDepth::computeDepthRateFiltered(double depth_rate_filtered_in, double heave_w, ros::Time now)
{
  double depth_rate_filtered =
      prm.depth_rate_alpha->Get() * depth_rate_filtered_in + (1.0 - prm.depth_rate_alpha->Get()) * heave_w;

  if (fabs(depth_rate_filtered) >= prm.depth_rate_threshold->Get())
  {
    bf_.time_low_depth_rate = now;
  }

  return depth_rate_filtered;
}

std::pair<double, double> GoalBottomFollowDepth::computeEnvelope(double depth_bot)
{
  double env_upper = depth_bot - prm.alt_d->Get() - prm.env->Get() / 2.0;  // upper
  double env_lower = depth_bot - prm.alt_d->Get() + prm.env->Get() / 2.0;  // lower

  return std::make_pair(env_upper, env_lower);
}

void GoalBottomFollowDepth::processAltitude(bool good_altitude, double depth, ros::Time time_now,
                                            double depth_floor_bot)
{
  if (altitude_time_ > bf_.last_altitude_time)
  {
    // TODO: port median functionality
    double tmp[5];
    for (int i = 0; i < 5; ++i)
      tmp[i] = bf_.past_altitude_values[i];
    bf_.median_altitude = ds_util::median(tmp, 5);
    if ((good_altitude) && (bf_.median_altitude > 0) && (fabs(bf_.median_altitude - altitude_) < prm.median_tol->Get()))
    {
      bf_.last_good_depth = depth;
      bf_.last_good_altitude = altitude_;
      bf_.last_good_depth_bot = bf_.depth_bot;
      bf_.last_good_altitude_time = time_now;
      // 2018-03-22 SS - pushing altitude buffer only if altitude is good, to avoid pushing -3 in the buffer
      // push_altitude_buffer(altitude_);
    }
    // We need this to initialize the buffer when it's empty
    // if (bf_.median_altitude == 0)
    //  push_altitude_buffer(altitude_);
    if (altitude_ != prm.alt_bad_indeterminant->Get())
      push_altitude_buffer(altitude_);
  }
  bf_.last_altitude_time = altitude_time_;

  if (fabs(bf_.depth_rate_filtered) > prm.depth_rate_threshold->Get())
    bf_.stuck = false;

  // SS - added bf_.bad_altitude_hits comparison to avoid transient behavior on flyers
  if ((!good_altitude) && (bf_.bad_altitude_hits > N_BAD_ALTITUDES))  // altitude is invalid
  {
    double alt_fly_down;
    if ((time_now - bf_.time_low_depth_rate).toSec() > prm.time_low_depth_rate_limit->Get())
      bf_.stuck = true;
    alt_fly_down = prm.alt_fly_down->Get();
    if (alt_fly_down < prm.alt_d->Get() + prm.env->Get() / 2.0)
      alt_fly_down = prm.alt_d->Get() + prm.env->Get() / 2.0;
    if (bf_.last_good_altitude > alt_fly_down)
    {
      // make sure the bottom depth doesn't exceed the value that would
      // put the depth setpoint below the floor
      bf_.depth_bot = std::min(depth + prm.alt_d->Get() + prm.env->Get() / 2.0, depth_floor_bot);
    }
    else
    {
      bf_.depth_bot = depth;
    }
    if (bf_.stuck)
    {
      // set the bottom depth to the vehicle depth and set the commanded fwd speed to zero
      bf_.depth_bot = depth;
      // SS - moved the zeroing of ref speed to the actual ref speed computation function
      // bf_.ref_speed=0.0;
    }
  }
}

void GoalBottomFollowDepth::userDepthOverride(double depth)
{
  ros::Time now = ros::Time::now();
  bf_.userDepthOverrideEngaged = false;
  // Disengage if we're having good bottom lock for the last few seconds
  if (now - bf_.last_good_altitude_time < ros::Duration(5.0))
  {
    return;
  }
  // Engage otherwise, until timeout reached
  else if ((bf_.userDepthOverrideEndTime - now) > ros::Duration(0.0))
  {
    double depth_floor_bot = prm.depth_floor->Get() + prm.alt_d->Get() - prm.env->Get() / 2.0;
    if ((ext_override_.override_depth_direction) == ds_control_msgs::ExternalBottomFollowTimedOverride::DRIVE_UP)
    {
      bf_.userDepthOverrideEngaged = true;
      bf_.override_depth_direction = ds_control_msgs::ExternalBottomFollowTimedOverride::DRIVE_UP;
      bf_.depth_bot = std::min(depth, depth_floor_bot);
    }
    else if ((ext_override_.override_depth_direction) == ds_control_msgs::ExternalBottomFollowTimedOverride::DRIVE_DOWN)
    {
      bf_.userDepthOverrideEngaged = true;
      bf_.override_depth_direction = ds_control_msgs::ExternalBottomFollowTimedOverride::DRIVE_DOWN;
      bf_.depth_bot = std::min(depth + prm.alt_max_range->Get(), depth_floor_bot);
    }
  }
}

double GoalBottomFollowDepth::computeDepthGoal(void)
{
  double depth_goal = bf_.depth_goal;
  if (depth_goal < bf_.depth_env[0] + (prm.env->Get() / 2.0 - bf_.step))  // Too high.
    depth_goal += bf_.step;
  else if (depth_goal > bf_.depth_env[1] - (prm.env->Get() / 2.0 - bf_.step))  // Too low.
  {
    if (bf_.alarm)
    {
      depth_goal = (bf_.depth_env[0] + bf_.depth_env[1]) / 2.0;
    }
    else
      depth_goal -= bf_.step;
  }
  if ((depth_goal > bf_.depth_env[0]) && (depth_goal < bf_.depth_env[1]))
    depth_goal = BOTTOM_ALPHA * depth_goal + (1.0 - BOTTOM_ALPHA) * (bf_.depth_env[0] + bf_.depth_env[1]) / 2.0;

  return depth_goal;
}

std::pair<double, bool> GoalBottomFollowDepth::computeSpeedGoal(double depth, ros::Duration dt)
{
  double ref_speed = prm.speed_d->Get();
  bool alarm;

  // Determine goal speed.  Within the speed padding, the goal speed
  // is linearly decreased from speed_d to speed_min.
  // bf_.ref_speed = prm.speed_d->Get();
  if (prm.slow_above_env->Get())
  {
    // Slow down if above or below envelope.
    if (depth < bf_.depth_env[0] + bf_.env_padding_speed)  // Vehicle in upper speed padding zone.
      ref_speed -= 2 * prm.speed_gain->Get() / prm.env->Get() * (prm.speed_d->Get() - prm.speed_min->Get()) *
                   ((bf_.depth_env[0] + bf_.env_padding_speed) - depth);
    else if (depth > bf_.depth_env[1] - bf_.env_padding_speed)  // Vehicle in lower speed padding zone.
      ref_speed -= 2 * prm.speed_gain->Get() / prm.env->Get() * (prm.speed_d->Get() - prm.speed_min->Get()) *
                   (depth - (bf_.depth_env[1] - bf_.env_padding_speed));
  }
  else
  {
    // Only slow down if below envelope.
    if (depth > bf_.depth_env[1] - bf_.env_padding_speed)  // Vehicle in lower speed padding zone.
      ref_speed -= 2 * prm.speed_gain->Get() / prm.env->Get() * (prm.speed_d->Get() - prm.speed_min->Get()) *
                   (depth - (bf_.depth_env[1] - bf_.env_padding_speed));
  }
  // Mark the time within the region for which ref_speed > speed_min,
  // and override the ref_speed if outside this.  For padded envelopes,
  // the time computed is equivalent to the time spent within the
  // envelope.
  if (ref_speed > prm.speed_min->Get())
    // TODO: add appropriate duration between two msg!
    bf_.time_inside_env += dt;  // ROV_UPDATE_PERIOD;
  else
    bf_.time_inside_env = ros::Duration(0.0);
  if (ref_speed < prm.speed_min->Get())
    ref_speed = prm.speed_outside->Get();

  // SS - moved here zerong of the ref speed if stuck
  if (bf_.stuck)
    ref_speed = 0.0;

  // Set the alarm if time_inside_env is less than alarm timeout.
  if (bf_.time_inside_env.toSec() < prm.alarm_timeout->Get())
    alarm = true;
  else  // Stand down.
    alarm = false;

  return std::make_pair(ref_speed, alarm);
}

void GoalBottomFollowDepth::onExternalAlarmMsg(const ds_control_msgs::ExternalBottomFollowAlarm::ConstPtr& msg)
{
  ext_alarm_ = *msg;
}

void GoalBottomFollowDepth::onExternalBottomFollowTimedOverrideMsg(
    const ds_control_msgs::ExternalBottomFollowTimedOverride::ConstPtr& msg)
{
  ext_override_ = *msg;

  bf_.userDepthOverrideEngageTime = ros::Time::now();
  bf_.userDepthOverrideEndTime = bf_.userDepthOverrideEngageTime + msg->timeout;
}

void GoalBottomFollowDepth::onAggregatedStateMsg(const ds_nav_msgs::AggregatedState::ConstPtr& msg)
{
  double depth_floor_bot;
  bool good_altitude = false;
  ros::Time time_now = ros::Time::now();

  // Estimate the depth of the seafloor based on altitude and vehicle depth
  std::tie(depth_floor_bot, good_altitude) = computeBottomDepth(time_now, msg->heave_w.value, msg->down.value);

  // Compute depth rate filtered and verify if the rate is low
  bf_.depth_rate_filtered = computeDepthRateFiltered(bf_.depth_rate_filtered, msg->heave_w.value, time_now);

  // Process the altitude and decide how to react to bad altitudes
  processAltitude(good_altitude, msg->down.value, time_now, depth_floor_bot);

  // Evaluated whether or not the user wants to push up or push down
  // and eventually override the depth_bot previously computed based on this
  userDepthOverride(msg->down.value);

  // Compute envelope.
  std::tie(bf_.depth_env[0], bf_.depth_env[1]) = computeEnvelope(bf_.depth_bot);

  // Determine depth goal.
  bf_.depth_goal = computeDepthGoal();

  // Determine speed goal
  ros::Duration time_since_last = msg->header.stamp - last_agg_msg_;
  std::tie(bf_.ref_speed, bf_.alarm) = computeSpeedGoal(msg->down.value, time_since_last);
  last_agg_msg_ = msg->header.stamp;

  // Assign updated values to goal
  state_.down.value = bf_.depth_goal;
  if (ext_alarm_.alarm)
    state_.down.value += ext_alarm_.delta_down;
  state_.down.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
  state_.surge_u.value = bf_.ref_speed;
  if (ext_alarm_.alarm)
    state_.surge_u.value = ext_alarm_.speed_override;
  state_.surge_u.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
  state_.header = msg->header;
  state_.ds_header = msg->ds_header;

  bf_.header = msg->header;
  bf_.ds_header = msg->ds_header;

  // Also publish the internal state of the bottom follower for introspection
  bf_.commanded_altitude = prm.alt_d->Get();
  bf_.raw_altitude = altitude_;
  bf_pub_.publish(bf_);

  // Publish the goal for the controller to use
  publishGoal(state_);
}
}
