/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_CONTROL_GOAL_BOTTOM_FOLLOW_DEPTH_H
#define DS_CONTROL_GOAL_BOTTOM_FOLLOW_DEPTH_H

#include "goal_bottom_follow/goal_bottom_follow.h"
#include "ds_control_msgs/BottomFollow1D.h"
#include "ds_control_msgs/ExternalBottomFollowAlarm.h"
#include "ds_control_msgs/ExternalBottomFollowTimedOverride.h"
#include "ds_nav_msgs/AggregatedState.h"
#include <boost/optional/optional_io.hpp>
#include "ds_util/median.h"
#include <limits>

#define BOTTOM_ALPHA (0.98)
#define N_BAD_ALTITUDES 6

namespace ds_control
{
typedef enum {
  BOTTOM_FOLLOW_PSEUDO_BOT_CODE_DEPTH_FLOOR,       /* bit 00: 000...00000001 */
  BOTTOM_FOLLOW_PSEUDO_BOT_CODE_ALT_BAD_MIN_RANGE, /* bit 01: 000...00000010 */
  BOTTOM_FOLLOW_PSEUDO_BOT_CODE_ALT_BAD_MAX_RANGE, /* bit 02: 000...00000100 */
} pseudo_depth_code;

typedef struct
{
  ds_param::DoubleParam::Ptr alt_d;
  ds_param::DoubleParam::Ptr env;
  ds_param::DoubleParam::Ptr speed_d;
  ds_param::DoubleParam::Ptr speed_gain;
  ds_param::DoubleParam::Ptr depth_rate_d;
  ds_param::DoubleParam::Ptr depth_accel_d;
  ds_param::DoubleParam::Ptr speed_min;
  ds_param::DoubleParam::Ptr alarm_timeout;
  ds_param::DoubleParam::Ptr depth_floor;
  ds_param::DoubleParam::Ptr alt_max_range;
  ds_param::DoubleParam::Ptr alt_bad_max_range;
  ds_param::DoubleParam::Ptr alt_min_range;
  ds_param::DoubleParam::Ptr alt_bad_min_range;
  ds_param::DoubleParam::Ptr alt_bad_indeterminant;
  ds_param::DoubleParam::Ptr alt_bad_timeout;
  ds_param::DoubleParam::Ptr speed_outside;
  ds_param::DoubleParam::Ptr median_tol;
  ds_param::DoubleParam::Ptr time_low_depth_rate_limit;
  ds_param::DoubleParam::Ptr depth_rate_alpha;
  ds_param::DoubleParam::Ptr depth_rate_threshold;
  ds_param::DoubleParam::Ptr alt_fly_down;
  ds_param::BoolParam::Ptr slow_above_env;
} prm_t;

class GoalBottomFollowDepth final : public GoalBottomFollow
{
public:
  /// @brief Construct a new GoalBottomFollow
  ///
  /// When using this constructor you must call ros::init elsewhere in your code
  GoalBottomFollowDepth();

  /// @brief Construct a new GoalBottomFollow
  ///
  /// This constructor calls ros::init(argc, argv, name) for you
  ///
  /// @param[in] argc
  /// @param[in] argv
  /// @param[in] name The name of the process type
  GoalBottomFollowDepth(int argc, char** argv, const std::string& name);

  /// @brief Destroys a GoalBottomFollowDepth object
  ~GoalBottomFollowDepth() override;

  uint64_t type() const noexcept override
  {
    return 0;
  }

  void initialize(void);

  std::tuple<double, double, double> compute_params(void);

  void rangesCallback(ds_sensor_msgs::Ranges3D in);

  /// @brief Get a reference to the computed private bf_ instance
  ds_control_msgs::BottomFollow1D& getBf(void);

  /// @brief Sets the private bf_ member from the input param in
  void setBf(ds_control_msgs::BottomFollow1D in);

  /// @brief Returns the bottom depth (accounting for depth floor) and whether the altitude is good or not
  std::pair<double, bool> computeBottomDepth(ros::Time time_now, double heave_w, double depth);

  /// @brief Compute depth rate filtered and verify if the rate is low
  double computeDepthRateFiltered(double depth_rate_filtered_in, double heave_w, ros::Time now);

  /// @brief Returns the high depth envelope value, and the low depth envelope value
  std::pair<double, double> computeEnvelope(double depth_bot);

  /// @brief Decide how to react to bad altitudes
  void processAltitude(bool good_altitude, double depth, ros::Time time_now, double depth_floor_bot);

  /// @brief Returns the depth goal
  double computeDepthGoal(void);

  /// @brief Computes goal forward speed. Returns goal speed and alarm value
  std::pair<double, bool> computeSpeedGoal(double depth, ros::Duration dt);

  void push_altitude_buffer(double altitude);

  void add_altitude(double altitude, ros::Time alt_time);

  double compute_speed_padding(double env, double speed_gain, double depth_rate_d);

  double compute_accel_padding(double depth_rate_d, double depth_accel_d);

  double compute_step(double env, double env_padding_speed, double env_padding_accel);

  std::tuple<double, double, double> compute_step_size(double env, double speed_gain, double depth_rate_d,
                                                       double depth_accel_d);

  void reconfigureCallback(const ds_param::ParamConnection::ParamCollection& params);

  void onAggregatedStateMsg(const ds_nav_msgs::AggregatedState::ConstPtr& msg);

  void onExternalAlarmMsg(const ds_control_msgs::ExternalBottomFollowAlarm::ConstPtr& msg);

  void onExternalBottomFollowTimedOverrideMsg(const ds_control_msgs::ExternalBottomFollowTimedOverride::ConstPtr& msg);

  // Checks whether or not the user wants to push up or push down. If so, and if the altitude is not good,
  // It overrides the depth_bot computation based on the user's desired behavior
  void userDepthOverride(double depth);

protected:
  void setupParameters() override;
  void setupSubscriptions() override;
  void setupPublishers() override;

private:
  ds_nav_msgs::AggregatedState state_;

  ds_control_msgs::ExternalBottomFollowAlarm ext_alarm_;

  ds_control_msgs::BottomFollow1D bf_;

  ds_control_msgs::ExternalBottomFollowTimedOverride ext_override_;

  ds_param::ParamConnection::Ptr conn_;

  prm_t prm;

  ros::Publisher bf_pub_;
  ros::Subscriber nav_state_;
  ros::Subscriber ext_alarm_sub_;
  ros::Subscriber ext_override_sub_;

  double altitude_;
  ros::Time altitude_time_;
  int bad_altitude_hits_;

  ros::Time last_agg_msg_;
};
}

#endif
