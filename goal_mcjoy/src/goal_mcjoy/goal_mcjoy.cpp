/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "goal_mcjoy/goal_mcjoy.h"
#include "goal_mcjoy_private.h"

namespace goal_mcjoy
{
GoalMcJoy::GoalMcJoy() : ds_control::JoystickBase(), d_ptr_(std::unique_ptr<GoalMcJoyPrivate>(new GoalMcJoyPrivate))
{
}

GoalMcJoy::GoalMcJoy(int argc, char** argv, const std::string& name)
  : ds_control::JoystickBase(argc, argv, name), d_ptr_(std::unique_ptr<GoalMcJoyPrivate>(new GoalMcJoyPrivate))
{
}

GoalMcJoy::~GoalMcJoy() = default;

void GoalMcJoy::setupSubscriptions()
{
  JoystickBase::setupSubscriptions();
  DS_D(GoalMcJoy);
  const auto joy_channel =
      ros::param::param<std::string>("~joystick_topic", ros::this_node::getName() + "/joystick_in");
  d->joy_sub_ = nodeHandle().subscribe(joy_channel, 10, &GoalMcJoy::joystickCallback, this);

  const auto agg_channel = ros::param::param<std::string>("~nav_topic", ros::this_node::getName() + "/nav_topic");
  d->agg_sub_ = nodeHandle().subscribe(agg_channel, 10, &GoalMcJoy::onAggregatedStateMsg, this);
}

void GoalMcJoy::setupParameters()
{
  JoystickBase::setupParameters();
}

void GoalMcJoy::joystickCallback(const ds_nav_msgs::AggregatedState& in)
{
  DS_D(GoalMcJoy);
  d->last_joy_ = in;
}

void GoalMcJoy::onAggregatedStateMsg(const ds_nav_msgs::AggregatedState& in)
{
  DS_D(GoalMcJoy);
  applyGainsAndPublish(d->last_joy_);
}
}
